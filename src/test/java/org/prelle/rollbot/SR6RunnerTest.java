package org.prelle.rollbot;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

import org.prelle.discord.sr6.StepAddAttributeToSkill;
import org.prelle.discord.sr6.StepAddExplodeOnEdge;
import org.prelle.discord.sr6.StepAddSkillAttribValues;
import org.prelle.discord.sr6.StepAddWildFlagOnEdge;
import org.prelle.discord.sr6.CharacterVariableResolver;
import org.prelle.discord.sr6.StepEvaluateHits;
import org.prelle.discord.sr6.StepMarkWildDie;
import org.prelle.discord.sr6.StepNoDiceOpIsD6;
import org.prelle.discord.sr6.SR6FlagResolver;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;

import de.rpgframework.DummyRPGFrameworkInitCallback;
import de.rpgframework.RPGFramework;
import de.rpgframework.RPGFrameworkConstants;
import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.boot.StandardBootSteps;

public class SR6RunnerTest {

    public static void main(String[] args) throws IOException{
		System.err.println(getUserInstallationDirectory().toString());
   	System.setProperty(RPGFrameworkConstants.PROPERTY_INSTALLATION_DIRECTORY, getUserInstallationDirectory().toString());
        (new SR6RunnerTest()).test2();
        System.exit(0);
    }

	//-------------------------------------------------------------------
	public static void test() throws IOException {
		Locale.setDefault(Locale.GERMAN);
		RPGFramework framework = RPGFrameworkLoader.getInstance();
		framework.addBootStep(StandardBootSteps.FRAMEWORK_PLUGINS);
		framework.addBootStep(StandardBootSteps.ROLEPLAYING_SYSTEMS);
		framework.addBootStep(StandardBootSteps.PRODUCT_DATA);
		framework.initialize(new DummyRPGFrameworkInitCallback());
		ShadowrunCharacter model = ShadowrunCore.load(new FileInputStream("src/test/resources/testdata/Butterfly.xml"));
//		DiceRoller.execute(DiceRoller.parse("2*3 + 4*5 +3   \"Testwurf\""), EvaluationType.ROLL_AS_D6_POOL, null, null);
//		DiceRoller.execute(DiceRoller.parse("3d6+4  \"Testwurf\" !EXPLODE"), EvaluationType.ROLL_AS_D6_POOL, null, null);
		DiceRoller.execute("Feuerwaffen/Pistolen Geschick EDGE", new CharacterVariableResolver(model), new SR6FlagResolver(),
				new StepAddWildFlagOnEdge(),
				new StepAddAttributeToSkill(),
				new StepAddExplodeOnEdge(),
				new StepNoDiceOpIsD6(),
				new StepMarkWildDie(),
				new StepEvaluateHits());
	}

	//-------------------------------------------------------------------
	public static void test2() throws IOException {
		Locale.setDefault(Locale.GERMAN);
		RPGFramework framework = RPGFrameworkLoader.getInstance();
		framework.addBootStep(StandardBootSteps.FRAMEWORK_PLUGINS);
		framework.addBootStep(StandardBootSteps.ROLEPLAYING_SYSTEMS);
		framework.addBootStep(StandardBootSteps.PRODUCT_DATA);
		framework.initialize(new DummyRPGFrameworkInitCallback());
		ShadowrunCharacter model = ShadowrunCore.load(new FileInputStream("src/test/resources/testdata/Butterfly.xml"));
//		DiceRoller.execute(DiceRoller.parse("2*3 + 4*5 +3   \"Testwurf\""), EvaluationType.ROLL_AS_D6_POOL, null, null);
//		DiceRoller.execute(DiceRoller.parse("3d6+4  \"Testwurf\" !EXPLODE"), EvaluationType.ROLL_AS_D6_POOL, null, null);
		DiceRoller.execute("Wahr ", new CharacterVariableResolver(model), new SR6FlagResolver(),
				new StepAddWildFlagOnEdge(),
				new StepAddAttributeToSkill(),
				new StepAddExplodeOnEdge(),
				new StepAddSkillAttribValues(model),
				new StepNoDiceOpIsD6(),
				new StepMarkWildDie(),
				new StepEvaluateHits());
	}

	//-------------------------------------------------------------------
	private static Path getUserInstallationDirectory() {
		Path home = Paths.get(System.getProperty("user.home"));
		if (System.getProperty("os.name").toLowerCase().indexOf("mac")>0) {
			// Running on Mac OS
			return home.resolve("Library").resolve("Application Support").resolve("de.rpgframework.Genesis").resolve(System.getProperty("profile","no-profile"));
		} else
			return home.resolve("genesis").resolve(System.getProperty("profile","no-profile"));
	}

}
