package org.prelle.rollbot;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.RollResult;

/**
 * @author prelle
 *
 */
public class TestParser {

	//-------------------------------------------------------------------
	@Test
	public void testDieWithoutQuantifier() {
		List<ParsedElement> result = Parser.parseStep1("d6");
		assertNotNull(result);
		System.out.println("testDieWithoutQuantifier: "+result);
		assertEquals(2, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(1).type);
		assertEquals("D", result.get(0).rawValue);
		assertEquals(6  , result.get(1).rawValue);
		
//		Parser.parse("d6");
	}

	//-------------------------------------------------------------------
	@Test
	public void testDieWithQuantifier() {
		List<ParsedElement> result = Parser.parseStep1("3w8");
		assertNotNull(result);
		System.out.println("testDieWithQuantifier: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER  , result.get(0).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(2).type);
		assertEquals(  3, result.get(0).rawValue);
		assertEquals("W", result.get(1).rawValue);
		assertEquals(  8, result.get(2).rawValue);
		
		Parser.parseStep2(result);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(Operator.DICE, result.get(1).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnly() {
		List<ParsedElement> result = Parser.parseStep1("2");
		assertNotNull(result);
		System.out.println("testNumberOnly: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(2, result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnlyMinus() {
		List<ParsedElement> result = Parser.parseStep1("-2");
		assertNotNull(result);
		System.out.println("testNumberOnlyMinus: "+result);
		assertEquals(2, result.size());
		assertEquals(ParsedElementType.OPERATOR, result.get(0).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(1).type);
		assertEquals(Operator.MINUS, result.get(0).rawValue);
		assertEquals( 2, result.get(1).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnlyPlus() {
		List<ParsedElement> result = Parser.parseStep1("+3");
		assertNotNull(result);
		System.out.println("testNumberOnlyPlus: "+result);
		assertEquals(2, result.size());
		assertEquals(ParsedElementType.OPERATOR, result.get(0).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(1).type);
		assertEquals(Operator.PLUS, result.get(0).rawValue);
		assertEquals( 3, result.get(1).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testOperationSimple() {
		List<ParsedElement> result = Parser.parseStep1("2+3");
		assertNotNull(result);
		System.out.println("testOperationSimple: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
		assertEquals(2, result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(3, result.get(2).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testOperationPlusMinus() {
		List<ParsedElement> result = Parser.parseStep1("2+-3");
		assertNotNull(result);
		System.out.println("testOperationPlusMinus: "+result);
		assertEquals(4, result.size());
		assertEquals(ParsedElementType.NUMBER  , result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(2).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(3).type);
		assertEquals(2, result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(Operator.MINUS, result.get(2).rawValue);
		assertEquals(3, result.get(3).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariable() {
		List<ParsedElement> result = Parser.parseStep1("HELLO");
		assertNotNull(result);
		System.out.println("testVariable: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals("HELLO", result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableWithSpec() {
		List<ParsedElement> result = Parser.parseStep1("Feuerwaffen/Pistolen");
		assertNotNull(result);
		System.out.println("testVariableWithSpec: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals("FEUERWAFFEN/PISTOLEN", result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableOperator() {
		List<ParsedElement> result = Parser.parseStep1("HELLO+3");
		assertNotNull(result);
		System.out.println("testVariableOperator: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
		assertEquals("HELLO", result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(3, result.get(2).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableOperator2() {
		List<ParsedElement> result = Parser.parseStep1("Feuerwaffen/Pistolen + GES + Edge");
		assertNotNull(result);
		System.out.println("testVariableOperator2: "+result);
		assertEquals(5, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(2).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(3).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(4).type);
	}

	//-------------------------------------------------------------------
	@Test
	public void testDieWithMod() {
		List<ParsedElement> result = Parser.parseStep1("2d6+4");
		assertNotNull(result);
		System.out.println("testDieWithMod: "+result);
		assertEquals(5, result.size());
		assertEquals(ParsedElementType.NUMBER  , result.get(0).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(2).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(3).type);
		assertEquals(ParsedElementType.NUMBER  , result.get(4).type);
	}

	//-------------------------------------------------------------------
	@Test
	public void testGroup1() {
		List<ParsedElement> result = Parser.parseStep1("(3)");
		assertNotNull(result);
		System.out.println("testGroup1: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.GROUP  , result.get(0).type);
	}

	//-------------------------------------------------------------------
	@Test
	public void testGroup2() {
		List<ParsedElement> result = Parser.parseStep1("1+(4)");
		assertNotNull(result);
		System.out.println("testGroup2: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER  , result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.GROUP   , result.get(2).type);
	}

	//-------------------------------------------------------------------
	@Test
	public void testGroup3() {
		List<ParsedElement> result = Parser.parseStep1("4*(3d6+2)");
		assertNotNull(result);
		System.out.println("testGroup3: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER  , result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.GROUP   , result.get(2).type);
	}

}
