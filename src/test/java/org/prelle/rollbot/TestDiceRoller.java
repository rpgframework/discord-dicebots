package org.prelle.rollbot;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.RollResult;

/**
 * @author prelle
 *
 */
public class TestDiceRoller {

//	//-------------------------------------------------------------------
//	@Test
//	public void testDieWithoutQuantifier() {
//		List<ParsedElement> result = DiceRoller.parse("d6");
//		assertNotNull(result);
//		System.out.println("testDieWithoutQuantifier: "+result);
//		assertEquals(1, result.size());
//		assertEquals(ParsedElementType.DICE, result.get(0).type);
//		assertEquals(6, result.get(0).rawValue);
////		assertEquals(1, result.get(0).dieCount);
//		
////		Parser.parse("d6");
//	}
//
//	//-------------------------------------------------------------------
//	@Test
//	public void testDieWithQuantifier() {
//		List<ParsedElement> result = DiceRoller.parse("3w8");
//		assertNotNull(result);
//		System.out.println("testDieWithQuantifier: "+result);
//		assertEquals(1, result.size());
//		assertEquals(ParsedElementType.DICE, result.get(0).type);
//		assertEquals(8, result.get(0).rawValue);
////		assertEquals(3, result.get(0).dieCount);
//	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnly() {
		List<ParsedElement> result = DiceRoller.parse("2");
		assertNotNull(result);
		System.out.println("testNumberOnly: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(2, result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnlyMinus() {
		List<ParsedElement> result = DiceRoller.parse("-2");
		assertNotNull(result);
		System.out.println("testNumberOnlyMinus: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(-2, result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testNumberOnlyPlus() {
		List<ParsedElement> result = DiceRoller.parse("+3");
		assertNotNull(result);
		System.out.println("testNumberOnlyPlus: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(3, result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testOperationSimple() {
		List<ParsedElement> result = DiceRoller.parse("2+3");
		assertNotNull(result);
		System.out.println("testOperationSimple: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
		assertEquals(2, result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(3, result.get(2).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testOperationPlusMinus() {
		List<ParsedElement> result = DiceRoller.parse("2+-3");
		assertNotNull(result);
		System.out.println("testOperationPlusMinus: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.NUMBER, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
		assertEquals(2, result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(-3, result.get(2).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariable() {
		List<ParsedElement> result = DiceRoller.parse("HELLO");
		assertNotNull(result);
		System.out.println("testVariable: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals("HELLO", result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableWithSpec() {
		List<ParsedElement> result = DiceRoller.parse("Feuerwaffen/Pistolen");
		assertNotNull(result);
		System.out.println("testVariableWithSpec: "+result);
		assertEquals(1, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals("FEUERWAFFEN/PISTOLEN", result.get(0).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableOperator() {
		List<ParsedElement> result = DiceRoller.parse("HELLO+3");
		assertNotNull(result);
		System.out.println("testVariableOperator: "+result);
		assertEquals(3, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
		assertEquals("HELLO", result.get(0).rawValue);
		assertEquals(Operator.PLUS, result.get(1).rawValue);
		assertEquals(3, result.get(2).rawValue);
	}

	//-------------------------------------------------------------------
	@Test
	public void testVariableOperator2() {
		List<ParsedElement> result = DiceRoller.parse("Feuerwaffen/Pistolen + GES + Edge");
		assertNotNull(result);
		System.out.println("testVariableOperator2: "+result);
		assertEquals(5, result.size());
		assertEquals(ParsedElementType.VARIABLE, result.get(0).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(2).type);
		assertEquals(ParsedElementType.OPERATOR, result.get(3).type);
		assertEquals(ParsedElementType.VARIABLE, result.get(4).type);
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void testDieWithMod() {
//		List<ParsedElement> result = DiceRoller.parse("2d6+4");
//		assertNotNull(result);
//		System.out.println("testDieWithMod: "+result);
//		assertEquals(3, result.size());
//		assertEquals(ParsedElementType.DICE, result.get(0).type);
//		assertEquals(6, result.get(0).rawValue);
////		assertEquals(2, result.get(0).dieCount);
//		assertEquals(ParsedElementType.OPERATOR, result.get(1).type);
//		assertEquals(ParsedElementType.NUMBER, result.get(2).type);
//		assertEquals(Operator.PLUS, result.get(1).rawValue);
//		assertEquals(4, result.get(2).rawValue);
//	}

}
