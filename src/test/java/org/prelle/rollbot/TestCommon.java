package org.prelle.rollbot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author prelle
 *
 */
public class TestCommon {

	//-------------------------------------------------------------------
	@Test
	public void test() {
		RollResult result = DiceRoller.execute("d6", null, null);
		assertNotNull(result.getDiceWithoutGroups().size());
		assertEquals(1, result.getDiceWithoutGroups().size());
	}

	//-------------------------------------------------------------------
	@Test
	public void test2() {
		RollResult result = DiceRoller.execute("3d8 !EXPLODE", null, null);
		assertNotNull(result.getDiceWithoutGroups().size());
		int dicesRolled = result.getDiceWithoutGroups().size();
		assertTrue(3<=result.getDiceWithoutGroups().size());
		assertEquals(1, result.getRawElements().size());
		assertNotNull(result.getRawElements().get(0).getValue());
		assertTrue(result.getRawElements().get(0).getValue()>=3);
		assertTrue(result.getRawElements().get(0).getValue()<=(dicesRolled*8));
	}

	//-------------------------------------------------------------------
	@Test
	public void test3() {
		RollResult result = DiceRoller.execute("1d6+4", null, null);
		assertNotNull(result.getDiceWithoutGroups().size());
		assertEquals(1, result.getDiceWithoutGroups().size());
		assertEquals(1, result.getRawElements().size());
		assertNotNull(result.getRawElements().get(0).getValue());
		assertTrue(result.getRawElements().get(0).getValue()>=5);
		assertTrue(result.getRawElements().get(0).getValue()<=10);
	}

	//-------------------------------------------------------------------
	@Test
	public void test4() {
		RollResult result = DiceRoller.execute("2w10+3", null, null);
		assertNotNull(result.getDiceWithoutGroups().size());
		assertEquals(2, result.getDiceWithoutGroups().size());
		assertEquals(1, result.getRawElements().size());
		assertNotNull(result.getRawElements().get(0).getValue());
		assertTrue(result.getRawElements().get(0).getValue()>=5);
		assertTrue(result.getRawElements().get(0).getValue()<=23);
	}

}
