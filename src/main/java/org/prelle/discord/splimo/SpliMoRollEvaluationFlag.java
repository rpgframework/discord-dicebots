package org.prelle.discord.splimo;

import org.prelle.rollbot.RollEvaluationFlag;

/**
 * @author prelle
 *
 */
public enum SpliMoRollEvaluationFlag implements RollEvaluationFlag {

	TRIUMPH,
	PATZER
	
}
