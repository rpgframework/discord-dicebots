package org.prelle.discord.splimo;

/**
 * @author prelle
 *
 */
public enum UserDataKey {

	SKILL_VALUE,
	ATTRIBUTE_VALUE
	
}
