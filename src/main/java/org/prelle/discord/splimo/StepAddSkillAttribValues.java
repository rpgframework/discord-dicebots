package org.prelle.discord.splimo;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.AttributeValue;
import org.prelle.splimo.Skill;
import org.prelle.splimo.SkillValue;
import org.prelle.splimo.SpliMoCharacter;

/**
 * Add the primary attribute to the roll command, should there be
 * a skill variable without an attribute present
 * @author prelle
 *
 */
public class StepAddSkillAttribValues implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.splimo");
	
	private SpliMoCharacter model;
	
	//-------------------------------------------------------------------
	public StepAddSkillAttribValues(SpliMoCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.PLAUSIBILITY_LATE_CHECK;
	}

	//-------------------------------------------------------------------
	/**
	 * If any resolved variable is the attribute EDGE, add the WILD flag
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		List<String> messageElements = new ArrayList<String>(); 
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.getType()==ParsedElementType.VARIABLE) {
				logger.info("Check "+elem.getRawValue()+" = "+elem.getRawValue().getClass());
				if (elem.getRawValue() instanceof Skill) {
					Skill skill = (Skill) elem.getRawValue();
					SkillValue sVal = model.getSkillValue(skill);
//					if (sVal==null && skill.isUseUntrained()) {
//						sVal = new SkillValue(skill, -1);
//					}
					if (sVal!=null)
						value.setUserData(UserDataKey.SKILL_VALUE.name(), sVal);
					logger.debug("Add SkillValue "+sVal);
					messageElements.add(skill.getName()+"("+sVal.getModifiedValue()+")");
				} else if (elem.getRawValue() instanceof Attribute) {
					Attribute attrib = (Attribute) elem.getRawValue();
					AttributeValue aVal = model.getAttribute(attrib);
					value.setUserData(UserDataKey.ATTRIBUTE_VALUE.name(), aVal);
					logger.debug("Add AttributeValue "+aVal);
					messageElements.add(attrib.getName()+"("+aVal.getValue()+")");
				}
			}
		}
		if (!messageElements.isEmpty()) {
			logger.info("Set message to "+String.join(" + ", messageElements));
			value.setMessage(String.join(" + ", messageElements));
		}
	}

}
