package org.prelle.discord.splimo;

import org.prelle.rollbot.DiceEvaluationFlag;

/**
 * @author prelle
 *
 */
public enum SpliMoDiceEvaluationFlags implements DiceEvaluationFlag {

	SICHERHEIT,
	RISIKO,

}
