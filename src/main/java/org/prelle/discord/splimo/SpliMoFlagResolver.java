package org.prelle.discord.splimo;

import java.util.function.Function;

import org.prelle.rollbot.ExecutionFlags;

/**
 * @author prelle
 *
 */
public class SpliMoFlagResolver implements Function<String, ExecutionFlags> {

	//-------------------------------------------------------------------
	/**
	 * @see java.util.function.Function#apply(java.lang.Object)
	 */
	@Override
	public ExecutionFlags apply(String val) {
		try {
			return SpliMoExecutionFlags.valueOf(val);
		} catch (Exception e) {
			return null;
		}
	}

}
