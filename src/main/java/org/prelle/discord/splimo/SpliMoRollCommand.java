package org.prelle.discord.splimo;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.PerGuildConfig;
import org.prelle.discord.splimo.SpliMoDiceGraphicGenerator.DiceRollResult;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.NoLinkedCharacterException;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.ResolverException;
import org.prelle.rollbot.RollResult;
import org.prelle.rollbot.TooManyDiceException;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.splimo.Attribute;
import org.prelle.splimo.AttributeValue;
import org.prelle.splimo.Skill;
import org.prelle.splimo.SkillValue;
import org.prelle.splimo.SplitterMondCore;
import org.prelle.splimo.SplitterTools;
import org.prelle.splimo.modifications.AttributeModification;
import org.prelle.splimo.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.Command.Choice;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;

/**
 * @author prelle
 *
 */
public class SpliMoRollCommand implements BotCommand<SpliMoVolatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	
	private SpliMoBot bot;
	
	public static PhaseHook[] diceHooks= new PhaseHook[] {
//			new StepAddWildFlagOnEdge(),
//			new StepAddAttributeToSkill(),
//			new StepAddExplodeOnEdge(),
//			new StepNoDiceOpIsD6(),
//			new StepMarkWildDie(),
//			new StepEvaluateHits()
	};
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(SpliMoRollCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public SpliMoRollCommand(SpliMoBot bot) {
		this.bot = bot;
//		diceHooks = new PhaseHook[] {
//				new StepAddWildFlagOnEdge(),
//				new StepAddAttributeToSkill(),
//				new StepAddExplodeOnEdge(),
//				new StepNoDiceOpIsD6(),
//				new StepMarkWildDie(),
//				new StepEvaluateHits()
//		};
		
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandData(java.util.Locale)
	 */
	@Override
	public CommandData getCommandData(Locale loc) {
		CommandData roll = new CommandData("roll", getTranslation(loc, "command.roll.desc"));
		OptionData wert  = new OptionData(OptionType.INTEGER, "wert", getTranslation(loc, "command.roll.param.value"), true);
		wert.setMinValue(0);
		wert.setMaxValue(30);
		OptionData ziel  = new OptionData(OptionType.INTEGER, "ziel", getTranslation(loc, "command.roll.param.target"), true);
		ziel.setMinValue(10);
		ziel.setMaxValue(60);
		OptionData type  = new OptionData(OptionType.INTEGER,  "art" , getTranslation(loc, "command.roll.param.type"), false);
		type.addChoice("normal", 1);
		type.addChoice("risiko", 2);
		type.addChoice("sicher", 3);
		OptionData gross = new OptionData(OptionType.BOOLEAN,  "großmeister" , getTranslation(loc, "command.roll.param.gross"), false);
		// ToDo: Min/Max
		roll.addOptions(wert);
		roll.addOptions(ziel);
		roll.addOptions(type);
		roll.addOptions(gross);
		return roll;
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "roll."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
//	@Override
//	public void processOld(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
//		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
//		MessageChannel channel = event.getChannel();
//		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
//		
//		// Check if the user input shall be cleaned after some time
//		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
//			bot.expireMessage(event.getMessage(), Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
//		}
//
//		/*
//		 * Now process depending on variant
//		 */
//		int pool = Integer.parseInt(matcher.group(1));
//		int edge = 0;
//		int wild = 0;
//		
//		if (cmd==ROLL_WILD)  {
//			wild = Integer.parseInt(matcher.group(2));
//		} else if (cmd==ROLL_EDGE)  {
//			wild = Integer.parseInt(matcher.group(2));
//			edge = Integer.parseInt(matcher.group(3));
//		}
//		
//		String name = event.getAuthor().getName();
//		try { name = event.getGuild().retrieveMember(event.getAuthor()).complete().getNickname(); } catch (Exception e) {}
//		EmbedBuilder embed = new EmbedBuilder();
//		embed.setTitle(name);
//		DiceRollResult ret = SpliMoDiceGraphicGenerator.generate(embed, pool, wild, edge);
//		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
//		.setDescription(ret.message)
//		.setColor(255*65536 + 0*256 + 0xA0)
//		.setThumbnail("attachment://success.png")
//		;
//		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//			embed.setFooter("This message is going to destroy itself in "+SpliMoBot.TIMEOUT_ROLL_RESULT+" minutes");
//			bot.expireMessage(event.getMessage(), Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
//		}
//		
//		logger.info("Send");
//		channel.sendFile(ret.image, "result.png")
//		.addFile(ret.thumbnail, "success.png")
//		.embed(embed.build())
//		.queue(message -> {
//			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//				bot.expireMessage(message, Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
//			}
//			logger.info("Adding reactions");
////			message.addReaction(Emote.ICON_URL);
////			message.addReaction("U+0031U+20E3").queue(); // 1
////			message.addReaction("U+0032U+20E3").queue(); // 2
////			message.addReaction("U+0033U+20E3").queue(); // 3
////			message.addReaction("U+0034U+20E3").queue(); // 4
////			message.addReaction("U+0035U+20E3").queue(); // 5
////			if (guild!=null) {
////				try {
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND1EDGE)).queue();
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND2EDGE)).queue();
////				} catch (Exception e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				}
////			}
//		});
//
////		ReactionWaitObject obj = bot.getRecentWaitObject(channel);
////		if (obj!=null) {
////			obj.command.onUserGeneratedResult(obj.variant, obj.message, event.getAuthor(), ret, obj.userData);
////		}
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.info("Process variant "+cmd.getId()+" with "+matcher+" from "+event.getAuthor());
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
			bot.expireMessage(event.getMessage(), Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}

		/*
		 * Now process depending on variant
		 */
		List<PhaseHook> list = (new ArrayList<PhaseHook>(Arrays.asList(diceHooks)));
		list.add(new StepAddSkillAttribValues(bot.getLinkedCharacter(event.getAuthor())));
		PhaseHook[] enhancedHooks = list.toArray(new PhaseHook[list.size()]);
		String rollCommand = matcher.group(1);
		RollResult result = null;
		try {
			result = DiceRoller.execute(rollCommand, new CharacterVariableResolver(bot.getLinkedCharacter(event.getAuthor())), new SpliMoFlagResolver(), enhancedHooks);
		} catch (TooManyDiceException re) {
			String error = getTranslation(locale, "error.too_many_dice", event.getAuthor().getAsMention());
			channel.sendMessage(error).queue();
			return;
		} catch (ResolverException re) {
			String error = getTranslation(locale, "error.unknown_variable", re.getVariable());
			for (Skill tmp : SplitterMondCore.getSkills()) {
				error += "\n* "+tmp.getId()+" or '"+CharacterVariableResolver.getNameWithoutWhitespace(tmp.getName())+"'";
			}
			for (Attribute tmp : Attribute.primaryValues()) {
				error += "\n* "+tmp.getName()+"'";
			}
			channel.sendMessage(error).queue();
			return;
		} catch (NoLinkedCharacterException nlc) {
			String error = getTranslation(locale, "error.no_linked_character");
			channel.sendMessage(error).queue();
			return;
			
		}
		
		String name = event.getAuthor().getName();
		try { name = event.getGuild().retrieveMember(event.getAuthor()).complete().getNickname(); } catch (Exception e) {}
		showMessage(bot, result, event.getAuthor(), name, channel);
		
//		List<String> conds = new ArrayList<String>();
//		List<String> other = new ArrayList<String>();
//		// Skill
//		if (result.getUserData(SkillValue.class, UserDataKey.SKILL_VALUE.name())!=null) {
//			SkillValue val = result.getUserData(SkillValue.class, UserDataKey.SKILL_VALUE.name());
//			for (Modification mod : val.getModifications()) {
//				if (mod instanceof SkillModification) {
//					SkillModification smod = (SkillModification)mod;
//					if (smod.isConditional()) {
//						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
//					}
//				} else {
//					other.add("**"+ShadowrunTools.getModificationSourceStringWithPage(mod.getSource())+"**");
//				}
//			}
//		}
//		// Attribute
//		if (result.getUserData(SkillValue.class, UserDataKey.ATTRIBUTE_VALUE.name())!=null) {
//			AttributeValue val = result.getUserData(AttributeValue.class, UserDataKey.ATTRIBUTE_VALUE.name());
//			for (Modification mod : val.getModifications()) {
//				if (mod instanceof AttributeModification) {
//					AttributeModification smod = (AttributeModification)mod;
//					if (smod.isConditional()) {
//						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
//					}
//				} else {
//					other.add("**"+ShadowrunTools.getModificationSourceStringWithPage(mod.getSource())+"**");
//				}
//			}
//		}
//
//		boolean glitch = result.hasFlag(SR6RollEvaluationFlag.GLITCH) || result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH);
//		int hits = result.getValue();
//		boolean success = hits>0 && !glitch;
//		byte[] thumbnail = SR6DiceGraphicGenerator.generateEGImage(success, hits, glitch);
//		byte[] image = SR6DiceGraphicGenerator.generateNew(result.getDiceWithoutGroups());
//
//		String descr = result.toString();
//		if (result.getMessage()!=null)
//			descr = result.getMessage();
//		if (glitch) {
//			if (result.hasFlag(SR6RollEvaluationFlag.GLITCH))
//				descr+="\n**GLITCH**";
//			else if (result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH))
//				descr+="\n**CRITICAL GLITCH**";		
//		}
//		
//		String name = event.getAuthor().getName();
//		try { name = event.getGuild().retrieveMember(event.getAuthor()).complete().getNickname(); } catch (Exception e) {}
//		EmbedBuilder embed = new EmbedBuilder();
//		embed.setTitle(name);
////		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, wild, edge);
//		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
//		.setDescription(descr)
//		.setColor(255*65536 + 0*256 + 0xA0)
//		.setThumbnail("attachment://success.png")
//		;
//		if (!conds.isEmpty()) {
//			embed.addField("Conditional", String.join(", ", conds), true);
//		}
//		if (!other.isEmpty()) {
//			embed.addField("Also relevant", String.join(", ", other), true);
//		}
//
//		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//			embed.setFooter("This message is going to destroy itself in "+SR6Bot.TIMEOUT_ROLL_RESULT+" minutes");
//			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
//		}
//		
//		logger.info("Send");
//		channel.sendFile(image, "result.png")
//		.addFile(thumbnail, "success.png")
//		.embed(embed.build())
//		.queue(message -> {
//			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//				bot.expireMessage(message, Instant.now().plus(SR6Bot.TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
//			}
//			logger.info("Adding reactions");
////			message.addReaction(Emote.ICON_URL);
////			message.addReaction("U+0031U+20E3").queue(); // 1
////			message.addReaction("U+0032U+20E3").queue(); // 2
////			message.addReaction("U+0033U+20E3").queue(); // 3
////			message.addReaction("U+0034U+20E3").queue(); // 4
////			message.addReaction("U+0035U+20E3").queue(); // 5
////			if (guild!=null) {
////				try {
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND1EDGE)).queue();
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND2EDGE)).queue();
////				} catch (Exception e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				}
////			}
//		});
//
////		ReactionWaitObject obj = bot.getRecentWaitObject(channel);
////		if (obj!=null) {
////			obj.command.onUserGeneratedResult(obj.variant, obj.message, event.getAuthor(), ret, obj.userData);
////		}

		/*
		 * Check if the player was expected to make a roll
		 */
//		SR6Bot.ReactionWaitObject obj = bot.getRecentWaitObject(channel);
//		if (obj!=null) {
//			obj.command.onUserGeneratedResult(obj.variant, message, event.getAuthor(), result, obj.userData);
//		}
//		if (channel instanceof TextChannel) {
//			Guild guild = ((TextChannel)channel).getGuild();
//			DiscordGamingSession<SpliMoVolatile> sess = bot.getSession(guild, channel);
//			if (sess!=null && sess.getCheckSession()!=null) {
//				CheckSession session = sess.getCheckSession();
//				if (result==null) {
//					session.results.put(event.getAuthor(), "?");
//				} else {
//					String add = "";
//					if (result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH)) {
//						add="("+getTranslation(session.locale, "label.critical_glitch")+")";
//					} else if (result.hasFlag(SR6RollEvaluationFlag.GLITCH)) {
//						add="("+getTranslation(session.locale, "label.glitch")+")";
//					}
//					session.results.put(event.getAuthor(), "["+result.getValue()+"]"+add);
//				}
//				logger.warn("TODO: update Check message after normal roll");
//			}
//		}
	}

	//-------------------------------------------------------------------
	public static void showMessage(SpliMoBot bot, RollResult result, User user, String name, MessageChannel channel) {
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);

		List<String> conds = new ArrayList<String>();
		List<String> other = new ArrayList<String>();
		// Skill
		if (result.getUserData(SkillValue.class, UserDataKey.SKILL_VALUE.name())!=null) {
			SkillValue val = result.getUserData(SkillValue.class, UserDataKey.SKILL_VALUE.name());
			for (Modification mod : val.getModifications()) {
				if (mod instanceof SkillModification) {
					SkillModification smod = (SkillModification)mod;
					if (smod.isConditional()) {
						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ""+smod.getSource()));
					}
				} else {
					other.add("**"+ShadowrunTools.getModificationSourceStringWithPage(mod.getSource())+"**");
				}
			}
		}
		// Attribute
		if (result.getUserData(SkillValue.class, UserDataKey.ATTRIBUTE_VALUE.name())!=null) {
			AttributeValue val = result.getUserData(AttributeValue.class, UserDataKey.ATTRIBUTE_VALUE.name());
			for (Modification mod : val.getModifications()) {
				if (mod instanceof AttributeModification) {
					AttributeModification smod = (AttributeModification)mod;
					if (smod.isConditional()) {
						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
					}
				} else {
					other.add("**"+ShadowrunTools.getModificationSourceStringWithPage(mod.getSource())+"**");
				}
			}
		}

		boolean glitch = result.hasFlag(SpliMoRollEvaluationFlag.PATZER);
		int hits = result.getValue();
		boolean success = hits>0 && !glitch;
		byte[] thumbnail = SpliMoDiceGraphicGenerator.generateEGImage(success, hits);
		byte[] image = SpliMoDiceGraphicGenerator.generateNew(result.getDiceWithoutGroups());

		String descr = result.toString();
		if (result.getMessage()!=null)
			descr = result.getMessage();
		if (glitch) {
			if (result.hasFlag(SpliMoRollEvaluationFlag.PATZER))
				descr+="\n**PATZER**";
		}
		
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(name);
//		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, wild, edge);
		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
		.setDescription(descr)
		.setColor(255*65536 + 0*256 + 0xA0)
		.setThumbnail("attachment://success.png")
		;
		if (!conds.isEmpty()) {
			embed.addField("Conditional", String.join(", ", conds), true);
		}
		if (!other.isEmpty()) {
			embed.addField("Also relevant", String.join(", ", other), true);
		}

		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
			embed.setFooter("This message is going to destroy itself in "+SpliMoBot.TIMEOUT_ROLL_RESULT+" minutes");
//			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		
		logger.info("Send");
		channel.sendFile(image, "result.png")
		.addFile(thumbnail, "success.png")
		.embed(embed.build())
		.queue(message -> {
			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
				bot.expireMessage(message, Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
			}
			
			SpliMoBot.ReactionWaitObject obj = bot.getRecentWaitObject(channel);
			logger.info("Reaction object = "+obj);
			if (obj!=null) {
				logger.info("cmd = "+obj.variant);
				obj.command.onUserGeneratedResult(obj.variant, message, user, result, obj.userData);
			}
			logger.info("Adding reactions");
//			message.addReaction(Emote.ICON_URL);
//			message.addReaction("U+0031U+20E3").queue(); // 1
//			message.addReaction("U+0032U+20E3").queue(); // 2
//			message.addReaction("U+0033U+20E3").queue(); // 3
//			message.addReaction("U+0034U+20E3").queue(); // 4
//			message.addReaction("U+0035U+20E3").queue(); // 5
//			if (guild!=null) {
//				try {
//					message.addReaction(getGuildConfig(guild).getEmote(SPEND1EDGE)).queue();
//					message.addReaction(getGuildConfig(guild).getEmote(SPEND2EDGE)).queue();
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
		});
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onSlashCommand(net.dv8tion.jda.api.events.interaction.SlashCommandEvent)
	 */
	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		logger.info("Process "+event.getOptions());
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
//		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
//			bot.expireMessage(event.getMessage(), Instant.now().plus(SpliMoBot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
//		}
		Locale locale = Locale.forLanguageTag(guildConfig.getLanguage());
		
		int diceCount = 2;
		if (event.getOption("art")!=null) {		
			int type = (int)event.getOption("art").getAsLong();
			switch (type) {
			case 1: diceCount=2; break;
			case 2: diceCount=2; break;
			case 3: diceCount=4; break;
			}
		}
		// Großmeister
		
		// Wert
		int value = (int)event.getOption("wert").getAsLong();
		String valueS = (value>0)?(" + "+value):(" "+value);
		
		/*
		 * Now process depending on variant
		 */
		List<PhaseHook> list = (new ArrayList<PhaseHook>(Arrays.asList(diceHooks)));
		list.add(new StepAddSkillAttribValues(bot.getLinkedCharacter(event.getUser())));
		PhaseHook[] enhancedHooks = list.toArray(new PhaseHook[list.size()]);
		String rollCommand = diceCount+"d6"+valueS;
		RollResult result = null;
		try {
			result = DiceRoller.execute(rollCommand, new CharacterVariableResolver(bot.getLinkedCharacter(event.getUser())), new SpliMoFlagResolver(), enhancedHooks);
		} catch (TooManyDiceException re) {
			String error = getTranslation(locale, "error.too_many_dice", event.getUser().getAsMention());
			channel.sendMessage(error).queue();
			return;
		} catch (ResolverException re) {
			String error = getTranslation(locale, "error.unknown_variable", re.getVariable());
			for (Skill tmp : SplitterMondCore.getSkills()) {
				error += "\n* "+tmp.getId()+" or '"+CharacterVariableResolver.getNameWithoutWhitespace(tmp.getName())+"'";
			}
			for (Attribute tmp : Attribute.primaryValues()) {
				error += "\n* "+tmp.getName()+"'";
			}
			channel.sendMessage(error).queue();
			return;
		} catch (NoLinkedCharacterException nlc) {
			String error = getTranslation(locale, "error.no_linked_character");
			channel.sendMessage(error).queue();
			return;
			
		}
		
		String name = event.getUser().getName();
		try { name = event.getGuild().retrieveMember(event.getUser()).complete().getNickname(); } catch (Exception e) {}
		showMessage(bot, result, event.getUser(), name, channel);
	}

}
