package org.prelle.discord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.splimo.SpliMoBot;
import org.prelle.discord.sr6.SR6Bot;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class GraphicalDiceBot extends Application {
	
	private final static Logger logger = LogManager.getLogger("dicebot");

	private final static String SLASH = System.getProperty("file.separator");
	private final static String CONFIGNAME= "dicebot.properties";
	private final static File[] TO_CHECK = new File[]{
			new File(SLASH+"etc"+SLASH+"eden"+SLASH+CONFIGNAME),
			new File(SLASH+"etc"+SLASH+CONFIGNAME),
			new File(System.getProperty("user.home")+SLASH+"etc"+SLASH+CONFIGNAME),
			new File(System.getProperty("user.dir")+SLASH+CONFIGNAME),
	};
	
	private Map<Class<? extends BotSkeleton>, BotSkeleton> bots;
	private SR6Bot botSR6;
	private SpliMoBot botSpliMo;

	//-------------------------------------------------------------------
   public static void main(String[] args) {
//	   System.setProperty("testfx.robot", "glass");
//       System.setProperty("testfx.headless", "true");
       Application.launch(args);
    }

	//-------------------------------------------------------------------
	/**
	 */
	@SuppressWarnings("unchecked")
	public GraphicalDiceBot() {
		/*
		 * Configuration
		 */
		Properties pro = new Properties();
		boolean noConfigFound = true;
		
		// try local
		InputStream ins = ClassLoader.getSystemResourceAsStream(CONFIGNAME);
		try {
			if (ins!=null) {
				System.out.println("Loading config from local resource "+ClassLoader.getSystemResource(CONFIGNAME));
				pro.load(ins);
				noConfigFound = false;
			} else {
				for (File file : TO_CHECK) {
					if (file.exists()) {
						System.out.println("Loading config from "+file);
						pro.load(new FileReader(file));
						//				PropertyConfigurator.configure(pro);
						logger.info("Loaded config from "+file);
						noConfigFound = false;
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		if (noConfigFound) {
			logger.fatal("No configuration found under "+Arrays.toString(TO_CHECK));
			System.err.println("No configuration found under "+Arrays.toString(TO_CHECK));
		}

		/*
		 * Process configuration
		 */
		String dbURL  = pro.getProperty("jdbc.url","jdbc:hsqldb:mem:testdb");
		String dbUser = pro.getProperty("jdbc.user","SA");
		String dbPass = pro.getProperty("jdbc.pass","");
		DiscordSQL database = null; 
		try {
			database = new DiscordSQL(dbURL, dbUser, dbPass);
		} catch (SQLException e1) {
			logger.fatal("Failed connecting to database at "+dbURL,e1);
			System.exit(1);
		}
		
		
		bots = new HashMap<Class<? extends BotSkeleton>, BotSkeleton>();
		   
		for (Object keyObj : pro.keySet()) {
			String key = (String)keyObj;
			if (key.startsWith("class.")) {
				String className = pro.getProperty(key);
				try {
					logger.info("Initialize dice bot: "+className);
					Class<? extends BotSkeleton> clazz = (Class<? extends BotSkeleton>) Class.forName(className);
					String token = pro.getProperty("token."+key.substring(6));
					logger.info("  Token = "+token);
					Constructor<BotSkeleton<?>> constr = (Constructor<BotSkeleton<?>>) clazz.getConstructor(String.class, DiscordSQL.class, Properties.class);
					BotSkeleton<VolatileCharacterData> bot = (BotSkeleton<VolatileCharacterData>) constr.newInstance(token, database, pro);
					bots.put(clazz, bot);
				} catch (ClassNotFoundException e) {
					logger.fatal("Class Not Found: "+className);
				} catch (NoSuchMethodException e) {
					logger.fatal("Failed getting constructor: "+e);
				} catch (InvocationTargetException | IllegalAccessException | InstantiationException | IllegalArgumentException e) {
					logger.fatal("Failed calling constructor: "+e,e);
				}
			}
		}
//		botSR6 = new SR6Bot();
//		botSpliMo = new SpliMoBot();
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub

	}

}
