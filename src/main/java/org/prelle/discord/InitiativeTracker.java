package org.prelle.discord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author prelle
 *
 */
public class InitiativeTracker<T> {
	
	public class IniPhase implements Comparable<IniPhase> {
		public Integer ini;
		public List<T> list;
		public IniPhase(Integer ini, List<T> list) {
			this.ini = ini;
			this.list = list;
		}
		@Override
		public int compareTo(InitiativeTracker<T>.IniPhase o) {
			return -ini.compareTo(o.ini);
		}
	}

	private Map<Integer, List<T>> listsByIni;
	private Comparator<T> comparator;
	
	//-------------------------------------------------------------------
	public InitiativeTracker(Comparator<T> withinIniPhaseComparator) {
		listsByIni = new HashMap<Integer, List<T>>();
		this.comparator = withinIniPhaseComparator;
	}

	//-------------------------------------------------------------------
	public void clear() {
		listsByIni.clear();
	}

	//-------------------------------------------------------------------
	public void remove(T toRemove) {
		for (Entry<Integer, List<T>> entry : (new HashSet<>(listsByIni.entrySet()))) {
			for (T data : (new ArrayList<T>(entry.getValue()))) {
				if (data==toRemove) {
					entry.getValue().remove(data);
					if (entry.getValue().isEmpty()) {
						listsByIni.remove(entry.getKey());
					}
				}
					
			}
		}
		
	}

	//-------------------------------------------------------------------
	public void add(Integer ini, T toAdd) {
		List<T> list = listsByIni.get(ini);
		if (list==null) {
			list = new ArrayList<T>();
			listsByIni.put(ini, list);
		}
		list.add(toAdd);
	}

	//-------------------------------------------------------------------
	public void modify(T toModify, Integer newIni)  {
		remove(toModify);
		add(newIni, toModify);
	}

	//-------------------------------------------------------------------
	public void sort() {
		if (comparator==null)
			return;
		for (List<T> list : listsByIni.values()) {
			Collections.sort(list, comparator);
		}
	}

	//-------------------------------------------------------------------
	public List<IniPhase> getIniPhases() {
		List<IniPhase> ret = new ArrayList<InitiativeTracker<T>.IniPhase>();
		for (Entry<Integer, List<T>> entry : listsByIni.entrySet()) {
			ret.add( new IniPhase(entry.getKey(), new ArrayList<T>(entry.getValue())) );
		}
		Collections.sort(ret);
		return ret;
	}

}
