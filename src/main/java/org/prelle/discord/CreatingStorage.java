package org.prelle.discord;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class CreatingStorage<K,V> {

	private Map<K, V> data;
	private Constructor<V> constructor;
	
	//-------------------------------------------------------------------
	public CreatingStorage(Constructor<V> emptyConst) {
		data = new HashMap<K, V>();
		this.constructor = emptyConst;
	}
	
	//-------------------------------------------------------------------
	public V get(K key) {
		V ret = data.get(key);
		if (ret!=null)
			return ret;
		try {
			ret = constructor.newInstance();
			data.put(key, ret);
			return ret;
		} catch (Exception e) {
			throw new RuntimeException("Failed instantiating object with empty constructor");
		}
	}
	
	//-------------------------------------------------------------------
	public void set(K key, V val) {
		data.put(key, val);
	}

}
