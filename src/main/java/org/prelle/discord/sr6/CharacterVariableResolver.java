package org.prelle.discord.sr6;

import java.util.function.BiFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.NoLinkedCharacterException;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ResolverException;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillSpecializationValue;
import org.prelle.shadowrun6.SkillValue;

/**
 * @author prelle
 *
 */
public class CharacterVariableResolver implements BiFunction<ParsedElement, String, Integer> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private ShadowrunCharacter model;

	//-------------------------------------------------------------------
	public CharacterVariableResolver(ShadowrunCharacter model) {
		this.model = model;
	}

	//-------------------------------------------------------------------
	public static String getNameWithoutWhitespace(String name) {
		StringBuffer ret = new StringBuffer(name);
		while (ret.indexOf(" ")>0) {
			ret.deleteCharAt(ret.indexOf(" "));
		}
		System.err.println("Change ["+name+"] to ["+ret+"]");
		return ret.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.util.function.Function#apply(java.lang.Object)
	 */
	@Override
	public Integer apply(ParsedElement elem, String t) {
		logger.debug("resolve "+t);
		if (model==null)
			throw new NoLinkedCharacterException();
		String key = t.toLowerCase();
		for (Attribute  attr : Attribute.values()) {
			if (attr.getName().toLowerCase().startsWith(key) || attr.getShortName().toLowerCase().equals(key)) {
				logger.info(" "+t+" resolves to attribute "+attr);
				elem.setName(t);
				elem.setRawValue(attr);
				return model.getAttribute(attr).getModifiedValue();
			}
		}
		
		String key1 = key;
		String key2 = null;
		if (key.indexOf("/")>0) {
			key1 = key.substring(0, key.indexOf("/"));
			key2 = key.substring(key.indexOf("/")+1, key.length());
		}
		
		for (Skill skill : ShadowrunCore.getSkills()) {
			String goodName = getNameWithoutWhitespace(skill.getName());
			if (skill.getName().toLowerCase().startsWith(key1) || goodName.toLowerCase().startsWith(key1) || skill.getId().startsWith(key1)) {
				logger.info(" "+t+" resolves to skill "+skill);
				SkillValue sVal = model.getSkillValue(skill);
				if (sVal==null) {
					if (!skill.isUseUntrained())
						throw new SkillNotUsableException(skill);
					sVal = new SkillValue(skill, -1);
				}
				int foo = sVal.getModifiedValue();
				logger.info(" = "+sVal.getPoints()+" / "+foo+" / "+sVal.getModifications()+" / "+sVal.getModifierExplanation());
				if (key2!=null) {
					for (SkillSpecialization spec : skill.getSpecializations()) {
						if (spec.getName().toLowerCase().startsWith(key2)) {
							SkillSpecializationValue specVal = sVal.getSpecialization(spec);
							logger.debug(" = "+specVal);
							if (specVal!=null)
								return foo + (specVal.isExpertise()?3:2);
							elem.setName(t);
							elem.setRawValue(spec);
							return foo;
						}
					}
					throw new ResolverException(t);
				}
				elem.setName(t);
				elem.setRawValue(skill);
				return foo;
			}
			
		}
		
		throw new ResolverException(t);
	}

}
