package org.prelle.discord.sr6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.DiscordGamingSession;
import org.prelle.discord.VolatileCharacterData;
import org.prelle.rollbot.RollResult;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class EdgeCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	protected final static String CONFIRM = "U+2714";
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant EDGE_SET    = new CommandVariant("set"   , " *edge *se(?:t)? ([0-9]) *", "edge set <val>", CommandUser.ANYONE);
	private final static CommandVariant EDGE_MODIFY = new CommandVariant("modify", " *edge *([+-]*[0-9]) *"    , "edge +/-<val>", CommandUser.ANYONE) ;
	private final static CommandVariant EDGE_SHOW   = new CommandVariant("show", " *edge *sh(?:ow|o)? *"  , "edge show", CommandUser.ANYONE) ;
	
	private SR6Bot bot;
	private CommandVariant[] variants;

	public static void main(String[] args) {
		String val = "edge  -1";
		System.out.println("Input: ["+val+"]");
		for (CommandVariant var : new CommandVariant[] {EDGE_SET, EDGE_MODIFY, EDGE_SHOW}) {
		Matcher matcher = var.getPattern().matcher(val);
		System.out.println("RegEx: ["+var.getPattern().pattern()+"]");
		System.out.println(matcher.matches());
		}
	}
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(EdgeCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public EdgeCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				EDGE_MODIFY,
				EDGE_SET,
				EDGE_SHOW
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "edge."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		TextChannel channel = (TextChannel) event.getChannel();
		Guild guild = event.getGuild();
		User author = event.getAuthor();
		DiscordGamingSession<SR6Volatile> session = bot.getSession(guild, channel);
		
		// There must be a running session (meaning: an active gamemaster)
		if (session.getGamemaster()==null) {
			channel.sendMessage("No session running").queue();
			return;
		}
		
		SR6Volatile volat = session.getVolatile(author);
		if (session.getGamemaster()==null) {
			channel.sendMessage(author.getAsMention()+" you are not a player in this session.\nUse **"+bot.getCommandChar(guild)+"session join** to ask for permission to join").queue();
			return;
		}
		
		if (cmd==EDGE_MODIFY) {
			int value = Integer.parseInt(matcher.group(1));
			int newVal = volat.getEdge() + value;
			if (newVal>7) newVal=7;
			if (newVal<0) newVal=0;
			volat.setEdge(newVal);
			channel.sendMessage(author.getAsMention()+", your Edge is now "+volat.getEdge()).queue();;
		} else if (cmd==EDGE_SET) {
			int value = Integer.parseInt(matcher.group(1));
			volat.setEdge(value);
			channel.sendMessage(author.getAsMention()+", your Edge is now "+volat.getEdge()).queue();;
		} else if (cmd==EDGE_SHOW) {
			StringBuffer buf = new StringBuffer("```asciidoc");
			List<SR6Volatile> vols = new ArrayList<SR6Volatile>(session.getVolatiles());
			Collections.sort(vols, new Comparator<VolatileCharacterData>() {
				public int compare(VolatileCharacterData o1, VolatileCharacterData o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			for (SR6Volatile vol : vols) {
				buf.append(String.format("\n%20s  = %d", vol.getName(), vol.getEdge()));
			}
			buf.append("\n```");
			channel.sendMessage(buf.toString()).queue();
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
