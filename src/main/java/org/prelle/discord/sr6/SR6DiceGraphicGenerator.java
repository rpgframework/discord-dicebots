package org.prelle.discord.sr6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.prelle.discord.PngEncoderFX;
import org.prelle.rollbot.CommonDiceEvaluationFlag;
import org.prelle.rollbot.RollResult;
import org.prelle.rollbot.RolledDie;

import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import net.dv8tion.jda.api.EmbedBuilder;

/**
 * @author prelle
 *
 */
public class SR6DiceGraphicGenerator {
	
	//-------------------------------------------------------------------
	private static class DiceResult {
		int face;
		boolean wild;
		boolean ignoredHit;
		boolean exploded;
		public DiceResult(int face, boolean wild) {
			this.face = face;
			this.wild = wild;
		}
	}
	
	public static enum ResultType {
		CRITICAL_GLITCH,
		GLITCH,
		FAILURE,
		SUCCESS,
	}
	
	public static class DiceRollResult {
		byte[] image;
		String message;
		ResultType type; 
		byte[] thumbnail;
		public int hits;
	}
	
	public final static Random RANDOM = new Random();	
	private final static int DICE_PER_ROW = 8;
	
	private static byte[] data;
	private static Map<String,Image> imagesByName = new HashMap<String, Image>();
	
	//-------------------------------------------------------------------
	private static void explode(List<DiceResult> result, boolean wild) {
		DiceResult die = new DiceResult(RANDOM.nextInt(6)+1, false);
		die.exploded = true;
		result.add( die );
		if (die.face==6) {
			explode(result, wild);
		}
	}

	//-------------------------------------------------------------------
	private static List<DiceResult> rollDice(int dice, int wild, int edge) {
		int numNormal = dice-wild + edge;
//		logger.debug("("+dice+","+wild+","+edge+") = "+numNormal+" normal dice");
		List<DiceResult> result = new ArrayList<DiceResult>();
		// Roll regular dices
		for (int i=0; i<numNormal; i++) {
			DiceResult die = new DiceResult(RANDOM.nextInt(6)+1, false); 
			result.add( die );
			if (die.face==6 && edge>0) {
				explode(result, false);
			}
		}
		// Roll wild dices
		for (int i=0; i<wild; i++) {
			DiceResult die = new DiceResult(RANDOM.nextInt(6)+1, true); 
			result.add( die );
			if (die.face==6 && edge>0) {
				explode(result, false);
			}
		}
		
		return result;
	}

	//-------------------------------------------------------------------
	public static byte[] generate(List<DiceResult> result) {
		int rows = (int) Math.ceil(result.size()/(double)DICE_PER_ROW);
		Canvas canvas = new Canvas(DICE_PER_ROW*52, rows*52);
		int x=0;
		int y=0;
		for (DiceResult die : result) {
			canvas.getGraphicsContext2D().drawImage(getResultImage(die), x*52, y*52);
			x++;
			if (x==DICE_PER_ROW) {
				y++;
				x=0;
			}
		}
//		return canvas;
		return exportPngSnapshot(canvas, Color.TRANSPARENT);
	}

	//-------------------------------------------------------------------
	static byte[] generateNew(List<RolledDie> result) {
		int rows = (int) Math.ceil(result.size()/(double)DICE_PER_ROW);
		Canvas canvas = new Canvas(DICE_PER_ROW*52, rows*52);
		int x=0;
		int y=0;
		for (RolledDie die : result) {
			canvas.getGraphicsContext2D().drawImage(getResultImage(die), x*52, y*52);
			x++;
			if (x==DICE_PER_ROW) {
				y++;
				x=0;
			}
		}
//		return canvas;
		return exportPngSnapshot(canvas, Color.TRANSPARENT);
	}

	//-------------------------------------------------------------------
	public static DiceRollResult generate(EmbedBuilder embed, int dice, int wild, int edge) {
		if (dice>40) {
			DiceRollResult ret = new DiceRollResult();
			ret.message="Nice try, stupid";
			return ret;
		}
		List<DiceResult> result = rollDice(dice, wild, edge);
		
		// First of all, check of the downside of the wild dies triggers
		boolean wildDieDownside = result.stream().anyMatch(die -> die.face==1 && die.wild && !die.exploded);
		
		/*
		 * Evaluate
		 */
		int hits = 0;
		int ones = 0;
		for (DiceResult die : result) {
			switch (die.face) {
			case 1:
				if (!die.exploded)
					ones++; 
				break;
			case 5:
				if (wildDieDownside) {
					die.ignoredHit = true;
					break;
				}
			case 6:
				if (die.wild && !die.exploded)
					hits+=3;
				else
					hits++; 
				break;
			}
		}
		
		/*
		 * Prepare return value
		 */
		DiceRollResult ret = new DiceRollResult();
		ret.image = generate(result);
		ret.hits = hits;
		// for glitch calculation, add edge to pool size
		dice += edge;
		if (ones>(dice/2.0)) {
			// Glitch
			if (hits==0) {
				ret.type = ResultType.CRITICAL_GLITCH;
				ret.message = "**CRITICAL GLITCH!**";
				ret.thumbnail = generateEGImage(false, 0, true);
			} else {
				ret.message = "**GLITCH**, but with "+hits+" hits";
				ret.type = ResultType.GLITCH;
				ret.thumbnail = generateEGImage(false, hits, true);
			}
		} else {
			ret.thumbnail = generateEGImage((hits>0), hits, false);
			if (hits==0) {
				ret.type = ResultType.FAILURE;
				ret.message = "**Failure**";
			} else {
				ret.type = ResultType.SUCCESS;
				ret.message = "**"+hits+"** hits";
			}
		}
		
		return ret;
	}

	//-------------------------------------------------------------------
	public static void generate(EmbedBuilder embed, RollResult result) {
	
		boolean glitch = result.hasFlag(SR6RollEvaluationFlag.GLITCH) || result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH);
		int hits = result.getValue();
		boolean success = hits>0 && !glitch;
		byte[] thumbnail = generateEGImage(success, hits, glitch);
		
		byte[] images = generateNew(result.getDiceWithoutGroups());
	}

	//-------------------------------------------------------------------
	private static Image getResultImage(RolledDie die) {
		String filename = (die.hasFlag(SR6DiceEvaluationFlags.WILD)?"wild_die_":"die_")+die.getFace();
		if (die.hasFlag(SR6DiceEvaluationFlags.HIT_NOT_COUNTED))
			filename += "_ignored";
		else if (die.hasFlag(CommonDiceEvaluationFlag.EXPLODED))
			filename += "_exploded";
		filename += ".png";
		if (imagesByName.containsKey(filename))
			return imagesByName.get(filename);
		Image image = new Image(ClassLoader.getSystemResourceAsStream(filename));
		imagesByName.put(filename, image);
		return image;
	}

	//-------------------------------------------------------------------
	private static Image getResultImage(DiceResult die) {
		String filename = (die.wild?"wild_die_":"die_")+die.face;
		if (die.ignoredHit)
			filename += "_ignored";
		else if (die.exploded)
			filename += "_exploded";
		filename += ".png";
		if (imagesByName.containsKey(filename))
			return imagesByName.get(filename);
		Image image = new Image(ClassLoader.getSystemResourceAsStream(filename));
		imagesByName.put(filename, image);
		return image;
	}

	//-------------------------------------------------------------------
	static byte[] generateEGImage(boolean success, int eg, boolean glitch) {
		Canvas canvas = new Canvas(60,60);
		canvas.getGraphicsContext2D().setFill(success?Color.GREEN:(glitch?Color.RED:Color.GAINSBORO));
		canvas.getGraphicsContext2D().fillRoundRect(0, 0, 60, 60, 8, 8);
		canvas.getGraphicsContext2D().setFill(success?Color.BLACK:(glitch?Color.WHITE:Color.BLACK));
		canvas.getGraphicsContext2D().setFont(Font.font("serif", 40));
		if (eg>9) {
			canvas.getGraphicsContext2D().fillText(String.valueOf(eg), 5, 40);
		} else {
			canvas.getGraphicsContext2D().fillText(String.valueOf(eg), 15, 40);
		}
		return exportPngSnapshot(canvas, Color.TRANSPARENT);
	}
	
	//-------------------------------------------------------------------
	private static byte[] exportPngSnapshot(Node node, Paint backgroundFill) {
        if (node.getScene() == null) {
            Scene snapshotScene = new Scene(new Group(node));
        }

        SnapshotParameters params = new SnapshotParameters();
        params.setFill(backgroundFill);
        
        synchronized (node) {
        Platform.runLater(() -> {
            Image chartSnapshot = node.snapshot(params, null);
            PngEncoderFX encoder = new PngEncoderFX(chartSnapshot, true);
            data = encoder.pngEncode();
            synchronized (node) {
            	node.notify();
			}           
        });
        	try {
				node.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return data;
//        Image chartSnapshot = node.snapshot(params, null);
//        PngEncoderFX encoder = new PngEncoderFX(chartSnapshot, true);
//        return encoder.pngEncode();
    }
}
