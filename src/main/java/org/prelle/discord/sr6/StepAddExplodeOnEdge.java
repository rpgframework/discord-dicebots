package org.prelle.discord.sr6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.CommonExecutionFlag;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.Attribute;

/**
 * If the Edge attribute is added to the roll, auto-flag the roll with
 * EXPLODE
 * @author prelle
 *
 */
public class StepAddExplodeOnEdge implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.sr6");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.PLAUSIBILITY_CHECK;
	}

	//-------------------------------------------------------------------
	/**
	 * If any resolved variable is the attribute EDGE, add the WILD flag
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		for (ParsedElement elem : value.getRawElements()) {
			logger.warn("..."+elem);
			if (elem.getType()==ParsedElementType.VARIABLE) {
				logger.info("Check "+elem.getRawValue()+" = "+elem.getRawValue().getClass());
				if (elem.getRawValue() instanceof Attribute) {
					Attribute attrib = (Attribute) elem.getRawValue();
					if (attrib==Attribute.EDGE && !value.hasFlag(CommonExecutionFlag.EXPLODE)) {
						logger.debug("Edge attribute detected - add EXPLODE flag");
						value.addFlag(CommonExecutionFlag.EXPLODE);
					}
				}
			}
		}
	}

}
