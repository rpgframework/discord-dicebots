package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.PerGuildConfig;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.items.CarriedItem;
import org.prelle.shadowrun6.items.ItemType;
import org.prelle.shadowrun6.print.PrintScheme;
import org.prelle.shadowrun6.print.elements.ArmorAndPoolsElement;
import org.prelle.shadowrun6.print.elements.BasicElement;
import org.prelle.shadowrun6.print.elements.CombinedDroneElement;
import org.prelle.shadowrun6.print.elements.CombinedVehicleElement;
import org.prelle.shadowrun6.print.elements.MatrixDevicesElement;
import org.prelle.shadowrun6.print.elements.SmallWeaponsElement;

import de.rpgframework.print.PDFPrintElement.RenderingParameter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class GearCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	public final static CommandVariant WEAPONS = new CommandVariant("weapons", "gear  *w(?:eapons|eapon|eapo|eap|ea|e)? *" , "gear weapons", CommandUser.ANYONE) ;
	public final static CommandVariant VEHICLE = new CommandVariant("vehicles","gear  *v(?:ehicles|ehicle|ehicl|ehic|ehi|eh|e)? ([0-9]) *" , "gear vehicle <idx>", CommandUser.ANYONE) ;
	public final static CommandVariant VEHICLES= new CommandVariant("vehicles","gear  *v(?:ehicles|ehicle|ehicl|ehic|ehi|eh|e)? *" , "gear vehicles", CommandUser.ANYONE) ;
	public final static CommandVariant ARMOR   = new CommandVariant("armor"  , "gear  *a(?:rmor|rmo|rm|r)? *" , "gear armor", CommandUser.ANYONE) ;
	public final static CommandVariant DRONE   = new CommandVariant("drones" , "gear  *d(?:rones|rone|ron|ro|d)? ([0-9]) *" , "gear drone <idx>", CommandUser.ANYONE) ;
	public final static CommandVariant DRONES  = new CommandVariant("drones" , "gear  *d(?:rones|rone|ron|ro|d)? *" , "gear drones", CommandUser.ANYONE) ;
	public final static CommandVariant MATRIX  = new CommandVariant("matrix" , "gear  *m(?:atrix|atri|atr|at|a)? *" , "gear matrix", CommandUser.ANYONE) ;
	public final static CommandVariant ELECTRO = new CommandVariant("electro", "gear  *e(?:lectronics|ectronic|lectroni|lectron|lectro|lectr|lect|lec|le|l)? *" , "gear electronics", CommandUser.ANYONE) ;
	public final static CommandVariant ANY     = new CommandVariant("any"    , "gear.*" , "gear help", CommandUser.ANYONE) ;
	
	private CommandVariant[] variants;
	private SR6Bot bot;
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(GearCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public GearCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				WEAPONS, ARMOR, MATRIX, VEHICLE, VEHICLES, DRONE, DRONES, ANY
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "gear."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}

		ShadowrunCharacter model = bot.getLinkedCharacter(event.getAuthor());
		if (model==null) {
			String error = getTranslation(locale, "error.no_linked_character");
			channel.sendMessage(error).queue();
			return;
		}
		
		PrintScheme scheme = new PrintScheme(true);
		byte[] image = null;
		BasicElement elem = null;
		RenderingParameter render = new RenderingParameter();
		if (cmd==WEAPONS) {
			elem = new SmallWeaponsElement(scheme, model);
		} else if (cmd==ARMOR) {
			elem = new ArmorAndPoolsElement(scheme, model);
		} else if (cmd==MATRIX) {
			elem = new MatrixDevicesElement(scheme, model);
		} else if (cmd==VEHICLES) {
			sendVehicleTable(channel, model);
			return;
		} else if (cmd==VEHICLE) {
			elem = new CombinedVehicleElement(scheme, model, Integer.parseInt(matcher.group(1)));
		} else if (cmd==DRONES) {
			sendDroneTable(channel, model);
			return;
		} else if (cmd==DRONE) {
			elem = new CombinedDroneElement(scheme, model,0);
		} else {
			logger.debug("Did not detect command variant: "+cmd);
			logger.debug("Did not detect command variant: "+cmd.getCommand());
			StringBuffer buf = new StringBuffer("```md");
			for (CommandVariant var : variants) {
				if (var==ANY)
					continue;
				buf.append("\n"+var.getCommand()+" \t- "+getCommandExplanations(var, locale));
				switch (var.getMinimalUser()) {
				case ADMIN: buf.append(" < Server Admin >"); break;
				case GAMEMASTER: buf.append(" < GM >"); break;
				}
			}
			buf.append("\n```");

			String head = getTranslation(locale, "help.header", bot.getGuildConfig(event.getChannel()).getCommandChar());
			event.getChannel().sendMessage(head+"\n"+buf).queue();
			return;
		}
		
		image = elem.render2(render, 2);
		String name = event.getAuthor().getName();
		try { name = event.getGuild().retrieveMember(event.getAuthor()).complete().getNickname(); } catch (Exception e) {}
		name = model.getName();
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(name);
//		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, wild, edge);
		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
		.setColor(255*65536 + 0*256 + 0xA0)
		;

		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
			embed.setFooter("This message is going to destroy itself in "+SR6Bot.TIMEOUT_ROLL_RESULT+" minutes");
		}
		
		logger.info("Send");
		channel.sendFile(image, "result.png")
		.embed(embed.build())
		.queue(mess -> {
			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
				bot.expireMessage(mess, Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
			}
		});
	}

	//-------------------------------------------------------------------
	private void sendVehicleTable(MessageChannel channel, ShadowrunCharacter model) {
		int i=-1;
		StringBuffer buf = new StringBuffer();
		for (CarriedItem item : model.getItems(true,  ItemType.VEHICLES)) {
			i++;
			buf.append(String.format("%2d : %s\n", i, item.getName()));
		}
		logger.info("Send "+buf);
		channel.sendMessage(buf.toString()).queue();;
	}

	//-------------------------------------------------------------------
	private void sendDroneTable(MessageChannel channel, ShadowrunCharacter model) {
		int i=-1;
		StringBuffer buf = new StringBuffer();
		for (CarriedItem item : model.getItems(true,  ItemType.droneTypes())) {
			i++;
			buf.append(String.format("%.2d : %s\n", i, item.getName()));
		}
		logger.info("Send "+buf);
		channel.sendMessage(buf.toString()).queue();;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
		// TODO Auto-generated method stub

	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
