package org.prelle.discord.sr6;

/**
 * @author prelle
 *
 */
public enum UserDataKey {

	SKILL_VALUE,
	ATTRIBUTE_VALUE
	
}
