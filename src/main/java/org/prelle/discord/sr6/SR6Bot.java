/**
 * 
 */
package org.prelle.discord.sr6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.prelle.discord.BotCommand;
import org.prelle.discord.BotSkeleton;
import org.prelle.discord.CombatParticipant;
import org.prelle.discord.DiscordGamingSession;
import org.prelle.discord.DiscordSQL;
import org.prelle.discord.InitiativeTracker;
import org.prelle.discord.PerGuildConfig;
import org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult;
import org.prelle.rpgframework.shadowrun6.data.Shadowrun6DataPlugin;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.modifications.AttributeModification;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.character.RulePlugin.RulePluginProgessListener;
import de.rpgframework.genericrpg.modification.Modification;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.requests.GatewayIntent;

/**
 * @author prelle
 *
 */
public class SR6Bot extends BotSkeleton<SR6Volatile>  {
	
	public final static int TIMEOUT_ROLL_COMMAND = 2;
	public final static int TIMEOUT_ROLL_RESULT  = 2;
	
	private final static String SPEND1EDGE = "spend1edge";
	private final static String SPEND2EDGE = "spend2edge";
	private final static String SPEND3EDGE = "spend3edge";
	private final static String SPEND4EDGE = "spend4edge";
	private final static String SPEND5EDGE = "spend5edge";

	//-------------------------------------------------------------------
	static {
		 logger = LogManager.getLogger("discord.sr6");
//		jdaBuilder = JDABuilder.createLight("NzA3NjkxNzgzODE5MzYyMzc2.XrRw_g.04pldzpp_abh2IaIEqYJc_eLNfM", 
//		jdaBuilder = JDABuilder.createLight("NzA2OTg0NDUzOTU1NjQ5NTQ3.Xrq0KA.EUwBCwTzid-0tZtL1ga5UB6a77A", 
//				GatewayIntent.DIRECT_MESSAGES, 
//				GatewayIntent.DIRECT_MESSAGE_TYPING,
//				GatewayIntent.DIRECT_MESSAGE_REACTIONS,
//				GatewayIntent.GUILD_MESSAGES,
//				GatewayIntent.GUILD_MESSAGE_TYPING,
//				GatewayIntent.GUILD_MESSAGE_REACTIONS
//				);
		
	        Locale.setDefault(Locale.ENGLISH);
		Shadowrun6DataPlugin plugin = new Shadowrun6DataPlugin();
		plugin.init(new RulePluginProgessListener() {
			public void progressChanged(double value) {
		        Locale.setDefault(Locale.ENGLISH);
			}
		});
	}

	private JDABuilder jdaBuilder;
	private Map<User, ShadowrunCharacter> charPerUser;
	private Map<User, CombatParticipant> combPerUser;
	private Map<String, InitiativeTracker<String>> iniPerChannel;
	
	private String releaseNotesContent;
	private Instant releaseNotesTimestamp;

	//-------------------------------------------------------------------
	public SR6Bot(String token, DiscordSQL database, Properties config) {
		super(database, "SHADOWRUN6", SR6Volatile.class);
		jdaBuilder = JDABuilder.createLight(token, 
				GatewayIntent.DIRECT_MESSAGES, 
				GatewayIntent.DIRECT_MESSAGE_TYPING,
				GatewayIntent.DIRECT_MESSAGE_REACTIONS,
				GatewayIntent.GUILD_MESSAGES,
				GatewayIntent.GUILD_MESSAGE_REACTIONS,
				GatewayIntent.GUILD_EMOJIS
				);

		charPerUser = new HashMap<User, ShadowrunCharacter>();
		combPerUser = new HashMap<User, CombatParticipant>();
		iniPerChannel = new HashMap<String, InitiativeTracker<String>>();

		// Search for a release notes file
		String relNotesFileName = config.getProperty("release_notes.sr6", "/etc/eden/dicebot_releasenotes_sr6.txt");
		Path path = Paths.get(relNotesFileName);
		logger.info("Expect release notes at "+path);
		if (Files.exists(path)) {
			try {
				releaseNotesContent   = new String(Files.readAllBytes(path));
				releaseNotesTimestamp = Files.getLastModifiedTime(path).toInstant().truncatedTo(ChronoUnit.SECONDS);
				logger.info("Last release notes are from "+releaseNotesTimestamp);
			} catch (IOException e) {
				logger.error("Failed reading release notes",e);
			}
		}
		
		addCommand(new ConfigureCommand(this));
		addCommand(new RollCommand(this)); 
//		addCommand(new SkillCommand(this));
//		addCommand(new AttribCommand(this));
		addCommand(new SessionCommand(this));
		addCommand(new EdgeCommand(this));
		addCommand(new InitiativeCommand(this));
		addCommand(new CheckCommand(this));
		addCommand(new GearCommand(this));
		
		//We construct a builder for a BOT account. If we wanted to use a CLIENT account
		// we would use AccountType.CLIENT
		try {
			jda = jdaBuilder.build();
			jda.addEventListener(this);
			jda.awaitReady(); // Blocking guarantees that JDA will be completely loaded.
			System.out.println("SR6Bot set up");
		}
		catch (LoginException e) {
			//If anything goes wrong in terms of authentication, this is the exception that will represent it
			e.printStackTrace();
		} catch (InterruptedException e) {
			//Due to the fact that awaitReady is a blocking method, one which waits until JDA is fully loaded,
			// the waiting can be interrupted. This is the exception that would fire in that situation.
			//As a note: in this extremely simplified example this will never occur. In fact, this will never occur unless
			// you use awaitReady in a thread that has the possibility of being interrupted (async thread usage and interrupts)
			e.printStackTrace();
		}
		
		installGlobalCommands();
	}

	//-------------------------------------------------------------------
	public ShadowrunCharacter getLinkedCharacter(User user) {
		ShadowrunCharacter model = charPerUser.get(user);
		if (model==null) {
			String filename = "/tmp/"+user.getAsTag()+".xml";
			try {
				Path path = Paths.get(filename);
				if (!Files.exists(path))
					return null;
				byte[] data = Files.readAllBytes(Paths.get(filename));
				model = ShadowrunCore.load(data);
				logger.info("Successfully linked '"+model.getName()+"' for player "+user.getName());
				charPerUser.put(user, model);
			} catch (IOException e) {
				logger.error("Failed loading "+filename,e);
			}		
		}
		return model;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @param event
	 *          An event containing information about a {@link net.dv8tion.jda.api.entities.Message Message} that was
	 *          sent in a channel.
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onMessageReceived(net.dv8tion.jda.api.events.message.MessageReceivedEvent)
	 */
	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		User author = event.getAuthor();                //The user that sent the message
		// Ignore messages from other bots
		if (author.isBot())
			return;

		// Ignore private messages
		if (event.isFromType(ChannelType.PRIVATE)) {
//			onPrivateMessageReceived((PrivateMessageReceivedEvent)event);
			return;
		}
		
		//Event specific information
		Message message = event.getMessage();           //The message that was received.
		TextChannel channel = event.getTextChannel();    //This is the MessageChannel that the message was sent to.
		//  This could be a TextChannel, PrivateChannel, or Group!

		String msg = message.getContentDisplay().trim();

		
		// Handle uploads
		if (event.getMessage().getAttachments().size()>0) {
//			onFileUpload(event);
			return;
		}
		
		Guild  guild  = event.getGuild();
		
		if (msg.isEmpty())
			return;
		try {
			if (msg.charAt(0)==getGuildConfig(guild).getCommandChar()) {
				logger.info("RCV ["+msg+"] from "+guild);
				if (!super.process(event)) {
					logger.warn("Failed");
					channel.sendMessage("I don't understand. Try "+getCommandChar(guild)+"help").queue();
				} 
			}
		} catch (Exception e) {
			logger.error("Failed parsing: "+msg,e);
			channel.sendMessage(e.toString()).queue();
		}
	}

//	//-------------------------------------------------------------------
//	private void roll(MessageChannel channel, Message mess, User author, Guild guild, String msg) {
//		PerGuildConfig guildConfig = getGuildConfig(channel);
//		
//		logger.info(msg+"   from "+author.getName()+" on channel "+channel.getName()+" on server "+guild);
//		String[] args = msg.split(" ");
//		int pool = Integer.parseInt(args[1]);
//		int wild = 0;
//		if (args.length>2)
//			wild = Integer.parseInt(args[2]);
//		int edge = 0;
//		if (args.length>3)
//			edge = Integer.parseInt(args[3]);
//
//		if (pool>30) {
//			author.openPrivateChannel().queue((privChannel) ->
//			{
//				privChannel.sendMessage("Pool too large!").queue();
//			});
//			return;
//		}
//
//
//		EmbedBuilder embed = new EmbedBuilder();
//		embed.setTitle(author.getName());
//		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, wild, edge);
//		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
//		.setDescription(ret.message)
//		.setColor(255*65536 + 0*256 + 0xA0)
//		.setThumbnail("attachment://success.png")
//		;
//		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//			embed.setFooter("This message is going to destroy itself in "+TIMEOUT_ROLL_RESULT+" minutes");
//			expireMessage(mess, Instant.now().plus(TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
//		}
//		
//		logger.info("Send");
//		channel.sendFile(ret.image, "result.png")
//		.addFile(ret.thumbnail, "success.png")
//		.embed(embed.build())
//		.queue(message -> {
//			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
//				expireMessage(message, Instant.now().plus(TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
//			}
//			logger.info("Adding reactions");
////			message.addReaction(Emote.ICON_URL);
////			message.addReaction("U+0031U+20E3").queue(); // 1
////			message.addReaction("U+0032U+20E3").queue(); // 2
////			message.addReaction("U+0033U+20E3").queue(); // 3
////			message.addReaction("U+0034U+20E3").queue(); // 4
////			message.addReaction("U+0035U+20E3").queue(); // 5
////			if (guild!=null) {
////				try {
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND1EDGE)).queue();
////					message.addReaction(getGuildConfig(guild).getEmote(SPEND2EDGE)).queue();
////				} catch (Exception e) {
////					// TODO Auto-generated catch block
////					e.printStackTrace();
////				}
////			}
//		});
//
//		ReactionWaitObject obj = getRecentWaitObject(channel);
//		if (obj!=null) {
//			obj.command.onUserGeneratedResult(obj.variant, obj.message, author, ret, obj.userData);
//		}
//
//	}
	
	//-------------------------------------------------------------------
	private void sendCard(MessageChannel channel, String author, ShadowrunCharacter model) {
		String desc = String.format("Metatype:   \t**%s**\nMagic/Resonance: \t**%s**\nKarma:  **%d**\n%s: **%s**", 
				model.getMetatype().getName(), 
				model.getMagicOrResonanceType().getName(), 
				model.getKarmaInvested(),
				Attribute.REPUTATION.getName(),
				model.getAttribute(Attribute.REPUTATION).getModifiedValue());
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(model.getName());
		embed.setDescription(desc);
		if (model.getImage()!=null) {
			embed.setImage("attachment://portrait.png");
			channel.sendFile(model.getImage(), "portrait.png").embed(embed.build()).queue();
		} else {
			channel.sendMessage(embed.build()).queue();
		}
		
	}
	
	//-------------------------------------------------------------------
	protected void onFileUpload(PrivateMessageReceivedEvent event) {
		Attachment attach = event.getMessage().getAttachments().get(0);
		if (!attach.getFileName().endsWith(".xml") || attach.isImage() || attach.isVideo())
			return;

		// Try to load character
		try {
			byte[] data = attach.retrieveInputStream().get().readAllBytes();
			ShadowrunCharacter model = ShadowrunCore.load(data);
			charPerUser.put(event.getAuthor(), model);
			logger.info("Successfully linked '"+model.getName()+"' for player "+event.getAuthor().getName());
			// Try to write char to temp directory
			try {
//				String filename = "/tmp/"+model.getName()+".xml";
				String filename = "/tmp/"+event.getAuthor().getAsTag()+".xml";
				Files.write(Paths.get(filename), data);
				// Delete upload message
				event.getMessage().delete().queue();
			} catch (Exception e) {
				
			}
			
			/* 
			 * If user is already in a sesssion, set volatile
			 */
//			Guild guild = event.getGuild();
//			User author = event.getAuthor();
//			DiscordGamingSession<SR6Volatile> session = getSession(guild, event.getChannel());
//			if (session!=null) {
//				SR6Volatile vol = session.getVolatile(author);
//				if (vol!=null) {
//					logger.debug("Configure volatile with loaded character");
//					vol.setEdge(model.getAttribute(Attribute.EDGE).getModifiedValue());
//				}
//			}
			
			
//			if (event.isFromType(ChannelType.PRIVATE)) {
//				event.getPrivateChannel().sendMessage("Successfully linked "+model.getName()+" with "+event.getAuthor().getName()).queue();
//				sendCard(event.getPrivateChannel(), event.getAuthor().getName(), model);
//			} else {
				event.getChannel().sendMessage("Successfully linked "+model.getName()+" with "+event.getAuthor().getName()).queue();
				sendCard(event.getChannel(), event.getAuthor().getName(), model);
//			}

		} catch (IOException | InterruptedException | ExecutionException e) {
			logger.warn("User "+event.getAuthor().getName()+" uploaded a XML file '"+attach.getFileName()+"' that could not be parsed as SR6 char");
		}
	}
	
	//-------------------------------------------------------------------
	private void onFileUpload(MessageReceivedEvent event) {
		Attachment attach = event.getMessage().getAttachments().get(0);
		if (!attach.getFileName().endsWith(".xml") || attach.isImage() || attach.isVideo())
			return;

		// Try to load character
		try {
			byte[] data = attach.retrieveInputStream().get().readAllBytes();
			ShadowrunCharacter model = ShadowrunCore.load(data);
			charPerUser.put(event.getAuthor(), model);
			logger.info("Successfully linked '"+model.getName()+"' for player "+event.getAuthor().getName());
			// Try to write char to temp directory
			try {
//				String filename = "/tmp/"+model.getName()+".xml";
				String filename = "/tmp/"+event.getAuthor().getAsTag()+".xml";
				Files.write(Paths.get(filename), data);
				// Delete upload message
				event.getMessage().delete().queue();
			} catch (Exception e) {
				
			}
			
			
			
			/* 
			 * If user is already in a sesssion, set volatile
			 */
			Guild guild = event.getGuild();
			User author = event.getAuthor();
			DiscordGamingSession<SR6Volatile> session = getSession(guild, event.getChannel());
			if (session!=null) {
				SR6Volatile vol = session.getVolatile(author);
				if (vol!=null) {
					logger.debug("Configure volatile with loaded character");
					vol.setEdge(model.getAttribute(Attribute.EDGE).getModifiedValue());
				}
			}
			
			
			if (event.isFromType(ChannelType.PRIVATE)) {
				event.getPrivateChannel().sendMessage("Successfully linked "+model.getName()+" with "+event.getAuthor().getName()).queue();
				sendCard(event.getPrivateChannel(), event.getAuthor().getName(), model);
			} else {
				event.getChannel().sendMessage("Successfully linked "+model.getName()+" with "+event.getAuthor().getName()).queue();
				sendCard(event.getChannel(), event.getAuthor().getName(), model);
			}

		} catch (IOException | InterruptedException | ExecutionException e) {
			logger.warn("User "+event.getAuthor().getName()+" uploaded a XML file '"+attach.getFileName()+"' that could not be parsed as SR6 char");
		}
	}

	//-------------------------------------------------------------------
	private Object parseSR6(TextChannel channel, Message mess, User author, Guild guild, String msg) {
		String[] params = msg.trim().split(" ");
		logger.info("parseSR6: "+Arrays.toString(params)+"   from "+author.getName()+" in channel "+channel.getName()+" on server "+guild);
		if (params.length<2) {
			channel.sendMessage("Missing command").queue();
			return null;
		}
		// Find character for user
		ShadowrunCharacter model = charPerUser.get(author);
		if (model==null) {
			channel.sendMessage("No character linked to user "+author.getName()+"\nThis may occur if the Bot has been restarted.").queue();
			return null;
		}
		
		String command = params[1];
		
		
//		if ("skill".startsWith(command)) {
//			skill(channel, mess, author, params);
//		} else 
		if ("attribute".startsWith(command)) {
			attrib(channel, author, params);
		} else if ("initiative".startsWith(command)) {
			initiative(channel, author, guild, params);
		} else if ("resist".startsWith(command)) {
			resist(channel, author, params);
		} else if ("edge".startsWith(command)) {
			resist(channel, author, params);
		} else if ("test".startsWith(command)) {
			resist(channel, author, params);
		}
		return null;	
	}
	
	//-------------------------------------------------------------------
	public DiceRollResult rollSkill(MessageChannel channel, ShadowrunCharacter model, Skill skill, SkillSpecialization specKey, int pool, int edge, int wild) {
		PerGuildConfig guildConfig = getGuildConfig(channel);

		SkillValue val = model.getSkillValue(skill);
		if (val==null) {
			val = new SkillValue(skill, skill.isUseUntrained()?0:-1);
		}
		
		List<String> conds = new ArrayList<String>();
		List<String> other = new ArrayList<String>();
		for (Modification mod : val.getModifications()) {
			if (mod instanceof SkillModification) {
				SkillModification smod = (SkillModification)mod;
				if (smod.isConditional()) {
					conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
				}
			} else {
				other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
			}
		}
		
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(model.getName()+" rolls "+skill.getName()+
				((specKey!=null)?(" ("+specKey.getName()+")"):""));
		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, wild, edge);
//		String messa = ret.message;
		if (!conds.isEmpty()) {
			embed.addField("Conditional", String.join(", ", conds), true);
//			messa+= "\nConditional: "+String.join(", ", conds);				
		}
		if (!other.isEmpty()) {
			embed.addField("Also relevant", String.join(", ", other), true);
//			messa+= "\nAlso relevant: "+String.join(", ", other);				
		}
		
		embed.setImage("attachment://result.png") 
//		.setDescription(messa)
		.setColor(255*65536 + 0*256 + 0xA0)
		.setThumbnail("attachment://success.png")
		;
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
			embed.setFooter("This message is going to destroy itself in "+TIMEOUT_ROLL_RESULT+" minutes");
		}
		channel.sendFile(ret.image, "result.png")
		.addFile(ret.thumbnail, "success.png")
		.embed(embed.build())
		.queue( msg -> {
			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
				expireMessage(msg, Instant.now().plus(TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
			}
		});
		return ret;

	}

	//-------------------------------------------------------------------
	public DiceRollResult skillOld(MessageChannel channel, Message mess, User author, String[] params) {
		PerGuildConfig guildConfig = getGuildConfig(channel);
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && mess!=null) {
			expireMessage(mess, Instant.now().plus(TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		
		try {
			// Find character for user
			ShadowrunCharacter model = charPerUser.get(author);
			if (model==null) {
				channel.sendMessage("No character linked to user "+author.getName()).queue();
				return null;
			}
			if (params.length<3) {
				String error = "Missing skillname. Valid values are:";
				for (Skill tmp : ShadowrunCore.getSkills()) {
					error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return null;
			}
			
			String skillName = params[2].toLowerCase();
			Skill skill = null;
			for (Skill tmp : ShadowrunCore.getSkills()) {
				if (tmp.getId().startsWith(skillName.toLowerCase()) || tmp.getName().toLowerCase().startsWith(skillName)) {
					skill = tmp;
					break;
				}
			}
			if (skill==null) {
				String error = "Unknown skill. Valid keys are:";
				for (Skill tmp : ShadowrunCore.getSkills()) {
					error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return null;
			}
			
			SkillValue val = model.getSkillValue(skill);
			if (val==null) {
				val = new SkillValue(skill, skill.isUseUntrained()?0:-1);
			}
			
			List<String> conds = new ArrayList<String>();
			List<String> other = new ArrayList<String>();
			for (Modification mod : val.getModifications()) {
				if (mod instanceof SkillModification) {
					SkillModification smod = (SkillModification)mod;
					if (smod.isConditional()) {
						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
					}
				} else {
					other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
				}
			}
			
			
			int pool = ShadowrunTools.getSkillPool(model, skill);
			
			/*
			 * Potentially respect specializations
			 */
			SkillSpecialization specKey = null;
			if (params.length>3) {
				String specName = params[3].toLowerCase();
				for (SkillSpecialization tmp : skill.getSpecializations()) {
					if (tmp.getId().startsWith(specName) || tmp.getName().toLowerCase().startsWith(specName)) {
						specKey = tmp;
						break;
					}
				}
				if (specKey==null) {
					String error = "Unknown specialization. Valid keys are:";
					for (SkillSpecialization tmp : skill.getSpecializations()) {
						error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
					}
					channel.sendMessage(error).queue();
					return null;
				}
				
				pool = ShadowrunTools.getSkillPool(model, skill, skill.getAttribute1(), specKey.getId());
			}
			
			EmbedBuilder embed = new EmbedBuilder();
			embed.setTitle(model.getName()+" rolls "+skill.getName()+
					((specKey!=null)?(" ("+specKey.getName()+")"):""));
			DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, 0, 0);
			String messa = ret.message;
			if (!conds.isEmpty()) {
				messa+= "\nConditional: "+String.join(", ", conds);				
			}
			if (!other.isEmpty()) {
				messa+= "\nAlso relevant: "+String.join(", ", other);				
			}
			
			embed.setImage("attachment://result.png") 
			.setDescription(messa)
			.setColor(255*65536 + 0*256 + 0xA0)
			.setThumbnail("attachment://success.png")
			;
			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
				embed.setFooter("This message is going to destroy itself in "+TIMEOUT_ROLL_RESULT+" minutes");
			}
			channel.sendFile(ret.image, "result.png")
			.addFile(ret.thumbnail, "success.png")
			.embed(embed.build())
			.queue( msg -> {
				if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
					expireMessage(msg, Instant.now().plus(TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
				}
			});
			return ret;
		} catch (Exception e) {
			logger.error("Error processing "+Arrays.toString(params),e);
			channel.sendMessage("Sorry, the bot failed processing this request: "+e).queue();
		}
		return null;
	}

	//-------------------------------------------------------------------
	private void attrib(MessageChannel channel, User author, String[] params) {
		try {
			// Find character for user
			ShadowrunCharacter model = charPerUser.get(author);
			if (model==null) {
				channel.sendMessage("No character linked to user "+author.getName()).queue();
				return;
			}
			List<Attribute> attribs = new ArrayList<Attribute>();
			attribs.addAll(Arrays.asList(Attribute.primaryValues()));
			attribs.addAll(Arrays.asList(Attribute.COMPOSURE, Attribute.JUDGE_INTENTIONS, Attribute.MEMORY, Attribute.LIFT_CARRY));;
			if (params.length<3) {
				String error = "Missing attribute name. Valid values are:";
				for (Attribute tmp : attribs) {
					error += "\n* "+tmp.name().toLowerCase()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return;
			}
			
			String attrName = params[2].toLowerCase();
			Attribute attrib = null;
			for (Attribute tmp :attribs) {
				if (tmp.name().toLowerCase().startsWith(attrName) || tmp.getName().toLowerCase().startsWith(attrName)) {
					attrib = tmp;
					break;
				}
			}
			if (attrib==null) {
				String error = "Unknown attribute. Valid keys are:";
				for (Attribute tmp :attribs) {
					error += "\n* "+tmp.name().toLowerCase()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return;
			}
			
			AttributeValue val = model.getAttribute(attrib);
			if (val==null) {
				val = new AttributeValue(attrib, 1);
			}
			
			List<String> conds = new ArrayList<String>();
			List<String> other = new ArrayList<String>();
			for (Modification mod : val.getModifications()) {
				if (mod instanceof AttributeModification) {
					AttributeModification smod = (AttributeModification)mod;
					if (smod.isConditional()) {
						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
					}
				} else {
					other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
				}
			}
			
			
			int pool = val.getModifiedValue();
//			if (Arrays.asList(Attribute.primaryValues()).contains(attrib))
//				pool *=2;
			
			EmbedBuilder embed = new EmbedBuilder();
			embed.setTitle(model.getName()+" rolls "+attrib.getName());
			DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, 0, 0);
			String mess = ret.message;
			if (!conds.isEmpty()) {
				mess+= "\nConditional: "+String.join(", ", conds);				
			}
			if (!other.isEmpty()) {
				mess+= "\nAlso relevant: "+String.join(", ", other);				
			}
			
			embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
			.setDescription(mess)
			.setColor(255*65536 + 0*256 + 0xA0)
			.setThumbnail("attachment://success.png")
			;
			channel.sendFile(ret.image, "result.png")
			.addFile(ret.thumbnail, "success.png")
			.embed(embed.build())
			.queue();
		} catch (Exception e) {
			logger.error("Error processing "+Arrays.toString(params),e);
			channel.sendMessage("Sorry, the bot failed processing this request: "+e).queue();
		}
	}

	//-------------------------------------------------------------------
	private void initiative(MessageChannel channel, User author, Guild guild, String[] params) {
		try {
			// Find character for user
			ShadowrunCharacter model = charPerUser.get(author);
			if (model==null) {
				channel.sendMessage("No character linked to user "+author.getName()).queue();
				return;
			}
			
			DiscordGamingSession<SR6Volatile> session = sessions.get(guild, channel);
			if (session.getGamemaster()==null) {
				channel.sendMessage("There isn't a session running.\nTry **"+COMMAND_CHAR+"session start** when you are the gamemaster.").queue();
				return;
			}
			
			/*
			 * Ensure that author is player in a session
			 */
			SR6Volatile volat = session.getVolatile(author);
			if (session.getGamemaster()==null) {
				channel.sendMessage("You haven't joined the session\nTry **"+COMMAND_CHAR+"session join** when you are the player.").queue();
				return;
			}
			
			// Get INI tracker for this channel
//			InitiativeTracker<String> tracker = iniPerChannel.get(channel.getId());
			InitiativeTracker<SR6Volatile> tracker = session.getIniTracker();
			
			int iniResult = 0;
			if (params.length>2) {
				String cmd = params[2].toLowerCase();
				if ("show".startsWith(cmd)) {
					StringBuffer buf = new StringBuffer();
					for (InitiativeTracker<SR6Volatile>.IniPhase phase : tracker.getIniPhases()) {
						buf.append("\n**"+phase.ini+"** - ");
						buf.append(String.join(", " +phase.list));
					}
					channel.sendMessage(buf.toString()).queue();
					return;
				} else if ("roll".startsWith(cmd)) {
					
				}
				
				channel.sendMessage("Unknown command: "+cmd).queue();
				return;
			}

			
			Attribute attrMod = Attribute.INITIATIVE_PHYSICAL;
			Attribute attrDic = Attribute.INITIATIVE_DICE_PHYSICAL;
			
			AttributeValue valMod = model.getAttribute(attrMod);
			AttributeValue dicMod = model.getAttribute(attrDic);
			
			int sum = valMod.getModifiedValue();
			
			List<Integer> results = new ArrayList<Integer>();
			for (int i=0; i<dicMod.getModifiedValue(); i++) {
				int dice = SR6DiceGraphicGenerator.RANDOM.nextInt(6)+1;
				results.add(dice);
				sum += dice;
			}
			
			// Store in tracker
			tracker.modify(volat, sum);
			
			
			EmbedBuilder embed = new EmbedBuilder();
			embed.setTitle(model.getName()+" rolls "+attrMod.getName());
			String mess = "Result: "+valMod.getModifiedValue()+" + "+results+" = **"+sum+"**";
			logger.info("Send "+mess);
//			DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, 0, 0);
//			String mess = ret.message;
//			if (!conds.isEmpty()) {
//				mess+= "\nConditional: "+String.join(", ", conds);				
//			}
//			if (!other.isEmpty()) {
//				mess+= "\nAlso relevant: "+String.join(", ", other);				
//			}
//			
//			embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
			embed.setDescription(mess);
			channel.sendMessage(embed.build()).queue();
//			.setDescription(mess)
//			.setColor(255*65536 + 0*256 + 0xA0)
//			.setThumbnail("attachment://success.png")
//			;
//			channel.sendFile(ret.image, "result.png")
//			.addFile(ret.thumbnail, "success.png")
//			.embed(embed.build())
//			.queue();
		} catch (Exception e) {
			logger.error("Error processing "+Arrays.toString(params),e);
			channel.sendMessage("Sorry, the bot failed processing this request: "+e).queue();
		}
	}

	//-------------------------------------------------------------------
	private void resist(MessageChannel channel, User author, String[] params) {
		try {
			// Find character for user
			ShadowrunCharacter model = charPerUser.get(author);
			if (model==null) {
				channel.sendMessage("No character linked to user "+author.getName()).queue();
				return;
			}
			List<Attribute> attribs = new ArrayList<Attribute>();
			attribs.addAll(Arrays.asList(Attribute.DAMAGE_RESISTANCE, Attribute.TOXIN_RESISTANCE_POOL, Attribute.DRAIN_RESISTANCE));
			if (params.length<3) {
				String error = "Missing type of resistance. Valid values are:";
				for (Attribute tmp : attribs) {
					error += "\n* "+tmp.name().toLowerCase()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return;
			}
			
			String attrName = params[2].toLowerCase();
			Attribute attrib = null;
			for (Attribute tmp :attribs) {
				if (tmp.name().toLowerCase().startsWith(attrName) || tmp.getName().toLowerCase().startsWith(attrName)) {
					attrib = tmp;
					break;
				}
			}
			if (attrib==null) {
				String error = "Unknown resistance type. Valid keys are:";
				for (Attribute tmp :attribs) {
					error += "\n* "+tmp.name().toLowerCase()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return;
			}
			
			AttributeValue val = model.getAttribute(attrib);
			if (val==null) {
				val = new AttributeValue(attrib, 1);
			}

			List<String> conds = new ArrayList<String>();
			List<String> other = new ArrayList<String>();
			for (Modification mod : val.getModifications()) {
				if (mod instanceof AttributeModification) {
					AttributeModification smod = (AttributeModification)mod;
					if (smod.isConditional()) {
						conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
					}
				} else {
					other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
				}
			}
			
			
			int pool = val.getAugmentedValue();
//			if (Arrays.asList(Attribute.primaryValues()).contains(attrib))
//				pool *=2;
			
			EmbedBuilder embed = new EmbedBuilder();
			embed.setTitle(model.getName()+" resists using "+attrib.getName());
			DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, 0, 0);
			String mess = ret.message;
			mess+= "\nPool explanation: \n"+ShadowrunTools.getAttributePoolExplanation(model, attrib);				
			if (!conds.isEmpty()) {
				mess+= "\nConditional: "+String.join(", ", conds);				
			}
			if (!other.isEmpty()) {
				mess+= "\nAlso relevant: "+String.join(", ", other);				
			}
			
			embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
			.setDescription(mess)
			.setColor(255*65536 + 0*256 + 0xA0)
			.setThumbnail("attachment://success.png")
			;
			channel.sendFile(ret.image, "result.png")
			.addFile(ret.thumbnail, "success.png")
			.embed(embed.build())
			.queue();
		} catch (Exception e) {
			logger.error("Error processing "+Arrays.toString(params),e);
			channel.sendMessage("Sorry, the bot failed processing this request: "+e).queue();
		}
	}

//	//-------------------------------------------------------------------
//	private void edge(MessageChannel channel, User author, Guild guild, String msg) {
//		String[] params = msg.trim().split(" ");
//		logger.info("parseEdge: "+Arrays.toString(params)+"   from "+author.getName()+" in channel "+channel.getName()+" on server "+guild);
//		if (params.length<2) {
//			channel.sendMessage("Syntax:\n"
//					+COMMAND_CHAR+"sr6  edge <value>     - Raise or lower current edge by this value\n"
//					+COMMAND_CHAR+"sr6 edge set <value> - Set edge to the given value").queue();
//			return;
//		}
//		
//		// Get volatile data for User in this channel
//		DiscordGamingSession<SR6Volatile> session = sessions.get(guild, channel);
//		if (session.getGamemaster()==null) {
//			channel.sendMessage("No session running").queue();
//			return;
//		}
//		
//		SR6Volatile volat = session.getVolatile(author);
//		if (session.getGamemaster()==null) {
//			channel.sendMessage(author.getAsMention()+" you are not a player in this session.\nUse **"+COMMAND_CHAR+"session join** to ask for permission to join").queue();
//			return;
//		}
//		
//		String command = params[2];
//		logger.info("Command "+command);
//		if ("set".equals(command)) {
//			if (params.length<4) {
//				channel.sendMessage("Syntax:\n"
//						+COMMAND_CHAR+"sr6 edge <value>     - Raise or lower current edge by this value\n"
//						+COMMAND_CHAR+"sr6 edge set <value> - Set edge to the given value").queue();
//				return;
//			}
//			int value = Integer.parseInt(params[3]);
//			volat.setEdge(value);
//			channel.sendMessage(author.getAsMention()+", your Edge is now "+volat.getEdge()).queue();;
//		} else if ("show".equals(command)) {
//		} else {
//			int value = Integer.parseInt(command);
//			int newVal = volat.getEdge() + value;
//			if (newVal>7) newVal=7;
//			if (newVal<0) newVal=0;
//			volat.setEdge(newVal);
//			channel.sendMessage(author.getAsMention()+", your Edge is now "+volat.getEdge()).queue();;
//		}
//		
//	}

	//-------------------------------------------------------------------
	/**
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onReady(net.dv8tion.jda.api.events.ReadyEvent)
	 */
	@Override
	public void onReady(ReadyEvent event) {
		super.onReady(event);
		JDA jda = event.getJDA();                       //JDA, the core of the api.
		int count=0;
		for (Guild guild : jda.getGuilds()) {
			count++;
			List<String> roleNames = guild.getRoles().stream().map( role -> role.getName()).collect(Collectors.toList());
			String community = (guild.getCommunityUpdatesChannel()!=null)?guild.getCommunityUpdatesChannel().getName():"-";
			
			logger.info(count+": "+guild.getName()+" with "+guild.getMemberCount()+" members and community channel "+community);
			PerGuildConfig perGuild = addGuild(guild);
			
			
			logger.info("Configuring commands");
//			try {
//				super.setupGuildCommands(guild, perGuild);
//			} catch (Exception e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}

			try {
				if (perGuild.getLastReleaseNotes()==null || (releaseNotesTimestamp!=null && perGuild.getLastReleaseNotes().isBefore(releaseNotesTimestamp))) {
					logger.info("Post release notes to "+guild.getName());
					perGuild.setLastInfo(releaseNotesTimestamp);
					database.write(perGuild);
					
					for (TextChannel chan : guild.getTextChannels()) {
						if (chan.canTalk()) {
							logger.debug("Send release notes to channel "+chan.getName());
							if (releaseNotesContent!=null)
								chan.sendMessage(releaseNotesContent).queue();
							break;
						}
					}
				} else {
					logger.debug("Release notes for "+guild.getName()+" are up to date");
				}
				
				/*
				 * Asynchronously find emotes
				 */
//				guild.retrieveEmotes().queue(list -> {
//					for (Emote emote : list) {
//						logger.info("Emote "+emote.getName()+" and a id of "+emote.getIdLong()+" / "+emote.getId());
//						switch (emote.getName()) {
//						case SPEND1EDGE: 
//						case SPEND2EDGE: 
//							logger.info("Found emote "+emote.getName());
//							if (perGuild!=null)
//								perGuild.memorizeEmote(emote.getName(), emote);
//							else
//								logger.warn("No guild config");
//							break;
//						}
//					}
//				});
				
//			logger.info("  guild has "+guild.retrieveEmotes().complete().size()+" emotes retrieved");
//			logger.info("  guild has "+guild.getEmotes().size()+" emotes");
//			for (Emote emote : guild.retrieveEmotes().complete()) {
//				logger.info("Emote "+emote.getName()+" and a id of "+emote.getIdLong()+" / "+emote.getId());
//			}
//			logger.info("  guild has "+guild.getEmoteCache().size()+" emotes in cache");
//			logger.info("Retrieve 1 = "+guild.getEmoteById(710599105952350240L));
//			guild.getEmoteById("spend1edge");
			} catch (Exception e) {
				logger.error("Error onReady on channel "+guild.getTextChannels().get(0).getName(),e);
			}
		}
	} 
	
	//-------------------------------------------------------------------
	void setupEmotes(Guild guild) throws IOException {
		List<String> searchedEmotes = Arrays.asList(SPEND1EDGE, SPEND2EDGE, SPEND3EDGE, SPEND4EDGE, SPEND5EDGE);
		List<String> missingEmotes = new ArrayList<String>(searchedEmotes);
		
//		logger.info("  guild has "+guild.retrieveEmotes().complete().size()+" emotes retrieved");
		logger.info("  guild has "+guild.getEmotes().size()+" emotes");
		for (Emote emote : guild.retrieveEmotes().complete()) {
			if (searchedEmotes.contains(emote.getName()))
				missingEmotes.remove(emote.getName());
			logger.info("Emote "+emote.getName()+" and a id of "+emote.getIdLong()+" / "+emote.getId());
		}
		logger.info("Missing emotes for guild '"+guild.getName()+"' = "+missingEmotes);
		
		Icon icon = null;
		for (String name : missingEmotes) {
			switch (name)  {
			case SPEND1EDGE:
				icon = Icon.from(ClassLoader.getSystemResourceAsStream("Icon1.png"));
				guild.createEmote(SPEND1EDGE, icon).queue();
				break;
			case SPEND2EDGE:
				icon = Icon.from(ClassLoader.getSystemResourceAsStream("Icon2.png"));
				guild.createEmote(SPEND2EDGE, icon).queue();
				break;
			case SPEND3EDGE:
				icon = Icon.from(ClassLoader.getSystemResourceAsStream("Icon3.png"));
				guild.createEmote(SPEND3EDGE, icon).queue();
				break;
			case SPEND4EDGE:
				icon = Icon.from(ClassLoader.getSystemResourceAsStream("Icon4.png"));
				guild.createEmote(SPEND4EDGE, icon).queue();
				break;
			case SPEND5EDGE:
				icon = Icon.from(ClassLoader.getSystemResourceAsStream("Icon5.png"));
				guild.createEmote(SPEND5EDGE, icon).queue();
				break;
			}
		}
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onGuildJoin(net.dv8tion.jda.api.events.guild.GuildJoinEvent)
	 */
	@Override
	public void onGuildJoin(GuildJoinEvent event) {
		super.onGuildJoin(event);
		
//		try {
//			setupEmotes(event.getGuild());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

}
