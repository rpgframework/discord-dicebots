package org.prelle.discord.sr6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.Operator;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.Skill;

/**
 * Add the primary attribute to the roll command, should there be
 * a skill variable without an attribute present
 * @author prelle
 *
 */
public class StepAddAttributeToSkill implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.sr6");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.PLAUSIBILITY_CHECK;
	}

	//-------------------------------------------------------------------
	/**
	 * If any resolved variable is the attribute EDGE, add the WILD flag
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		Skill skill = null;
		ParsedElement skillElem = null;
		Attribute attrib = null;
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.getType()==ParsedElementType.VARIABLE) {
				logger.debug("Check "+elem.getRawValue()+" = "+elem.getRawValue().getClass());
				if (elem.getRawValue() instanceof Skill) {
					skill = (Skill) elem.getRawValue();
					skillElem = elem;
					logger.debug("Roll contains a skill: "+skill);
				} else if (elem.getRawValue() instanceof Attribute) {
					if (attrib==null || !attrib.isPrimary()) {
						attrib = (Attribute) elem.getRawValue();
						logger.debug("Roll contains an attribute: "+attrib);
					}
				}
			}
		}
		
		if (skill==null)
			return;
		
		if (attrib==null || !attrib.isPrimary()) {
			logger.debug("Add missing attribute to skill: "+skill.getAttribute1());
			ParsedElement op = new ParsedElement(ParsedElementType.OPERATOR, Operator.PLUS);
			value.getRawElements().add(value.getRawElements().indexOf(skillElem)+1, op);
			ParsedElement attribElem = new ParsedElement(ParsedElementType.VARIABLE, skill.getAttribute1().getName());
			value.getRawElements().add(value.getRawElements().indexOf(skillElem)+2, attribElem);
		}
	}

}
