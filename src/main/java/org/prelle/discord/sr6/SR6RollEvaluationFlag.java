package org.prelle.discord.sr6;

import org.prelle.rollbot.RollEvaluationFlag;

/**
 * @author prelle
 *
 */
public enum SR6RollEvaluationFlag implements RollEvaluationFlag {

	GLITCH,
	CRITICAL_GLITCH
	
}
