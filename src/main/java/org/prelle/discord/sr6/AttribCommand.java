package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.PerGuildConfig;
import org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.AttributeValue;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.modifications.AttributeModification;

import de.rpgframework.genericrpg.modification.Modification;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;

/**
 * @author prelle
 *
 */
public class AttribCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	public final static CommandVariant ATTRIB_PLAIN = new CommandVariant("norm", "sr6 a(?:ttrib|ttri|ttr|tt|t)? ([a-zA-Z]+) *" , "sr6 attrib <attribute>", CommandUser.ANYONE) ;
	private final static CommandVariant ATTRIB_EDGE = new CommandVariant("edge", "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) (\\d) *" , "sr6 attrib <attribute> <edge>", CommandUser.ANYONE) ;
	
	private CommandVariant[] variants;
	private SR6Bot bot;
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(AttribCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public AttribCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				ATTRIB_PLAIN,ATTRIB_EDGE
		};
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandData(java.util.Locale)
	 */
	@Override
	public CommandData getCommandData(Locale loc) {
		CommandData roll = new CommandData("roll", getTranslation(loc, "command.roll.desc"));
		
		OptionData dice = new OptionData(OptionType.INTEGER, "würfel", getTranslation(loc, "command.roll.param.wuerfel"), true);
		// ToDo: Min/Max
		roll.getOptions().add(dice);
		return roll;
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "attrib."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		
		// Find character for user
		ShadowrunCharacter model = bot.getLinkedCharacter(event.getAuthor());
		if (model==null) {
			channel.sendMessage("No character linked to user "+event.getAuthor().getName()).queue(newMsg -> {
				if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
					bot.expireMessage(newMsg, Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
				}
			});
			return;
		}

		/*
		 * Now process depending on variant
		 */
		String attrName = matcher.group(1).toLowerCase();
		int    edge = 0;
		
		if (cmd==ATTRIB_EDGE)  {
			edge = Integer.parseInt(matcher.group(2));
		}
		
		/*
		 * Find attribute by name
		 */
		List<Attribute> attribs = new ArrayList<Attribute>();
		attribs.addAll(Arrays.asList(Attribute.primaryValues()));
		attribs.addAll(Arrays.asList(Attribute.COMPOSURE, Attribute.JUDGE_INTENTIONS, Attribute.MEMORY, Attribute.LIFT_CARRY));;
		Attribute attrib = null;
		for (Attribute tmp : attribs) {
			if (tmp.name().toLowerCase().startsWith(attrName) || tmp.getName().toLowerCase().startsWith(attrName)) {
				attrib = tmp;
				break;
			}
		}
		if (attrib==null) {
			String error = getTranslation(locale, "error.unknown_attribute");
			for (Attribute tmp : attribs) {
				error += "\n* "+tmp.getName()+"'";
			}
			channel.sendMessage(error).queue();
			return;
		}

		/*
		 * Calculate pool for that skill
		 */
		AttributeValue val = model.getAttribute(attrib);
		if (val==null) {
			val = new AttributeValue(attrib, 1);
		}
		
		List<String> conds = new ArrayList<String>();
		List<String> other = new ArrayList<String>();
		for (Modification mod : val.getModifications()) {
			if (mod instanceof AttributeModification) {
				AttributeModification smod = (AttributeModification)mod;
				if (smod.isConditional()) {
					conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
				}
			} else {
				other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
			}
		}
		
		
		int pool = val.getModifiedValue();
		
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(model.getName()+" rolls "+attrib.getName());
		DiceRollResult ret = SR6DiceGraphicGenerator.generate(embed, pool, 0, edge);
		String mess = ret.message;
		if (!conds.isEmpty()) {
			mess+= "\nConditional: "+String.join(", ", conds);				
		}
		if (!other.isEmpty()) {
			mess+= "\nAlso relevant: "+String.join(", ", other);				
		}
		
		embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
		.setDescription(mess)
		.setColor(255*65536 + 0*256 + 0xA0)
		.setThumbnail("attachment://success.png")
		;
		// Eventually expire message
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
			embed.setFooter("This message is going to destroy itself in "+SR6Bot.TIMEOUT_ROLL_RESULT+" minutes");
			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		channel.sendFile(ret.image, "result.png")
		.addFile(ret.thumbnail, "success.png")
		.embed(embed.build())
		.queue();
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
