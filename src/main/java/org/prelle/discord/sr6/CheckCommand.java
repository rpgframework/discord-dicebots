package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CheckSession;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.DiscordGamingSession;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.GenericMessageEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class CheckCommand implements BotCommand<SR6Volatile> {
	
	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant SKILL_PLAIN = new CommandVariant("skillplain", "check skill ([a-zA-Z]*)" , "check skill <skillname>", CommandUser.GAMEMASTER);
	
	private CommandVariant[] variants;

//	public static void main(String[] args) {
//		String val = "check skill Hello";
//		Matcher matcher = SKILL_PLAIN.getPattern().matcher(val);
//		System.out.println("Input: ["+val+"]");
//		System.out.println("RegEx: ["+SKILL_PLAIN.getPattern().pattern()+"]");
//		System.out.println(matcher.matches());
//	}
	
	private SR6Bot bot;
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(CheckCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}
	
	//-------------------------------------------------------------------
	private MessageEmbed buildEmbed(CheckSession sess) {
		List<User> list = new ArrayList<User>(sess.results.keySet());
		// Load characters where possible
		Map<User, ShadowrunCharacter> models = new HashMap<User, ShadowrunCharacter>();
		for (User user : list) {
			ShadowrunCharacter model = bot.getLinkedCharacter(user);
			if (model!=null)
				models.put(user, model);
		}
		// Sort , by character name preferred
		Collections.sort(list, new Comparator<User>() {
			public int compare(User o1, User o2) {
				String name1 = models.containsKey(o1)?models.get(o1).getName():o1.getName();
				String name2 = models.containsKey(o2)?models.get(o2).getName():o2.getName();
				return name1.compareTo(name2);
			}
		});
		StringBuffer buf = new StringBuffer("```md\n");
		for (User user : list) {
			String name = models.containsKey(user)?models.get(user).getName():user.getName();
			buf.append("* "+name+"  \t- "+sess.results.get(user)+"\n");
		}
		buf.append("```");
		
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(sess.title);
		embed.appendDescription(sess.message);
		embed.addField(getTranslation(sess.locale, "check.skillplain.results"), buf.toString(), false);
		embed.addField(getTranslation(sess.locale, "check.skillplain.command"), sess.commands, false);
		embed.setFooter(getTranslation(sess.locale, "check.skillplain.footnote"));
		return embed.build();
	}

	//-------------------------------------------------------------------
	public CheckCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				SKILL_PLAIN
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "check."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		if (cmd==SKILL_PLAIN) {
			skillPlain(event, matcher, locale);
		} else
			throw new IllegalArgumentException("CommandVariant "+cmd.getId()+" unknown in "+getClass());
	}

	//-------------------------------------------------------------------
	/**
	 * Inform all players that a simple skill test shall be made
	 */
	private void skillPlain(GenericMessageEvent event, Matcher matcher, Locale locale) {
		MessageChannel channel = event.getChannel();
		Guild guild = null;
		if (event.isFromType(ChannelType.TEXT)) {
			guild = event.getTextChannel().getGuild();
		}
		
		/*
		 * Verify skill
		 */
		String p1 = matcher.group(1).toLowerCase();
		Skill skill = null;
		for (Skill tmp : ShadowrunCore.getSkills()) {
			if (tmp.getName().toLowerCase().startsWith(p1) || tmp.getId().startsWith(p1)) {
				skill = tmp;
				break;
			}
		}
		if (skill==null) {
			String error = getTranslation(locale, "error.unknown_skill");
			for (Skill tmp : ShadowrunCore.getSkills()) {
				error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
			}
			channel.sendMessage(error).queue();
			return;
		}
		
		// Your gamemaster wants you to roll a skill check for **NAME**
		CheckSession session = new CheckSession();
		session.locale  = locale;
		session.message = getTranslation(locale, "check.skillplain.message", skill.getName());
		session.reactionCommand = bot.getCommandChar(guild)+"roll "+skill.getName().toLowerCase();
		session.commands = session.reactionCommand+"\n"+bot.getCommandChar(guild)+"roll *pool*";
		session.title    = getTranslation(locale, "check.skillplain.title", skill.getName());
		
		/*
		 * Store the check session
		 */
		DiscordGamingSession<SR6Volatile> discSess = bot.getSession(guild, channel);
		if (discSess!=null) {
			discSess.setCheckSession(session);
		}
		
		
		MessageEmbed embed = buildEmbed(session);
//		EmbedBuilder embed = new EmbedBuilder();
//		embed.setTitle();
//		embed.appendDescription(message);
//		embed.addField(getTranslation(locale, "check.skillplain.results", skill.getName()), "", false);
//		embed.addField(getTranslation(locale, "check.skillplain.command"), command, false);
//		embed.setFooter(getTranslation(locale, "check.skillplain.footnote"));
		channel.sendMessage(embed).queue( sentMsg -> {
			sentMsg.addReaction("U+1F3B2").queue();
			bot.waitForReaction(this, SKILL_PLAIN, channel, sentMsg, Instant.now().plus(2,ChronoUnit.MINUTES), session);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData) {
		// TODO Auto-generated method stub
		logger.warn("TODO: process react with "+userData+" in "+message.getEmbeds());
		if (cmd==SKILL_PLAIN) {
			CheckSession session = (CheckSession)userData;
			logger.info("Reaction command = "+session.reactionCommand);
			logger.info("Reaction from   = "+event.getMember());
			String[] params = session.reactionCommand.split(" ");
//			DiceRollResult result = bot.skill(event.getChannel(), null, event.getUser(), params);
			User rollUser = event.getMember().getUser();
			List<PhaseHook> list = (new ArrayList<PhaseHook>(Arrays.asList(RollCommand.diceHooks)));
			list.add(new StepAddSkillAttribValues(bot.getLinkedCharacter(rollUser)));
			PhaseHook[] enhancedHooks = list.toArray(new PhaseHook[list.size()]);
			String cm = session.reactionCommand.substring(session.reactionCommand.indexOf(" ")+1);
			RollResult result = DiceRoller.execute(cm, new CharacterVariableResolver(bot.getLinkedCharacter(rollUser)), new SR6FlagResolver(), enhancedHooks);
//			DiceRollResult result = (DiceRollResult) bot.parse(event.getGuild(), event.getChannel(), message, session.reactionCommand);
			if (result==null) {
				session.results.put(event.getUser(), "?");
			} else {
				String add = "";
				if (result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH)) {
					add="("+getTranslation(session.locale, "label.critical_glitch")+")";
				} else if (result.hasFlag(SR6RollEvaluationFlag.GLITCH)) {
					add="("+getTranslation(session.locale, "label.glitch")+")";
				}
				session.results.put(event.getUser(), "["+result.getValue()+"]"+add);
			}
			message.editMessage(buildEmbed(session)).queue();;
			
			// Display command
			String name = event.getUser().getName();
			try { name = event.getGuild().retrieveMember(event.getUser()).complete().getNickname(); } catch (Exception e) {}
			RollCommand.showMessage(bot, result, event.getUser(), name, message.getChannel());
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant cmd, Message message, User author, RollResult result, Object userData) {
		// TODO Auto-generated method stub
		logger.warn("onUserGeneratedResult "+result+" in "+message.getEmbeds());
		if (cmd==SKILL_PLAIN) {
			CheckSession session = (CheckSession)userData;
			String add = "";
			if (result.hasFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH)) {
				add="("+getTranslation(session.locale, "label.critical_glitch")+")";
			} else if (result.hasFlag(SR6RollEvaluationFlag.GLITCH)) {
				add="("+getTranslation(session.locale, "label.glitch")+")";
			}
			session.results.put(author, "["+result.getValue()+"]"+add);
			message.editMessage(buildEmbed(session)).queue();;

		}
	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}
	
}
