package org.prelle.discord.sr6;

import java.util.function.Function;

import org.prelle.rollbot.ExecutionFlags;

/**
 * @author prelle
 *
 */
public class SR6FlagResolver implements Function<String, ExecutionFlags> {

	//-------------------------------------------------------------------
	/**
	 * @see java.util.function.Function#apply(java.lang.Object)
	 */
	@Override
	public ExecutionFlags apply(String val) {
		try {
			return SR6ExecutionFlags.valueOf(val);
		} catch (Exception e) {
			return null;
		}
	}

}
