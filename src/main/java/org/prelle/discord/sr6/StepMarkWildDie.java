package org.prelle.discord.sr6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.DiceEvaluationFlag;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;
import org.prelle.rollbot.ResolverException;

/**
 * @author prelle
 *
 */
public class StepMarkWildDie implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.sr6");

	//-------------------------------------------------------------------
	public StepMarkWildDie() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.AFTER_ROLL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		if (value.hasFlag(SR6ExecutionFlags.WILD)) {
			// Mark the first dice as wild
			if (!value.getDiceWithoutGroups().isEmpty()) {
				logger.debug("Mark die "+value.getDiceWithoutGroups().get(0)+" as WILD");
				value.getDiceWithoutGroups().get(0).addFlag(SR6DiceEvaluationFlags.WILD);
			}
		}
	}

}
