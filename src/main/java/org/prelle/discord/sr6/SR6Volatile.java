package org.prelle.discord.sr6;

import org.prelle.discord.VolatileCharacterData;

/**
 * @author prelle
 *
 */
public class SR6Volatile extends VolatileCharacterData {
	
	private int edge;
	private int stunDamage;
	private int physicalDamage;

	//-------------------------------------------------------------------
	public int getEdge() {
		return edge;
	}

	//-------------------------------------------------------------------
	public void setEdge(int edge) {
		this.edge = edge;
	}

	//-------------------------------------------------------------------
	public int getStunDamage() {
		return stunDamage;
	}

	//-------------------------------------------------------------------
	public void setStunDamage(int stunDamage) {
		this.stunDamage = stunDamage;
	}

	//-------------------------------------------------------------------
	public int getPhysicalDamage() {
		return physicalDamage;
	}

	//-------------------------------------------------------------------
	public void setPhysicalDamage(int physicalDamage) {
		this.physicalDamage = physicalDamage;
	}

}
