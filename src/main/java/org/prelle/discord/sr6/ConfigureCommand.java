package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.PerGuildConfig;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.NoLinkedCharacterException;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.ResolverException;
import org.prelle.rollbot.RollResult;
import org.prelle.rollbot.TooManyDiceException;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.Skill;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;

/**
 * @author prelle
 *
 */
public class ConfigureCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant CONFIG_CLEAN     = new CommandVariant("clean"      , "c(?:onfig|onfi|onf|on|o)? c(?:lean|lea|le|l)? ([a-zA-Z0-9]+) *" , "config clean [on|off]", CommandUser.GAMEMASTER) ;
//	private final static CommandVariant CONFIG_CCHAR     = new CommandVariant("commandchar", "c(?:onfig|onfi|onf|on|o)? c(?:har|ha|h)? ([^ ]) *" , "config char <X>", CommandUser.ADMIN) ;
	private final static CommandVariant CONFIG_EMOJI     = new CommandVariant("emoji"      , "c(?:onfig|onfi|onf|on|o)? e(?:moji|moj|mo|m)? *" , "config emoji", CommandUser.ADMIN) ;
	
	private SR6Bot bot;
	private CommandVariant[] variants;

//	public static void main(String[] args) {
//		String val = "sr6 config ch H";
//		System.out.println("Input: ["+val+"]");
//		for (CommandVariant var : new CommandVariant[] {CONFIG_CCHAR, CONFIG_CLEAN}) {
//		Matcher matcher = var.getPattern().matcher(val);
//		System.out.println("RegEx: ["+var.getPattern().pattern()+"]");
//		System.out.println(matcher.matches());
//		}
//	}
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(ConfigureCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public ConfigureCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				CONFIG_CLEAN,
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "config."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
//		if (cmd==CONFIG_CCHAR) {
//			char c = matcher.group(1).charAt(0);
//			
//			// Don't allow letters as trigger chars
//			if (Character.isLetter(c) || Character.isWhitespace(c)) {
//				channel.sendMessage(getTranslation(locale, "config.commandchar.invalid", c)).queue();
//				return;
//			}
//			
//			// Ensure member has permission
//			Guild guild = event.getGuild();
//			Member member = guild.getSelfMember();
//			if (!member.hasPermission(Permission.MANAGE_SERVER)) {
//				channel.sendMessage(getTranslation(locale, "config.error.permission", "MANAGE_SERVER")).queue();
//				return;
//			}
//			
//			// Perform the trigger command change
//			guildConfig.setCommandChar(c);
//			bot.saveGuildConfig(guildConfig);
//			channel.sendMessage(getTranslation(locale, "config.commandchar.changed", c)).queue();
//		} else 
		if (cmd==CONFIG_CLEAN) {
			// Ensure member has permission
			Guild guild = event.getGuild();
			Member member = guild.getSelfMember();
			if (!member.hasPermission(Permission.MANAGE_SERVER)) {
				channel.sendMessage(getTranslation(locale, "config.error.permission", "MANAGE_SERVER")).queue();
//				return;
			}
			// Perform command
			String val = matcher.group(1).toLowerCase();
			boolean value = (val.equals("1") || val.equals("on") || val.equals("yes"));
			guildConfig.setAutoClean(value);
			bot.saveGuildConfig(guildConfig);
			channel.sendMessage(getTranslation(locale, "config.autoclean", getTranslation(locale, "boolean."+value))).queue();
		} else if (cmd==CONFIG_EMOJI) {
			// Ensure member has permission
			Guild guild = event.getGuild();
			Member member = guild.getSelfMember();
			if (!member.hasPermission(Permission.MANAGE_SERVER)) {
				channel.sendMessage(getTranslation(locale, "config.error.permission", "MANAGE_SERVER")).queue();
				return;
			}
			// Perform command
			try {
				bot.setupEmotes(guild);
				channel.sendMessage(getTranslation(locale, "config.emotes.success")).queue();
			} catch (Exception e) {
				logger.warn("Failed setting up emotes",e);
				channel.sendMessage(getTranslation(locale, "config.emotes.failed", e.toString())).queue();
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
	}

	@Override
	public CommandData getCommandData(Locale loc) {
		CommandData roll = new CommandData("sr6", getTranslation(loc, "command.sr6.desc"));
		
		/*
		 *  Change prefix character
		 */
//		SubcommandData prefixCmd = new SubcommandData("prefix", getTranslation(loc, "command.sr6.prefix.desc"));
//		OptionData prefix = new OptionData(OptionType.STRING , "prefix", getTranslation(loc, "command.sr6.prefix.param.prefix"), true);
//		OptionData enable = new OptionData(OptionType.BOOLEAN, "enable", getTranslation(loc, "command.sr6.prefix.param.enable"), false);
//		prefixCmd.addOptions(prefix);
//		prefixCmd.addOptions(enable);
		
		/*
		 *  Change autoclean values
		 */
		SubcommandData clean = new SubcommandData("clean", getTranslation(loc, "command.sr6.clean.desc"));
//		OptionData time = new OptionData(OptionType.INTEGER , "minutes", getTranslation(loc, "command.sr6.clean.param.minutes"), true);
//		time.setMinValue(1);
//		time.setMaxValue(20);
		OptionData enable = new OptionData(OptionType.BOOLEAN, "enable", getTranslation(loc, "command.sr6.clean.param.enable"), true);
//		clean.addOptions(time);
		clean.addOptions(enable);
		
		roll.addSubcommands(clean);
		return roll;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onSlashCommand(net.dv8tion.jda.api.events.interaction.SlashCommandEvent)
	 */
	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		logger.info("Process "+event.getOptions());
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getHook()!=null) {
			bot.expireInteraction(event.getHook(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		Locale locale = Locale.forLanguageTag(guildConfig.getLanguage());

		/*
		 * Now process depending on variant
		 */
		String variant = event.getSubcommandName();
		switch (variant) {
		case "clean":
			configureClean(event, channel, locale);
			break;
		}
		
	}

	//-------------------------------------------------------------------
	private void configureClean(SlashCommandEvent event, MessageChannel channel, Locale locale) {
		logger.warn("configureClean called");
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		Guild guild = event.getGuild();
		Member member = guild.getSelfMember();
		if (!member.hasPermission(Permission.MANAGE_SERVER)) {
			channel.sendMessage(getTranslation(locale, "config.error.permission", "MANAGE_SERVER")).queue();
//			return;
		}

		//int minutes = (int) event.getOption("minutes").getAsLong();
		boolean enable = event.getOption("enable").getAsBoolean();
		guildConfig.setAutoClean(enable);
		
		logger.info(event.getUser().getName()+" sets auto-clean to "+enable);

		bot.saveGuildConfig(guildConfig);
		event.reply(getTranslation(locale, "config.autoclean", getTranslation(locale, "boolean."+enable))).setEphemeral(true).queue();
		
	}
	
}
