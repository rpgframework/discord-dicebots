package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.PerGuildConfig;
import org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.ShadowrunCore;
import org.prelle.shadowrun6.ShadowrunTools;
import org.prelle.shadowrun6.Skill;
import org.prelle.shadowrun6.SkillSpecialization;
import org.prelle.shadowrun6.SkillValue;
import org.prelle.shadowrun6.modifications.SkillModification;

import de.rpgframework.genericrpg.modification.Modification;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class SkillCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	public final static CommandVariant SKILL_PLAIN      = new CommandVariant("skillNorm"    , "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) *" , "sr6 skill <skillname>", CommandUser.ANYONE) ;
	private final static CommandVariant SKILL_PLAIN_EDGE = new CommandVariant("skillNormEdge", "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) (\\d) *" , "sr6 skill <skillname> <edge>", CommandUser.ANYONE) ;
	private final static CommandVariant SKILL_PLAIN_WILD = new CommandVariant("skillNormWild", "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) (\\d) (\\d) *" , "sr6 skill <skillname> <edge> <wild>", CommandUser.ANYONE) ;
	private final static CommandVariant SKILL_SPEC       = new CommandVariant("skillSpec"    , "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) ([a-zA-Z]+) *" , "sr6 skill <skillname> <specialization>", CommandUser.ANYONE);
	private final static CommandVariant SKILL_SPEC_EDGE  = new CommandVariant("skillSpecEdge", "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) ([a-zA-Z]+) (\\d) *" , "sr6 skill <skillname> <specialization> <edge>", CommandUser.ANYONE);
	private final static CommandVariant SKILL_SPEC_WILD  = new CommandVariant("skillSpecWild", "sr6 s(?:kill|kil|ki|k)? ([a-zA-Z]+) ([a-zA-Z]+) (\\d) (\\d) *" , "sr6 skill <skillname> <specialization> <edge> <wilddie>", CommandUser.ANYONE);

	public static void main(String[] args) {
		String val = "sr6 s elec f 2";
		System.out.println("Input: ["+val+"]");
		for (CommandVariant var : new CommandVariant[] {SKILL_PLAIN,SKILL_PLAIN_EDGE,SKILL_PLAIN_WILD,SKILL_SPEC,SKILL_SPEC_EDGE,SKILL_SPEC_WILD}) {
		Matcher matcher = var.getPattern().matcher(val);
		System.out.println("RegEx: ["+var.getPattern().pattern()+"]");
		System.out.println(matcher.matches());
		}
	}
	
	private CommandVariant[] variants;
	private SR6Bot bot;
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(SkillCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public SkillCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				SKILL_PLAIN,SKILL_PLAIN_EDGE,SKILL_PLAIN_WILD,SKILL_SPEC,SKILL_SPEC_EDGE,SKILL_SPEC_WILD
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "skill."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		MessageChannel channel = event.getChannel();
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		
		// Find character for user
		ShadowrunCharacter model = bot.getLinkedCharacter(event.getAuthor());
		if (model==null) {
			channel.sendMessage("No character linked to user "+event.getAuthor().getName()).queue(newMsg -> {
				if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
					bot.expireMessage(newMsg, Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
				}
			});
			return;
		}

		/*
		 * Now process depending on variant
		 */
		String skillName = matcher.group(1).toLowerCase();
		String specName  = null;
		int    edge = 0;
		int    wild = 0;
		
		if (cmd==SKILL_PLAIN_EDGE)  {
			edge = Integer.parseInt(matcher.group(2));
		} else if (cmd==SKILL_PLAIN_WILD)  {
			edge = Integer.parseInt(matcher.group(2));
			wild = Integer.parseInt(matcher.group(3));
		} else if (cmd==SKILL_SPEC)  {
			specName = matcher.group(2).toLowerCase();
		} else if (cmd==SKILL_SPEC_EDGE)  {
			specName = matcher.group(2).toLowerCase();
			edge = Integer.parseInt(matcher.group(3));
		} else if (cmd==SKILL_SPEC_WILD)  {
			specName = matcher.group(2).toLowerCase();
			edge = Integer.parseInt(matcher.group(3));
			wild = Integer.parseInt(matcher.group(4));
		}
		
		/*
		 * Find skill by name
		 */
		Skill skill = null;
		for (Skill tmp : ShadowrunCore.getSkills()) {
			if (tmp.getId().startsWith(skillName.toLowerCase()) || tmp.getName().toLowerCase().startsWith(skillName)) {
				skill = tmp;
				break;
			}
		}
		if (skill==null) {
			String error = getTranslation(locale, "error.unknown_skill");
			for (Skill tmp : ShadowrunCore.getSkills()) {
				error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
			}
			channel.sendMessage(error).queue();
			return;
		}

		/*
		 * Calculate pool for that skill
		 */
		SkillValue val = model.getSkillValue(skill);
		if (val==null) {
			val = new SkillValue(skill, skill.isUseUntrained()?0:-1);
		}
		
		List<String> conds = new ArrayList<String>();
		List<String> other = new ArrayList<String>();
		for (Modification mod : val.getModifications()) {
			if (mod instanceof SkillModification) {
				SkillModification smod = (SkillModification)mod;
				if (smod.isConditional()) {
					conds.add(String.format("+%d dice through **%s**", smod.getValue(), ShadowrunTools.getModificationSourceString(smod.getSource())));
				}
			} else {
				other.add("**"+ShadowrunTools.getModificationSourceString(mod.getSource())+"**");
			}
		}
		
		
		int pool = ShadowrunTools.getSkillPool(model, skill);

		
		/*
		 * Potentially respect specializations
		 */
		SkillSpecialization specKey = null;
		if (specName!=null) {
			for (SkillSpecialization tmp : skill.getSpecializations()) {
				if (tmp.getId().startsWith(specName) || tmp.getName().toLowerCase().startsWith(specName)) {
					specKey = tmp;
					break;
				}
			}
			if (specKey==null) {
				String error = "Unknown specialization. Valid keys are:";
				for (SkillSpecialization tmp : skill.getSpecializations()) {
					error += "\n* "+tmp.getId()+" or '"+tmp.getName()+"'";
				}
				channel.sendMessage(error).queue();
				return;
			}
			
			pool = ShadowrunTools.getSkillPool(model, skill, skill.getAttribute1(), specKey.getId());
		}
		
		DiceRollResult result = bot.rollSkill(channel, model, skill, specKey, pool, edge, wild);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
		// TODO Auto-generated method stub

	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
