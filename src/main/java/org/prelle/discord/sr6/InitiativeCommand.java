package org.prelle.discord.sr6;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.DiscordGamingSession;
import org.prelle.discord.InitiativeTracker;
import org.prelle.discord.PerGuildConfig;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCharacter;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class InitiativeCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	protected final static String CONFIRM = "U+2714";
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant INI_ROLL = new CommandVariant("roll", " *in(?:itiative|itiativ|itiati|itia|iti|it|i)? *r(?:oll|ol|o)? *"  , "initiative roll", CommandUser.ANYONE) ;
	private final static CommandVariant INI_SHOW = new CommandVariant("show", " *in(?:itiative|itiativ|itiati|itia|iti|it|i)? *s(?:how|ow|o)? *"  , "initiative show", CommandUser.ANYONE) ;
	private final static CommandVariant INI_SET  = new CommandVariant("set" , " *in(?:itiative|itiativ|itiati|itia|iti|it|i)? *([0-9]*) *"  , "initiative <number>", CommandUser.ANYONE) ;
	
	private SR6Bot bot;
	private CommandVariant[] variants;

	public static void main(String[] args) {
		String val = "ini roll";
		System.out.println("Input: ["+val+"]");
		for (CommandVariant var : new CommandVariant[] {INI_ROLL, INI_SHOW}) {
		Matcher matcher = var.getPattern().matcher(val);
		System.out.println("RegEx: ["+var.getPattern().pattern()+"]");
		System.out.println(matcher.matches());
		}
	}
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(InitiativeCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public InitiativeCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				INI_ROLL,
				INI_SHOW,
				INI_SET,
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "command."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		TextChannel channel = (TextChannel) event.getChannel();
		Guild guild = event.getGuild();
		User author = event.getAuthor();
		DiscordGamingSession<SR6Volatile> session = bot.getSession(guild, channel);
		
		// There must be a running session (meaning: an active gamemaster)
		if (session.getGamemaster()==null) {
			channel.sendMessage("No session running").queue();
			return;
		}
		
		SR6Volatile volat = session.getVolatile(author);
		if (volat==null) {
			channel.sendMessage(author.getAsMention()+" you are not a player in this session.\nUse **"+bot.getCommandChar(guild)+"session join** to ask for permission to join").queue();
			return;
		}
		
		PerGuildConfig guildConfig = bot.getGuildConfig(channel);
		// Check if the user input shall be cleaned after some time
		if (guildConfig!=null && guildConfig.isAutoCleanEnabled() && event.getMessage()!=null) {
			bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
		}
		
		if (cmd==INI_ROLL) {
			ShadowrunCharacter model = bot.getLinkedCharacter(event.getAuthor());
			if (model==null) {
				String error = getTranslation(locale, "error.no_linked_character");
				channel.sendMessage(error).queue();
				return;
			}
			int base = model.getAttribute(Attribute.INITIATIVE_PHYSICAL).getModifiedValue();
			int dice = model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifiedValue();
			
			RollResult result = DiceRoller.execute(dice+"d6+"+base, null, null);
			logger.info("Roll result: "+result);
			session.getIniTracker().modify(volat, result.getValue());
			// prepare Embed
			EmbedBuilder embed = new EmbedBuilder();
			embed.setTitle(model.getName());
			embed.setImage("attachment://result.png") // we specify this in sendFile as "cat.png"
			.setDescription(Attribute.INITIATIVE_PHYSICAL.getName()+": "+model.getAttribute(Attribute.INITIATIVE_PHYSICAL).getModifiedValue()+" + "
					+model.getAttribute(Attribute.INITIATIVE_DICE_PHYSICAL).getModifiedValue()+"d6")
			.setColor(255*65536 + 0*256 + 0xA0)
			.setThumbnail("attachment://success.png")
			;
			if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
				embed.setFooter("This message is going to destroy itself in "+SR6Bot.TIMEOUT_ROLL_RESULT+" minutes");
				bot.expireMessage(event.getMessage(), Instant.now().plus(SR6Bot.TIMEOUT_ROLL_COMMAND, ChronoUnit.MINUTES));
			}
			byte[] thumbnail = SR6DiceGraphicGenerator.generateEGImage(true, result.getValue(), false);
			byte[] image = SR6DiceGraphicGenerator.generateNew(result.getDiceWithoutGroups());
			
			// Send embed with images
			channel.sendFile(image, "result.png")
			.addFile(thumbnail, "success.png")
			.embed(embed.build())
			.queue(message -> {
				if (guildConfig!=null && guildConfig.isAutoCleanEnabled()) {
					bot.expireMessage(message, Instant.now().plus(SR6Bot.TIMEOUT_ROLL_RESULT, ChronoUnit.MINUTES));
				}
			});

		} else if (cmd==INI_SET) {
			int val = Integer.parseInt(matcher.group(1));
			session.getIniTracker().add(val, volat);
			channel.sendMessage("Your initiative is now "+val).queue();

		} else if (cmd==INI_SHOW) {
			StringBuffer buf = new StringBuffer();
			for (InitiativeTracker<SR6Volatile>.IniPhase phase : session.getIniTracker().getIniPhases()) {
				List<String> perPhase = new ArrayList<String>();
				for (SR6Volatile vol : phase.list) {
					perPhase.add(vol.getName());
				}
				buf.append(String.format("%2d : %s", phase.ini, String.join(", ", perPhase))+"\n");
			}
			channel.sendMessage(buf).queue();
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData) {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
