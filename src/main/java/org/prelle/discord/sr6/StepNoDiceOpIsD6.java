package org.prelle.discord.sr6;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.CommonExecutionFlag;
import org.prelle.rollbot.DiceRoller;
import org.prelle.rollbot.ExecutionFlags;
import org.prelle.rollbot.Operator;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;

/**
 * When the elements do not contain a dice operator, treat the whole thing as a 
 * pool of D6
 * @author prelle
 *
 */
public class StepNoDiceOpIsD6 implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.sr6");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.FINISHING;
	}

	//-------------------------------------------------------------------
	/**
	 * If any resolved variable is the attribute EDGE, add the WILD flag
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		if (!value.isPoolRoll()) {
			value.setValue(value.getRawElements().get(0).getValue());
			return;
		}
		
		// There should be only 1 item
		if (value.getRawElements().size()!=1) {
			logger.error("Expect only one element but found "+value.getRawElements());
			return;
		}
		if (value.getRawElements().get(0).getValue()==null) {
			logger.error("Expect result element to have a result value: "+value.getRawElements().get(0));
			return;
		}
		
		// No dice operator found - treat as pool
		ParsedElement group = new ParsedElement(ParsedElementType.NUMBER, value.getRawElements().get(0).getValue());
		group.setName(value.getRawElements().get(0).getName());
		if (value.getRawElements().get(0).getValue()>40) {
			logger.warn("User tried to roll "+value.getRawElements().get(0).getValue()+" dice");
			value.setMessage("G.O.D.: Misuse detected. Assigning bad dice karma to user");
			return;
		}
		ParsedElement diceOp= new ParsedElement(ParsedElementType.OPERATOR, Operator.DICE);
		ParsedElement dType = new ParsedElement(ParsedElementType.NUMBER, 6);
		
		List<ParsedElement> toExecute = new ArrayList<>();
		toExecute.add(group);
		toExecute.add(diceOp);
		toExecute.add(dType);
//		toExecute.add(new ParsedElement(ParsedElementType.FLAG, value.isPoolRoll()?"POOL":"NO_POOL"));
		logger.info("No dice operator found - treat as d6 pool\n  "+value.getRawElements());
		for (ExecutionFlags flag : value.getFlags()) {
			toExecute.add(new ParsedElement(ParsedElementType.FLAG, String.valueOf(flag)));
		}
		RollResult result2 = DiceRoller.execute(toExecute, value.getVarResolver(), value.getFlagResolver(), value.getHooks());
		logger.info("..result2="+result2.getRawElements());
		value.getRawElements().clear();
		value.getRawElements().addAll(result2.getRawElements());
		value.setRolled(result2.getRolled());
		
		value.setValue(result2.getRawElements().get(0).getValue());
	}

}
