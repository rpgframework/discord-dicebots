package org.prelle.discord.sr6;

import org.prelle.shadowrun6.Skill;

/**
 * @author prelle
 *
 */
public class SkillNotUsableException extends RuntimeException {
	
	private static final long serialVersionUID = -5752741351664584180L;

	private Skill skill;

	//-------------------------------------------------------------------
	public SkillNotUsableException(Skill skill) {
		this.skill = skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

}
