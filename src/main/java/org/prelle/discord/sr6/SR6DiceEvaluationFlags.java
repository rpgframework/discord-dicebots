package org.prelle.discord.sr6;

import org.prelle.rollbot.DiceEvaluationFlag;

/**
 * @author prelle
 *
 */
public enum SR6DiceEvaluationFlags implements DiceEvaluationFlag {

	WILD,
	HIT_NOT_COUNTED,
	THREE_HITS

}
