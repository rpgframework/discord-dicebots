package org.prelle.discord.sr6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.Operator;
import org.prelle.rollbot.ParsedElement;
import org.prelle.rollbot.ParsedElementType;
import org.prelle.rollbot.PhaseHook;
import org.prelle.rollbot.RollPhase;
import org.prelle.rollbot.RollResult;
import org.prelle.rollbot.RolledDie;

/**
 * @author prelle
 *
 */
public class StepEvaluateHits implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord.sr6");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.FINISHING;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		if (!value.isPoolRoll()) {
			logger.debug("No pool roll - don't treat as hits");
			return;
		}
		logger.debug("Is pool roll - treat as hits");
		
		int totalHits = 0;
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.getType()==ParsedElementType.OPERATOR && ((Operator)elem.getRawValue())==Operator.DICE) {
				int five = 0;
				int six  = 0;
				int ones = 0; 
				boolean wildIsOne = false;
				boolean wildIsSix = false;
				for (RolledDie die : elem.getDice()) {
					switch (die.getFace()) {
					case 1: 
						ones++;
						if (die.hasFlag(SR6DiceEvaluationFlags.WILD)) {
							wildIsOne = true;
						}
						break;
					case 5: 
						five++;
						if (die.hasFlag(SR6DiceEvaluationFlags.WILD)) {
							die.addFlag(SR6DiceEvaluationFlags.THREE_HITS);
							wildIsSix = true;
						}
						break;
					case 6:
						six++;
						if (die.hasFlag(SR6DiceEvaluationFlags.WILD)) {
							die.addFlag(SR6DiceEvaluationFlags.THREE_HITS);
							wildIsSix = true;
						}
						break;
					}
				}
				int hits = 0;
				if (wildIsOne) {
					logger.info(elem+" has a wild die with a '1' - only count 6es:  "+six);
					hits = six;
					elem.setValue(six);					
					for (RolledDie die : elem.getDice()) {
						if (die.getFace()==5) {
							die.addFlag(SR6DiceEvaluationFlags.HIT_NOT_COUNTED);
						}
					}
					totalHits+=hits;
				} else {
					hits = six + five;
					if (wildIsSix) {
						logger.info(elem+" has a wild die with a hit - count as 3 hits");
						hits+=2; // Count two additional hits
					}
					logger.info(elem+" has "+hits+" hits");
					elem.setValue(hits);
					totalHits+=hits;
				}
				// Check for glitches
				logger.info("Elem "+elem+"  has "+hits+" hits and "+ones+" ones    size="+elem.getDice().size());
				if (ones>(elem.getDice().size()/2)) {
					logger.info("is glitch");
					if (hits>0) {
						value.addFlag(SR6RollEvaluationFlag.GLITCH);
					} else {
						value.addFlag(SR6RollEvaluationFlag.CRITICAL_GLITCH);
					}
				}
			}
			
		}
		value.setValue(totalHits);
	}

}
