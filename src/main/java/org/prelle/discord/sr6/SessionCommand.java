package org.prelle.discord.sr6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.BotCommand;
import org.prelle.discord.CommandUser;
import org.prelle.discord.CommandVariant;
import org.prelle.discord.DiscordGamingSession;
import org.prelle.rollbot.RollResult;
import org.prelle.shadowrun6.Attribute;
import org.prelle.shadowrun6.ShadowrunCharacter;
import org.prelle.shadowrun6.gamemaster.GMSheet;
import org.prelle.shadowrun6.print.CharacterPrintUtil.ColorScheme;
import org.prelle.shadowrun6.print.PrintScheme;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class SessionCommand implements BotCommand<SR6Volatile> {

	private static Logger logger = LogManager.getLogger("discord.sr6");
	protected final static String CONFIRM = "U+2714";
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant SESSION_START = new CommandVariant("start", " *s(?:ession|essio|essi|ess|es|e)? +st(?:art|ar|a)? *", "session start", CommandUser.ANYONE);
	private final static CommandVariant SESSION_STOP  = new CommandVariant("stop" , " *s(?:ession|essio|essi|ess|es|e)? +st(?:op|o)? *"    , "session stop", CommandUser.ANYONE) ;
	private final static CommandVariant SESSION_JOIN  = new CommandVariant("join" , " *s(?:ession|essio|essi|ess|es|e)? +j(?:oin|oi|o)? *" , "session join", CommandUser.ANYONE) ;
	private final static CommandVariant SESSION_SHOW  = new CommandVariant("show" , " *s(?:ession|essio|essi|ess|es|e)? +sho(?:w)? *" , "session show", CommandUser.ANYONE) ;
	private final static CommandVariant SESSION_SHEET = new CommandVariant("sheet", " *s(?:ession|essio|essi|ess|es|e)? +she(?:et|e)? *" , "session sheet", CommandUser.ANYONE) ;
	private final static CommandVariant SESSION_SHEET_SELF = new CommandVariant("sheet", " *s(?:ession|essio|essi|ess|es|e)? +she(?:et|e)? *@self *" , "session sheet @self", CommandUser.ANYONE) ;
	
	private SR6Bot bot;
	private CommandVariant[] variants;

	public static void main(String[] args) {
		String val = " sess  shee @self";
		System.out.println("Input: ["+val+"]");
		for (CommandVariant var : new CommandVariant[] {SESSION_START, SESSION_STOP, SESSION_JOIN, SESSION_SHEET, SESSION_SHEET_SELF}) {
		Matcher matcher = var.getPattern().matcher(val);
		System.out.println("RegEx: ["+var.getPattern().pattern()+"]");
		System.out.println(matcher.matches());
		}
	}
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(SessionCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 */
	public SessionCommand(SR6Bot bot) {
		this.bot = bot;
		variants = new CommandVariant[] {
				SESSION_START,
				SESSION_STOP,
				SESSION_JOIN,
				SESSION_SHOW,
				SESSION_SHEET,
				SESSION_SHEET_SELF
		};
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "session."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		logger.debug("Process variant "+cmd.getId()+" with "+matcher);
		TextChannel channel = (TextChannel) event.getChannel();
		Guild guild = event.getGuild();
		User author = event.getAuthor();
		DiscordGamingSession<SR6Volatile> session = bot.getSession(guild, channel);
		
		// TODO Auto-generated method stub
		if (cmd==SESSION_START) {
			// Check if session already has an gamemaster
			if (session.getGamemaster()==null) {
				session.setGamemaster(author);
				channel.sendMessage(author.getAsMention()+", is now the gamemaster in this channel").queue();
			} else {
				if (session.getGamemaster().equals(author)) {
					channel.sendMessage("You already are the gamemaster").queue();
				} else {
					channel.sendMessage("Currently **"+session.getGamemaster().getName()+"** is the gamemaster here.\n(S)He needs to type **"+bot.getCommandChar(guild)+"gm stop** to close his session").queue();
				}
			}
		} else if (cmd==SESSION_STOP) {
			// Check if session already has an gamemaster
			if (session.getGamemaster()==null) {
				channel.sendMessage("No session running").queue();
			} else {
				if (session.getGamemaster().equals(author)) {
					session.clear();
					channel.sendMessage("You are not the gamemaster anymore. The game session has stopped.").queue();
				} else {
					channel.sendMessage("Currently **"+session.getGamemaster().getName()+"** is the gamemaster here. Only (s)he can stop the session.").queue();
				}
			}
		} else if (cmd==SESSION_JOIN) {
			// Offer to join the current session
			if (session.getGamemaster()==null) {
				channel.sendMessage("There is currently no session running").queue();
				return;
			}


			channel.sendMessage(session.getGamemaster().getAsMention()+" The user **"+author.getName()+"** begs to join your session.\nUse the reaction if you want to allow him/her to bath in your presence.").queue(message -> {
				session.addJoinRequest(author, message.getIdLong());
				bot.waitForReaction(this, cmd, channel, message, Instant.now().plusSeconds(2*60), null);
				if (guild.getSelfMember().hasPermission(channel, Permission.MESSAGE_ADD_REACTION)) {
					message.addReaction(CONFIRM).queue();
				} else {
					channel.sendMessage("Missing MESSAGE_ADD_REACTION permission - the GM must explicitly add the 'check' reaction").queue();
				}
			});;

			// Remove previous request message
			if (guild.getSelfMember().hasPermission(channel, Permission.MESSAGE_MANAGE)) {
				try {
					channel.deleteMessageById(event.getMessageIdLong()).queue();
				} catch (Exception e) {
					logger.warn("Failed to delete join message in channel "+channel.getName()+": "+e);
				}
			}
		} else if (cmd==SESSION_SHOW) {
			if (session.getGamemaster()==null) {
				channel.sendMessage("There is currently no session running").queue();
				return;
			}
			StringBuffer buf = new StringBuffer();
			buf.append("**Gamemaster**: "+session.getGamemaster().getName()+"\n\n");
			buf.append("**Players:**");
			List<User> players = session.getPlayer();
			Collections.sort(players, new Comparator<User>() {
				public int compare(User o1, User o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			for (User player : players) {
				buf.append("\n* "+player.getName());
			}
			
			channel.sendMessage(buf.toString()).queue();
		} else if (cmd==SESSION_SHEET || cmd==SESSION_SHEET_SELF) {
			// Generate a gamemaster sheet
			if (session.getGamemaster()==null) {
				channel.sendMessage("There is currently no session running").queue();
				return;
			}
			if (!session.getGamemaster().equals(author)) {
				channel.sendMessage("This command can only be performed by the gamemaster.").queue();
				return;
			}

			 Map<String,ShadowrunCharacter> map = new HashMap<>();
			for (User player : session.getPlayer()) {
				ShadowrunCharacter model = bot.getLinkedCharacter(player);
				if (model!=null) {
					map.put(player.getName(), model);
				}
			}
			logger.info("Map = "+map);
			if (map.isEmpty()) {
				channel.sendMessage("No players with linked characters in your session.").queue();
				return;
			}
			GMSheet sheet = new GMSheet();
			PrintScheme scheme = new PrintScheme(true);
			try {
				Path tmpPDF = Files.createTempFile("dicebot-gmsheet", ".pdf");
				String filename = tmpPDF.toString();
				sheet.createPdf(filename, map, scheme, ColorScheme.DARK_PINK);
				logger.info("File "+tmpPDF+" is "+Files.size(tmpPDF)+" bytes");
//				Message message = new MessageBuilder().append("My message").build();
				byte[] data = Files.readAllBytes(tmpPDF);
				logger.info("Read "+data.length+" bytes");
				MessageChannel sendOn = channel;
				if (cmd==SESSION_SHEET_SELF) {
					sendOn = author.openPrivateChannel().complete();
				}
				
				sendOn.sendFile(data, "GMSheet.pdf").queue(ev -> {
					try {
						Files.deleteIfExists(tmpPDF);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				});
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData) {
		logger.warn("onMessageReactionAdd: to "+cmd+" reacts "+event);
		
		if (cmd==SESSION_JOIN) {
			logger.info("Reaction to JOIN is "+event.getReaction()+" / "+event.getReactionEmote());
			if (event.getReactionEmote().getAsCodepoints().equals(CONFIRM)) {
				logger.info("Confirmed");
				DiscordGamingSession<SR6Volatile> session = bot.getSession(event.getGuild(), event.getChannel());
				if (event.getUser().equals(session.getGamemaster())) {
					logger.info("Gamemaster confirmed for "+message.getAuthor()+"  userData="+userData);
					User requestUser = session.getJoinRequestUser(message.getIdLong());
					logger.info("Request user = "+requestUser);
					Member member = message.getGuild().retrieveMember(requestUser).complete();
					logger.info("Member= "+member);
					SR6Volatile volat = session.getVolatile(requestUser);
					if (volat==null) {
						volat = new SR6Volatile();
						volat.setLinkedUser(requestUser);
						volat.setLinkedMember(member);
						volat.setName(member.getEffectiveName());
						session.addPlayer(requestUser, volat);
					}
					logger.info("Volatile = "+volat);
					
					// Eventually fill with data from character
					ShadowrunCharacter model = bot.getLinkedCharacter(requestUser);
					if (model!=null) {
						logger.debug("Configure volatile with loaded character");
						volat.setEdge(model.getAttribute(Attribute.EDGE).getModifiedValue());
					}
					
					if (volat!=null) {
						session.addPlayer(requestUser, volat);
						event.getTextChannel().sendMessage(getTranslation(Locale.ENGLISH, "message.player_joined", member.getEffectiveName())).queue();
					}
				} else {
					logger.info("Confirmation from someone who is not gamemaster  (expect "+session.getGamemaster()+" but got "+event.getUser());
				}
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
	}

	@Override
	public CommandData getCommandData(Locale loc) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
