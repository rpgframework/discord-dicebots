/**
 * 
 */
package org.prelle.discord;

import java.util.Locale;

/**
 * @author prelle
 *
 */
public class NoJavaFXLauncher {
    
    public static void main(String[] args) {
//        System.setProperty("prism.order", "sw");
//        System.setProperty("prism.text", "t2k");
//        System.setProperty("java.awt.headless", "true");
//        System.setProperty("glass.plattform", "Monocle");
//        System.setProperty("monocle.plattform", "Headless");
//        System.setProperty("jdk.gtk.verbose", "true");
        
        Locale.setDefault(Locale.ENGLISH);
    	GraphicalDiceBot.main(args);
    }
 
}
