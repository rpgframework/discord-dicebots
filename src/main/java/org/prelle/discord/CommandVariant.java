package org.prelle.discord;

import java.util.regex.Pattern;

/**
 * @author prelle
 *
 */
public class CommandVariant {
	
	private String id;
	private Pattern pattern;
	private String command;
	private CommandUser minimalUser;

	//-------------------------------------------------------------------
	/**
	 */
	public CommandVariant(String id, String regex, String cmd, CommandUser minimal) {
		pattern = Pattern.compile(regex);
		this.command = cmd;
		this.id = id;
		this.minimalUser = minimal;
	}

	//-------------------------------------------------------------------
	public Pattern getPattern() {
		return pattern;
	}

	//-------------------------------------------------------------------
	public String getCommand() {
		return command;
	}

	//-------------------------------------------------------------------
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	public CommandUser getMinimalUser() {
		return minimalUser;
	}

}
