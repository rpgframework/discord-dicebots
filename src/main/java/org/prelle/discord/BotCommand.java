package org.prelle.discord;

import java.util.Locale;
import java.util.regex.Matcher;

import org.prelle.rollbot.RollResult;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public interface BotCommand<V extends VolatileCharacterData> {
	
	public CommandData getCommandData(Locale loc);

//	public String getName();
//
//	public String getDescription(Locale loc);

	public CommandVariant[] getCommands();
	
	public String getCommandExplanations(CommandVariant cmd, Locale loc);
	
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale);
	
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event, Object userData);

	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret, Object userData);
	
	public void onSlashCommand(SlashCommandEvent event);

}
