package org.prelle.discord;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;

/**
 * @author prelle
 *
 */
public class VolatileCharacterData {

	private User linkedUser;
	private Member linkedMember;
	private String name;

	//-------------------------------------------------------------------
	/**
	 */
	public VolatileCharacterData() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the linkedUser
	 */
	public User getLinkedUser() {
		return linkedUser;
	}

	//-------------------------------------------------------------------
	/**
	 * @param linkedUser the linkedUser to set
	 */
	public void setLinkedUser(User linkedUser) {
		this.linkedUser = linkedUser;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the linkedMember
	 */
	public Member getLinkedMember() {
		return linkedMember;
	}

	//-------------------------------------------------------------------
	/**
	 * @param linkedMember the linkedMember to set
	 */
	public void setLinkedMember(Member linkedMember) {
		this.linkedMember = linkedMember;
	}

}
