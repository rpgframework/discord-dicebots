package org.prelle.discord;

/**
 * @author prelle
 *
 */
public enum CommandUser {

	ADMIN,
	GAMEMASTER,
	ANYONE,
	
}
