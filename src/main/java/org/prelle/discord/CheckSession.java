package org.prelle.discord;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.dv8tion.jda.api.entities.User;

public class CheckSession {
	public String title;
	public String message;
	public Map<User, String> results = new HashMap<User, String>();
	public String commands;
	public String reactionCommand;
	public Locale locale;
}