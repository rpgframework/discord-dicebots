package org.prelle.discord;

import java.lang.reflect.Constructor;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.discord.sr6.ConfigureCommand;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.Interaction;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

/**
 * @author prelle
 *
 */
public class BotSkeleton<V extends VolatileCharacterData> extends ListenerAdapter {
	
	public class ReactionWaitObject {
		public BotCommand command;
		public CommandVariant variant;
		public Message message;
		public Instant until;
		public MessageChannel channel;
		public Object userData;
		public ReactionWaitObject(BotCommand cmd, CommandVariant v, MessageChannel c, Message m, Instant u, Object userData) {
			this.command = cmd;
			this.variant = v;
			this.message = m;
			this.until = u;
			this.channel = c;
			this.userData= userData;
		}
	}
	

	protected static Logger logger = LogManager.getLogger("discord");

	protected final static char COMMAND_CHAR = '<';
	protected final static String CONFIRM = "U+2714";

	protected Map<String, PerGuildConfig> perGuildConfigs;
	protected Map<User, Map<CombatParticipant, V>> volatiles;

	protected DiscordSQL database;
	protected String botID;
	protected JDA jda;

	protected List<BotCommand<V>> commands;
	protected TwoLayerStorage<Guild, MessageChannel, DiscordGamingSession<V>> sessions;
	protected Class<V> volatileClass;
	
	protected Map<Long, ReactionWaitObject> awaitedReactions;
	
	protected Timer timer;
	protected Map<Message, Instant> messageExpirations;
	protected Map<InteractionHook, Instant> interactionExpirations;
	
	private transient Map<Long,BotCommand> commandsByLong;

	//-------------------------------------------------------------------
	public BotSkeleton(DiscordSQL database, String botID, Class<V> volatileClass) {
		this.database = database;
		this.volatileClass = volatileClass;
		this.botID    = botID;
		commands      = new ArrayList<BotCommand<V>>();
		perGuildConfigs = new HashMap<String, PerGuildConfig>();
		volatiles = new HashMap<User, Map<CombatParticipant,V>>();
		awaitedReactions = new HashMap<Long, BotSkeleton<V>.ReactionWaitObject>();
		commandsByLong = new HashMap<Long,BotCommand>();

		try {
			Class<DiscordGamingSession<V>> clazz = (Class<DiscordGamingSession<V>>) (new DiscordGamingSession<V>()).getClass();
			Constructor<DiscordGamingSession<V>> emptyConst = clazz.getConstructor();
			sessions = new TwoLayerStorage<Guild, MessageChannel, DiscordGamingSession<V>>(emptyConst);
		} catch (Exception e) {
			throw new RuntimeException("Failed setting up session maps");
		}
		
		/*
		 * Delete messages that have expired
		 */
		messageExpirations = new HashMap<Message, Instant>();
		interactionExpirations = new HashMap<InteractionHook, Instant>();
		timer         = new Timer(botID);
		timer.schedule(new TimerTask() {
			public void run() {
				expireMessages();
				expireInteractions();
				expireAwaitedReactions();
			}
		}, 60000, 60000);
		
		commands.add((BotCommand<V>) new HelpCommand(this));
	}

	//-------------------------------------------------------------------
	public char getCommandChar(@Nullable Guild guild) {
		PerGuildConfig cfg = getGuildConfig(guild);
		if (cfg!=null)
			return cfg.getCommandChar();
		return COMMAND_CHAR;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void addCommand(BotCommand command) {
		if (!commands.contains(command))
			commands.add(command);
	}

	//-------------------------------------------------------------------
	public List<BotCommand<V>> getCommands() {
		return new ArrayList<BotCommand<V>>(commands);
	}
	
	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	public void waitForReaction(BotCommand cmd, CommandVariant variant, MessageChannel channel, Message message, Instant until, Object userData) {
		logger.info("Wait for reactions to "+message.getId()+" until "+until);
		
		ReactionWaitObject wait = new ReactionWaitObject(cmd, variant, channel, message, until, userData);
		awaitedReactions.put(message.getIdLong(), wait);
	}

	//-------------------------------------------------------------------
	public ReactionWaitObject getRecentWaitObject(MessageChannel channel) {
		List<ReactionWaitObject> allFromChannel = awaitedReactions.values().stream().filter(wait -> wait.channel==channel).collect(Collectors.toList());
		switch (allFromChannel.size()) {
		case 0: return null;
		case 1: return allFromChannel.get(0);
		default:
			logger.warn("More than one ReactionWaitObject in channel");
			// Sort them
			Collections.sort(allFromChannel, new Comparator<ReactionWaitObject>() {
				public int compare(BotSkeleton<V>.ReactionWaitObject o1, BotSkeleton<V>.ReactionWaitObject o2) {
					return -1*o1.until.compareTo(o2.until);
				}
			});
			return allFromChannel.get(0);
		}
	}

	//-------------------------------------------------------------------
	public void expireMessage(Message message, Instant expireTime) {
		synchronized (messageExpirations) {
			logger.debug("Mark for deletion after "+expireTime);
			messageExpirations.put(message, expireTime);
		}
	}

	//-------------------------------------------------------------------
	public void expireInteraction(InteractionHook message, Instant expireTime) {
		synchronized (interactionExpirations) {
			logger.debug("Mark "+message.getInteraction().getIdLong()+" for deletion after "+expireTime);
			interactionExpirations.put(message, expireTime);
		}
	}

	//-------------------------------------------------------------------
	private void expireMessages() {
		Instant now = Instant.now();
		List<Message> toDelete = new ArrayList<Message>();
		synchronized (messageExpirations) {
//			logger.debug("expireMessages("+messageExpirations.size()+")");
			for  (Entry<Message,Instant> entry : messageExpirations.entrySet()) {
//				logger.debug("  check "+entry.getKey()+" in "+entry.getValue());
				if (entry.getValue().isBefore(now)) {
					toDelete.add(entry.getKey());
				}
			}
		}
		// Really delete
		try {
			toDelete.forEach(msg -> {
				messageExpirations.remove(msg);
				logger.debug("Delete "+msg);
				try {
					msg.getChannel().deleteMessageById(msg.getIdLong()).queue();
				} catch (Exception e) {
					logger.warn("Cannot delete message in channel "+msg.getChannel()+" on server "+msg.getGuild());
				}			
			});
		} catch (Exception e) {
			logger.error("Problem deleting: "+e);
		}
	}

	//-------------------------------------------------------------------
	private void expireAwaitedReactions() {
		Instant now = Instant.now();
		List<Long> toDelete = new ArrayList<Long>();
		synchronized (awaitedReactions) {
			for  (Entry<Long,ReactionWaitObject> entry : awaitedReactions.entrySet()) {
				if (now.isAfter(entry.getValue().until)) {
					logger.debug("  stop waiting for "+entry.getValue().variant);
					toDelete.add(entry.getKey());
				}
			}
		}
		// Really delete
		toDelete.forEach(msg -> {
			awaitedReactions.remove(msg);
			logger.debug("Don't wait anymore for reactions to "+msg);
			awaitedReactions.remove(msg);
		});
	}

	//-------------------------------------------------------------------
	private void expireInteractions() {
		Instant now = Instant.now();
		List<InteractionHook> toDelete = new ArrayList<InteractionHook>();
		synchronized (interactionExpirations) {
//			logger.debug("expireMessages("+messageExpirations.size()+")");
			for  (Entry<InteractionHook,Instant> entry : interactionExpirations.entrySet()) {
//				logger.debug("  check "+entry.getKey()+" in "+entry.getValue());
				if (entry.getValue().isBefore(now)) {
					toDelete.add(entry.getKey());
				}
			}
		}
		// Really delete
		try {
			toDelete.forEach(msg -> {
				interactionExpirations.remove(msg);
				logger.debug("Delete "+msg.getInteraction().getIdLong());
				try {
					msg.deleteOriginal().queue();
//					msg.deleteMessageById(msg.getInteraction().getIdLong()).queue();
				} catch (Exception e) {
					logger.warn("Cannot delete message in channel "+msg.getInteraction().getChannel()+" on server "+msg.getInteraction().getGuild());
				}			
			});
		} catch (Exception e) {
			logger.error("Problem deleting: "+e);
		}
	}
	
	//-------------------------------------------------------------------
	public boolean parse(MessageReceivedEvent event, String msg) {
		Guild guild = null;
		if (event.isFromType(ChannelType.TEXT)) {
			guild = event.getTextChannel().getGuild();
		}
		char cmdChar = getCommandChar(guild);
		if (msg.charAt(0)!=cmdChar)
			return false;
		msg = msg.substring(1);
		
		for (BotCommand<V> cmd : commands) {
			try {
				for (CommandVariant tmp : cmd.getCommands()) {
					Matcher matcher = tmp.getPattern().matcher(msg);
//					logger.info("* "+tmp.getCommand()+" = "+matcher.matches());
					if (matcher.matches()) {
						logger.info("For input ["+msg+"] call "+cmd);					
						cmd.process(event, tmp, matcher, Locale.ENGLISH);
						return true;
					}
				}
			} catch (Exception e) {
				logger.error("Error executing help to "+cmd.getClass(),e);
			}
		}
		logger.info("No command found for: ["+msg+"]");
		return false;
	}
	
	//-------------------------------------------------------------------
	public Object parse(Guild guild, MessageChannel channel, Message message, String msg) {
		char cmdChar = getCommandChar(guild);
		if (msg.charAt(0)!=cmdChar)
			return false;
		msg = msg.substring(1);
		PerGuildConfig guildCfg = getGuildConfig(guild);
		
		for (BotCommand<V> cmd : commands) {
			for (CommandVariant tmp : cmd.getCommands()) {
				Matcher matcher = tmp.getPattern().matcher(msg);
				logger.info("* "+tmp.getCommand()+" = "+matcher.matches());
				if (matcher.matches()) {
					logger.warn("TODO Call "+cmd);		
					MessageReceivedEvent newEvent = new MessageReceivedEvent(jda, message.getIdLong(), message);
					cmd.process(newEvent, tmp, matcher, Locale.forLanguageTag(guildCfg.getLanguage()));
//					cmd.process(event, tmp, matcher, Locale.ENGLISH);
					return true;
				}
			}
		}
		return false;
	}

	//-------------------------------------------------------------------
	protected boolean process(MessageReceivedEvent event) {
		String msg = event.getMessage().getContentDisplay().trim();
		return parse(event, msg);
	}

	//-------------------------------------------------------------------
	@Override
	public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
		User author = event.getAuthor();                //The user that sent the message
		Message message = event.getMessage();           //The message that was received.
		logger.info("Private message received from "+author+": "+message);
		if (author.isBot())
			return;
		
//		event.getChannel().sendMessage("Processing requests on private channels is not supported").queue();
		if (event.getMessage().getAttachments().size()>0) {
			onFileUpload(event);
			return;
		}

		super.onPrivateMessageReceived(event);
	}
	
	//-------------------------------------------------------------------
	protected void onFileUpload(PrivateMessageReceivedEvent event) {
		
	}

	//-------------------------------------------------------------------
	public PerGuildConfig addGuild(Guild guild) {
		String key = guild.getId();
		PerGuildConfig config = perGuildConfigs.get(key);
		if (config==null) {
			config = database.getGuildConfiguration(botID, guild.getId());
		}
		
		if (config==null) {
			config = new PerGuildConfig(botID,guild);
			config.setGuildName(guild.getName());
			database.write(config);
		}
		perGuildConfigs.put(key, config);
		return config;
	}

	//-------------------------------------------------------------------
	public PerGuildConfig getGuildConfig(Guild guild) {
		PerGuildConfig cfg = perGuildConfigs.get(guild.getId());
		if (cfg==null) {
			cfg = database.getGuildConfiguration(botID, guild.getId());
			if (cfg!=null) {
				perGuildConfigs.put(guild.getId(), cfg);
				cfg.setGuildName(guild.getName());
				return cfg;
			}
		}
		if (cfg==null) {
			cfg = new PerGuildConfig(botID, guild);
			cfg.setGuildName(guild.getName());
			cfg.setLastInfo(Instant.now());
			database.write(cfg);
		}
		assert cfg.getCommandChar()!='ß';
		return cfg;
	}

	//-------------------------------------------------------------------
	public PerGuildConfig getGuildConfig(MessageChannel channel) {
//		logger.debug("Channel "+channel.getName()+" is textchannel = "+(channel instanceof TextChannel));
		if (channel instanceof TextChannel) {
			TextChannel  tChan = (TextChannel)channel;
			Guild guild = tChan.getGuild();
//			logger.debug("  guild: "+guild+"    : "+perGuildConfigs.get(guild.getId()));
			PerGuildConfig cfg = perGuildConfigs.get(guild.getId());
			if (cfg==null) {
				cfg = new PerGuildConfig(botID, guild);
				cfg.setGuildName(guild.getName());
				cfg.setLastInfo(Instant.now());
				database.write(cfg);
			}
			assert cfg.getCommandChar()!='ß';
			return cfg;
		}
		return null;
	}	
	
	//-------------------------------------------------------------------
	public void saveGuildConfig(PerGuildConfig value) {
		database.write(value);
	}

	//-------------------------------------------------------------------
	public void addUser(User user) {
		Map<CombatParticipant, V> map = volatiles.get(user);
		if (map==null) {
			map = new HashMap<CombatParticipant, V>();
			volatiles.put(user, map);
		}
	}

	//-------------------------------------------------------------------
	protected void parseSession(TextChannel channel, User author, Guild guild, String msg, long messageId) {
		String[] params = msg.trim().split(" ");
		logger.info("parseSession: "+Arrays.toString(params)+"   from "+author.getName()+" in channel "+channel.getName()+" on server "+guild);
		if (params.length<2) {
			channel.sendMessage("Syntax:\n"
					+COMMAND_CHAR+"session start  - Become the gamemaster in this channel\n"
					+COMMAND_CHAR+"session stop   - Stop being gamemaster").queue();
			return;
		}

		DiscordGamingSession<V> session = sessions.get(guild, channel);
		String command = params[1];
		logger.info("Command "+command);
		if ("start".equals(command)) {
			// Check if session already has an gamemaster
			if (session.getGamemaster()==null) {
				session.setGamemaster(author);
				channel.sendMessage(author.getAsMention()+", is now the gamemaster in this channel").queue();
			} else {
				if (session.getGamemaster().equals(author)) {
					channel.sendMessage("You already are the gamemaster").queue();
				} else {
					channel.sendMessage("Currently **"+session.getGamemaster().getName()+"** is the gamemaster here.\n(S)He needs to type **"+COMMAND_CHAR+"gm stop** to close his session").queue();
				}
			}
		} else if ("stop".startsWith(command.toLowerCase())) {
			// Check if session already has an gamemaster
			if (session.getGamemaster()==null) {
				channel.sendMessage("No session running").queue();
			} else {
				if (session.getGamemaster().equals(author)) {
					session.clear();
					channel.sendMessage("You are not the gamemaster anymore. The game session has stopped.").queue();
				} else {
					channel.sendMessage("Currently **"+session.getGamemaster().getName()+"** is the gamemaster here. Only (s)he can stop the session.").queue();
				}

			}
		} else if ("join".startsWith(command.toLowerCase())) {
			// Offer to join the current session
			if (session.getGamemaster()==null) {
				channel.sendMessage("There is currently no session running").queue();
				return;
			}

			channel.sendMessage(session.getGamemaster().getAsMention()+" The user **"+author.getName()+"** begs to join your session.\nUse the reaction if you want to allow him/her to bath in your presence.").queue(message -> {
				session.addJoinRequest(author, message.getIdLong());
				message.addReaction(CONFIRM).queue();
			});;

			// Remove previous request message
			if (guild.getSelfMember().hasPermission(channel, Permission.MESSAGE_MANAGE)) {
				try {
					channel.deleteMessageById(messageId).queue();
				} catch (Exception e) {
					logger.warn("Failed to delete join message in channel "+channel.getName()+": "+e);
				}
			}
		} else if ("show".startsWith(command.toLowerCase())) {
			if (session.getGamemaster()==null) {
				channel.sendMessage("There is currently no session running").queue();
				return;
			}
			StringBuffer buf = new StringBuffer();
			buf.append("**Gamemaster**: "+session.getGamemaster().getName()+"\n\n");
			buf.append("**Players:**");
			List<User> players = session.getPlayer();
			Collections.sort(players, new Comparator<User>() {
				public int compare(User o1, User o2) {
					return o1.getName().compareTo(o2.getName());
				}
			});
			for (User player : players) {
				buf.append("\n* "+player.getName());
			}
			
			channel.sendMessage(buf.toString()).queue();
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onMessageReactionAdd(net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent)
	 */
	@Override
	public void onMessageReactionAdd(MessageReactionAddEvent event) {
		User author = event.getUser(); 
		if (author.isBot())
			return;
		
		logger.debug("onMessageReactionAdd("+event+")");
		MessageChannel channel = event.getChannel();
		
		// HELP: Is the ID unique per server or per channel ?
//		logger.warn("isEmoji = "+event.getReactionEmote().isEmoji());
//		logger.warn("isEmote = "+event.getReactionEmote().isEmote());
		ReactionWaitObject wait = awaitedReactions.get(event.getMessageIdLong());
		if (wait!=null) {
			logger.info("Reaction is expected");
			wait.command.onMessageReactionAdd(wait.variant, wait.message, event, wait.userData);
			return;
		}
		logger.debug("Reaction is unexpected");
		
		Guild guild = event.getGuild();
		if (guild==null)
			return;
		
		boolean isConfirmed = false;
		try {
			if (event.getReactionEmote().isEmoji()) {
				logger.debug("Emoji "+event.getReactionEmote().getEmoji());
				isConfirmed = event.getReactionEmote().getEmoji().equals(CONFIRM);
			} else if (event.getReactionEmote().isEmoji()) {
				isConfirmed = event.getReactionEmote().getAsCodepoints().equals(CONFIRM);
			}
		} catch (IllegalStateException e) {
			try {
				isConfirmed = event.getReactionEmote().getEmoji().equals(CONFIRM);
			} catch (Exception e1) {
				logger.warn("Problem in server "+channel.getName()+" for player "+author.getName(),e1);
				channel.sendMessage("Problem processing reaction: "+e1.getMessage()).queue();
			}
		} catch (Exception e) {
			logger.warn("Problem in server "+channel.getName()+" for player "+author.getName(),e);
			channel.sendMessage("Problem processing reaction: "+e.getMessage()).queue();
		}
		DiscordGamingSession<V> session = sessions.get(guild, channel);
		if (session.getGamemaster()!=null && session.getGamemaster().equals(author) && isConfirmed) {
			User user = session.getJoinRequestUser(event.getMessageIdLong());
			// Create volatile character data
			try {
				V volat = volatileClass.getConstructor().newInstance();
				String name = user.getName();
				try { name = event.getGuild().retrieveMember(event.getUser()).complete().getNickname(); } catch (Exception e) {}
				volat.setName(name);
				session.addPlayer(user, volat);
				channel.deleteMessageById(event.getMessageIdLong()).queue();
				channel.sendMessage("User "+user.getName()+" joins the session").queue();
			} catch (Exception e) {
				logger.error("Cannot create volatile characterdata object",e);
				channel.sendMessage("Sorry, an Bot error occurred: "+e.toString()).queue();
			}
		}
		
	}
	
	//-------------------------------------------------------------------
	public DiscordGamingSession<V> getSession(Guild guild, MessageChannel channel) {
		return sessions.get(guild, channel);
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onGuildJoin(net.dv8tion.jda.api.events.guild.GuildJoinEvent)
	 */
	@Override
	public void onGuildJoin(GuildJoinEvent event) {
		super.onGuildJoin(event);
		
		logger.info("onGuildJoin: "+event.getGuild().getName());
		Guild guild = event.getGuild();
		getGuildConfig(guild);
		
		
//		try {
//			setupEmotes(event.getGuild());
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}

	//-------------------------------------------------------------------
	public void registerCommand(Long idLong, BotCommand com) {
		commandsByLong.put(idLong, com);
	}

	//-------------------------------------------------------------------
	public BotCommand getCommand(Long idLong) {
		return commandsByLong.get(idLong);
	}

	//-------------------------------------------------------------------
	protected void installGlobalCommands() {
		logger.info("Configuring global commands");
		try {
			List<BotCommand> toInstall = new ArrayList<>(commands);
			// Check already known commands
			List<Command> coms = jda.retrieveCommands().complete();
			outer:
			for (Command com : coms) {
				// Does it already exist
				for (BotCommand tmp : toInstall) {
					CommandData cData = tmp.getCommandData(Locale.ENGLISH);
					if (cData != null) {
						if (com.getName().equals(tmp.getCommandData(Locale.ENGLISH).getName())) {
//							toInstall.remove(tmp);
//							logger.info("Required global Command '"+com.getName()+"' already installed");
//							registerCommand( (Long)com.getIdLong(), tmp);
							continue outer;
						}
					}
				}
				// This command should not be globally installed
				jda.deleteCommandById(com.getIdLong()).queue();
				logger.info("Delete unknown global Command "+com);
			}

			for (BotCommand com : toInstall) {
				CommandData cData = com.getCommandData(Locale.ENGLISH);
				if (cData != null) {
					logger.info("configure " + cData.getName());
					Command cmd = jda.upsertCommand(cData).complete();
					logger.info("  registered as " + cmd.getIdLong()+" / "+cmd.getId()+" / "+cmd.getName()+" globally");
					registerCommand(cmd.getIdLong(), com);
				}
			}
		} catch (Exception e) {
			logger.error("Error with slash commands",e);
		}

	}
	
	//-------------------------------------------------------------------
	protected void setupGuildCommands(Guild guild, PerGuildConfig perGuild) {
//		try {
//		Locale loc = Locale.forLanguageTag(perGuild.getLanguage());
//			List<Command> coms = guild.retrieveCommands().complete();
//			for (Command com : coms) {
////			jda.deleteCommandById(com.getIdLong()).queue();
//				logger.info("Found Guild Command "+com+" will be removed");
////			if (com.getName().equals("sr6")) {
//					try {
//						guild.deleteCommandById(com.getIdLong()).queue();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
////			}
//			}
//		} catch (ErrorResponseException e) {
//			if (e.getErrorCode()!=50001)
//				logger.error("Fauked retrieving or deleting commands",e);
//		}
//
////		for (BotCommand com : commands) {
////			CommandData cData = com.getCommandData(loc);
////			if (cData != null) {
////				if (perGuild.getCommandPrefix()!=null) {
////					cData.setName(perGuild.getCommandPrefix()+cData.getName());
////				}
////				logger.info("configure " + cData.getName());
////				Command cmd = guild.upsertCommand(cData).complete();
////				logger.info("  registered as " + cmd.getIdLong()+" / "+cmd.getId()+" / "+cmd.getName()+" in "+perGuild);
////				perGuild.registerCommand(cmd.getIdLong(), com);
////			}
////		}
	}

//	//-------------------------------------------------------------------
//	public void renameGuildCommands(Guild guild, PerGuildConfig perGuild) {
//		try {
//			List<Command> coms = guild.retrieveCommands().complete();
//			for (Command com : coms) {
//				guild.deleteCommandById(com.getIdLong()).queue();
//				logger.info("Command "+com+" removed");
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		setupGuildCommands(guild, perGuild);
//	}
//
	//-------------------------------------------------------------------
	/**
	 * @see net.dv8tion.jda.api.hooks.ListenerAdapter#onSlashCommand(net.dv8tion.jda.api.events.interaction.SlashCommandEvent)
	 */
	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		User author = event.getUser();                //The user that sent the message
		// Ignore messages from other bots
		if (author.isBot())
			return;
		
		logger.info("slash commandId  = "+event.getCommandId());
		logger.info("slash commandIdL = "+event.getCommandIdLong());
		logger.info("slash commandPath= "+event.getCommandPath());
		logger.info("slash name = "+event.getName());
		logger.info("slash subcommand name = "+event.getSubcommandName());
		logger.info("slash options    = "+event.getOptions());
		
		event.deferReply(true);
		
		event.getGuild();
		PerGuildConfig perGuild = getGuildConfig(event.getGuild());
		BotCommand<?> command = perGuild.getCommand(event.getCommandIdLong());
		if (command==null) {
			// Try global commands
			command = getCommand(event.getCommandIdLong());
		}
		if (command==null) {
			logger.error("No local or global bot command found for "+event.getCommandId()+"/"+event.getCommandPath()+" in "+perGuild);
			event.reply("Cannot execute - could not find command").queue();
		} else {
			command.onSlashCommand(event);
//			event.reply("I am done with you!").queue();
		}
	}

}
