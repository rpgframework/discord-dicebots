package org.prelle.discord;

import java.time.Instant;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.interactions.commands.Command;

/**
 * @author prelle
 *
 */
public class PerGuildConfig {
	
	private String botID;
	private String guildId;
	private String guildName;
	/** Two-character language code */
	private String language;
	private Instant lastInfo;
	private boolean autoClean = true;
	private char  commandChar = '<';
	private String  commandPrefix = null;
	private Map<MessageChannel, Object> perChannelConfigs;

	/** May be outdated, if guild data changed since Bot start */
	private transient Guild guild;
	private transient Locale locale;
	
	private transient Map<String, Emote> emotes;
	private transient Map<Long,BotCommand> commandsByLong;

	//-------------------------------------------------------------------
	public PerGuildConfig(String botID, String guildID) {
		this.botID = botID;
		this.guildId = guildID;
		this.language = "en";
		perChannelConfigs = new HashMap<MessageChannel, Object>();
		emotes = new HashMap<String, Emote>();
		commandsByLong = new HashMap<Long,BotCommand>();
	}

	//-------------------------------------------------------------------
	public PerGuildConfig(String botID, Guild guild) {
		this(botID, guild.getId());
		this.guild = guild;
	}

	//-------------------------------------------------------------------
	public String getBotId() { return botID; }
	public void setBotId(String botID) { this.botID = botID; }

	//-------------------------------------------------------------------
	public String getGuildId() { return guildId; }
	public void setGuildId(String guildId) { this.guildId = guildId; }

	//-------------------------------------------------------------------
	public String getGuildName() { return guildName; }
	public void setGuildName(String value) { this.guildName = value; }

	//-------------------------------------------------------------------
	public String getLanguage() { 
		return (language!=null)?language:"en"; 
	}
	public void setLanguage(String language) {	
		this.language = language;
		if (language!=null)
			locale = Locale.forLanguageTag(language);
	}

	//-------------------------------------------------------------------
	public void memorizeEmote(String id, Emote val) {
		emotes.put(id, val);
	}

	//-------------------------------------------------------------------
	public Emote getEmote(String key) {
		return emotes.get(key);
	}

	//-------------------------------------------------------------------
	public Instant getLastReleaseNotes() { return lastInfo; }
	public void setLastInfo(Instant lastInfo) { this.lastInfo = lastInfo; }

	//-------------------------------------------------------------------
	public boolean isAutoCleanEnabled() { return autoClean; }
	public void setAutoClean(boolean value) { this.autoClean = value; }

	//-------------------------------------------------------------------
	public char getCommandChar() { return commandChar; }
	public void setCommandChar(char commandChar) {
		if (Character.isLetter(commandChar) || Character.isWhitespace(commandChar)) {
			throw new IllegalArgumentException("Invalid command char");
		}
		this.commandChar = commandChar; }

	//-------------------------------------------------------------------
	public void registerCommand(Long idLong, BotCommand com) {
		commandsByLong.put(idLong, com);
	}

	//-------------------------------------------------------------------
	public BotCommand getCommand(Long idLong) {
		System.out.println("Find "+idLong+" in "+commandsByLong);
		return commandsByLong.get(idLong);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the commandPrefix
	 */
	public String getCommandPrefix() {
		return commandPrefix;
	}

	//-------------------------------------------------------------------
	/**
	 * @param commandPrefix the commandPrefix to set
	 */
	public void setCommandPrefix(String commandPrefix) {
		this.commandPrefix = commandPrefix;
	}
	
}
