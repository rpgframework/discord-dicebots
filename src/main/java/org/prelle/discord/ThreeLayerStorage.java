package org.prelle.discord;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class ThreeLayerStorage<G,C,U,V> {

	private Map<G, TwoLayerStorage<C,U,V>> data;
	private Constructor<V> constructor;
	
	//-------------------------------------------------------------------
	public ThreeLayerStorage(Constructor<V> emptyConst) {
		data = new HashMap<G, TwoLayerStorage<C,U, V>>();
		this.constructor = emptyConst;
	}
	
	//-------------------------------------------------------------------
	public V get(G server, C channel, U user) {
		TwoLayerStorage<C,U,V> ret = data.get(server);
		if (ret!=null)
			return ret.get(channel,user);
		try {
			ret = new TwoLayerStorage<C,U,V>(constructor);
			data.put(server, ret);
			return ret.get(channel,user);
		} catch (Exception e) {
			throw new RuntimeException("Failed instantiating object with empty constructor");
		}
	}
	
	//-------------------------------------------------------------------
	public void set(G server, C channel, U user, V val) {
		TwoLayerStorage<C,U,V> ret = data.get(server);
		if (ret==null) {
			ret = new TwoLayerStorage<C,U,V>(constructor);
			data.put(server, ret);
		}
		ret.set(channel, user, val);
	}

}
