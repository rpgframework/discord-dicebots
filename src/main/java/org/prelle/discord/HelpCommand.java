package org.prelle.discord;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.rollbot.RollResult;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

/**
 * @author prelle
 *
 */
public class HelpCommand implements BotCommand<VolatileCharacterData> {

	private static Logger logger = LogManager.getLogger("discord");
	
	private static Map<Locale, ResourceBundle> RESOURCES = new HashMap<>();
	private final static CommandVariant HELP      = new CommandVariant("help"    , "help *" , "help", CommandUser.ANYONE) ;
	
	//-------------------------------------------------------------------
	public static ResourceBundle getResources(Locale locale) {
		if (RESOURCES.containsKey(locale))
			return RESOURCES.get(locale);
		ResourceBundle bundle = ResourceBundle.getBundle(HelpCommand.class.getName(), locale);
		RESOURCES.put(locale, bundle);
		return bundle;
	}

	//-------------------------------------------------------------------
	public static String getTranslation(Locale locale, String key, Object...param) {
		ResourceBundle res = getResources(locale);
		if (res.containsKey(key))
			return String.format(res.getString(key), param);
		return key;
	}

	private CommandVariant[] variants;
	private BotSkeleton bot;

	//-------------------------------------------------------------------
	/**
	 */
	public HelpCommand(BotSkeleton bot) {
		this.bot = bot;
		variants = new CommandVariant[] {HELP};
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandData(java.util.Locale)
	 */
	@Override
	public CommandData getCommandData(Locale loc) {
		return null;
		
	}

//	//-------------------------------------------------------------------
//	@Override
//	public String getName() {
//		return "help";
//	}
//
//	//-------------------------------------------------------------------
//	@Override
//	public String getDescription(Locale loc) {
//		return null;
//	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommands()
	 */
	@Override
	public CommandVariant[] getCommands() {
		return variants;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#getCommandExplanations(org.prelle.discord.CommandVariant, java.util.Locale)
	 */
	@Override
	public String getCommandExplanations(CommandVariant cmd, Locale locale) {
		String key = "help."+cmd.getId();
		
		try {
			return getResources(locale).getString(key);
		} catch (Exception e) {
			logger.error(e.toString());
			return "Error: "+key;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#process(net.dv8tion.jda.api.events.message.MessageReceivedEvent, org.prelle.discord.CommandVariant, java.util.regex.Matcher, java.util.Locale)
	 */
	@Override
	public void process(MessageReceivedEvent event, CommandVariant cmd, Matcher matcher, Locale locale) {
		// TODO Auto-generated method stub
		StringBuffer buf = new StringBuffer("```md");
		List<BotCommand> list = bot.getCommands();
		for (BotCommand command: list) {
			for (CommandVariant var : command.getCommands()) {
				buf.append("\n"+var.getCommand()+" \t- "+command.getCommandExplanations(var, locale));
				switch (var.getMinimalUser()) {
				case ADMIN: buf.append(" < Server Admin >"); break;
				case GAMEMASTER: buf.append(" < GM >"); break;
				}
			}
		}
		buf.append("\n```");
		
		String head = getTranslation(locale, "help.header", bot.getGuildConfig(event.getChannel()).getCommandChar());
		event.getChannel().sendMessage(head+"\n"+buf).queue();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onMessageReactionAdd(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent, java.lang.Object)
	 */
	@Override
	public void onMessageReactionAdd(CommandVariant cmd, Message message, MessageReactionAddEvent event,
			Object userData) {
		// TODO Auto-generated method stub

	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.discord.BotCommand#onUserGeneratedResult(org.prelle.discord.CommandVariant, net.dv8tion.jda.api.entities.Message, net.dv8tion.jda.api.entities.User, org.prelle.discord.sr6.SR6DiceGraphicGenerator.DiceRollResult, java.lang.Object)
	 */
	@Override
	public void onUserGeneratedResult(CommandVariant variant, Message message, User author, RollResult ret,
			Object userData) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSlashCommand(SlashCommandEvent event) {
		// TODO Auto-generated method stub
		
	}

}
