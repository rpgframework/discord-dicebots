package org.prelle.discord;

/**
 * @author prelle
 *
 */
public class CombatParticipant {
	
	private String name;
	private int initiativeResult;

	//-------------------------------------------------------------------
	public CombatParticipant() {
	}

	//-------------------------------------------------------------------
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	public int getInitiativeResult() {
		return initiativeResult;
	}

	//-------------------------------------------------------------------
	public void setInitiativeResult(int initiativeResult) {
		this.initiativeResult = initiativeResult;
	}

}
