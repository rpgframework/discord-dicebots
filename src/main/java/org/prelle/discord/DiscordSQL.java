package org.prelle.discord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class DiscordSQL {
	
	private final static String TABLE_PER_GUILD = "DiscordPerGuild";

	final static Logger logger = LogManager.getLogger("discord.sql");

	private Connection con;
	private PreparedStatement writePerGuild;

	//-------------------------------------------------------------------
	public DiscordSQL(String jdbcURL, String user, String pass) throws SQLException {
		con = DriverManager.getConnection(jdbcURL, user, pass);
		
		ensurePerGuildTable();
		prepareStatements();
	}
	
	//-------------------------------------------------------------------
	private void ensurePerGuildTable() {
		Statement stat = null;
		try {
			stat = con.createStatement();
			ResultSet set = stat.executeQuery("SELECT guildId,guildName FROM "+TABLE_PER_GUILD);
			set.close();
			logger.info("Guild ok");
		} catch (SQLException e) {
			if ("42S02".equals(e.getSQLState())) {
				try {
					logger.error("2");
					stat.executeUpdate("CREATE TABLE "+TABLE_PER_GUILD+" ("
							+"  guildId  VARCHAR(64) NOT NULL,"
							+"  guildName VARCHAR(128) NOT NULL,"
							+"  botId VARCHAR(10) NOT NULL,"
							+"  lang CHAR(2),"
							+"  lastInfo TIMESTAMP NULL,"
							+"  autoclean BOOLEAN DEFAULT 'true',"
							+"  commandChar CHAR(1) DEFAULT '<',"
							+"  autoclean INTEGER DEFAULT 0,"
							+"  PRIMARY KEY (guildId, botId)"
							+")");
				} catch (SQLException e1) {
					logger.fatal("Failed setting up database",e);
					System.exit(1);
				}
			} else {
				logger.error("Unexpected error checking database: "+e.getSQLState()+" = "+e.toString());
				System.exit(1);
			}
		} finally {
			if (stat!=null) {
				try { stat.close(); } catch (Exception e) {}
			}
		}
	}
	
	//-------------------------------------------------------------------
	private void prepareStatements() {
		try {
			writePerGuild = con.prepareStatement("REPLACE INTO "+TABLE_PER_GUILD+" (guildId,guildName,botId,lang,lastInfo,commandChar,autoclean) VALUES (?,?,?,?,?,?,?)");
		} catch (SQLException e) {
			logger.error("Unexpected error preparing statements: "+e.getSQLState()+" = "+e.toString());
			System.exit(1);
		}
	}

	//-------------------------------------------------------------------
	public boolean write(PerGuildConfig data)  {
		if (data.getCommandChar()=='ß' || Character.isLetter(data.getCommandChar()))
			throw new IllegalArgumentException("Invalid command char. Letters are not allowed");
		
		try {
			writePerGuild.setString(1,data.getGuildId());
			writePerGuild.setString(2,data.getGuildName());
			writePerGuild.setString(3,data.getBotId());
			writePerGuild.setString(4,data.getLanguage());
			writePerGuild.setTimestamp(5, (data.getLastReleaseNotes()!=null)?new Timestamp(data.getLastReleaseNotes().toEpochMilli()):null);
			writePerGuild.setString(6,String.valueOf(data.getCommandChar()));
			writePerGuild.setInt   (7,data.isAutoCleanEnabled()?1:0);
			writePerGuild.execute();
			return true;
		} catch (SQLException e) {
			logger.error("Failed memorizing PerGuild config for "+data.getGuildId(),e);
			logger.error("SQL="+writePerGuild);
			return false;
		}
	}
	
	//-------------------------------------------------------------------
	public Collection<PerGuildConfig> getGuildConfigurations(String botID) {
		List<PerGuildConfig> ret = new ArrayList<PerGuildConfig>();
		try {
			Statement stat = con.createStatement();
			ResultSet set = stat.executeQuery("SELECT * FROM "+TABLE_PER_GUILD+" WHERE botId='"+botID+"'");
			while (set.next()) {
				PerGuildConfig data = new PerGuildConfig(set.getString("botId"), set.getString("guildId"));
				data.setLanguage(set.getString("lang"));
				data.setGuildName(set.getString("guildName"));
				if (set.getTimestamp("lastInfo")!=null)
					data.setLastInfo(Instant.ofEpochMilli(set.getTimestamp("lastInfo").getTime()));
				data.setCommandChar(set.getString("commandChar").charAt(0));
				ret.add(data);
			}
		} catch (SQLException e) {
			
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	public PerGuildConfig getGuildConfiguration(String botID, String guildId) {
		Statement stat = null;
		ResultSet set = null;
		try {
			stat = con.createStatement();
			set = stat.executeQuery("SELECT * FROM "+TABLE_PER_GUILD+" WHERE botId='"+botID+"' AND guildId='"+guildId+"'");
			while (set.next()) {
				PerGuildConfig data = new PerGuildConfig(set.getString("botId"), set.getString("guildId"));
				data.setLanguage(set.getString("lang"));
				data.setGuildName(set.getString("guildName"));
				if (set.getTimestamp("lastInfo")!=null)
					data.setLastInfo(Instant.ofEpochMilli(set.getTimestamp("lastInfo").getTime()));
				data.setCommandChar(set.getString("commandChar").charAt(0));
				data.setAutoClean(set.getInt("autoclean")>0);
				return data;
			}
		} catch (SQLException e) {
			logger.error("Failed SQL",e);			
		} finally {
			try {
				if (set!=null) set.close();
				if (stat!=null) stat.close();
			} catch (SQLException e) {
				logger.error("Failed SQL",e);
			}
		}
		return null;
	}
	
}
