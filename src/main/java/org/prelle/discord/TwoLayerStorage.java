package org.prelle.discord;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class TwoLayerStorage<C,U,V> {

	private Map<C, CreatingStorage<U, V>> data;
	private Constructor<V> constructor;
	
	//-------------------------------------------------------------------
	public TwoLayerStorage(Constructor<V> emptyConst) {
		data = new HashMap<C, CreatingStorage<U, V>>();
		this.constructor = emptyConst;
	}
	
	//-------------------------------------------------------------------
	public V get(C channel, U user) {
		CreatingStorage<U, V> ret = data.get(channel);
		if (ret!=null)
			return ret.get(user);
		try {
			ret = new CreatingStorage<U, V>(constructor);
			data.put(channel, ret);
			return ret.get(user);
		} catch (Exception e) {
			throw new RuntimeException("Failed instantiating object with empty constructor");
		}
	}
	
	//-------------------------------------------------------------------
	public void set(C channel, U user, V val) {
		CreatingStorage<U, V> ret = data.get(channel);
		if (ret==null) {
			ret = new CreatingStorage<U, V>(constructor);
			data.put(channel, ret);
		}
		ret.set(user, val);
	}

}
