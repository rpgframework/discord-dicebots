package org.prelle.discord;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.dv8tion.jda.api.entities.User;

/**
 * @author prelle
 *
 */
public class DiscordGamingSession<V extends VolatileCharacterData> {
	
	private User gamemaster;
	private Instant gamemasterSince;
	private Map<User,V> playerData;
	
	private Map<Long, User> joinRequests;
	private InitiativeTracker<V> iniTracker;
	
	private CheckSession checkSession;

	//-------------------------------------------------------------------
	public DiscordGamingSession() {
		this.playerData = new HashMap<User, V>();
		this.joinRequests = new HashMap<Long, User>();
		iniTracker = new InitiativeTracker<V>(null);
	}

	//-------------------------------------------------------------------
	public void clear() {
		playerData.clear();
		gamemaster = null;
		gamemasterSince = null;
		joinRequests.clear();
		iniTracker.clear();
		checkSession = null;
	}

	//-------------------------------------------------------------------
	public User getGamemaster() {
		if (gamemaster!=null) {
			// Set GM to null of session is longer than 4 hours
			if (Duration.between(gamemasterSince, Instant.now()).toHours()>8) {
				gamemaster = null;				
				gamemasterSince = null;
			}
			return gamemaster;
		}
		return null;
	}

	//-------------------------------------------------------------------
	public void setGamemaster(User gamemaster) {
		this.gamemaster = gamemaster;
		this.gamemasterSince = Instant.now();
	}

	//-------------------------------------------------------------------
	public List<User> getPlayer() {
		List<User> ret = new ArrayList<User>(playerData.keySet());
		Collections.sort(ret, new Comparator<User>() {
			public int compare(User o1, User o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		return ret;
	}

	//-------------------------------------------------------------------
	public void addPlayer(User user, V vol) {
		playerData.put(user, vol);
	}

	//-------------------------------------------------------------------
	public void removePlayer(User user) {
		playerData.remove(user);
	}

	//-------------------------------------------------------------------
	public V getVolatile(User player) {
		return playerData.get(player);
	}

	//-------------------------------------------------------------------
	public Collection<V> getVolatiles() {
		return playerData.values();
	}

	//-------------------------------------------------------------------
	public void addJoinRequest(User user, Long messageID) {
		joinRequests.put(messageID, user);
	}

	//-------------------------------------------------------------------
	public void removeJoinRequest(Long messageID) {
		joinRequests.remove(messageID);
	}

	//-------------------------------------------------------------------
	public User getJoinRequestUser(Long messageID) {
		return joinRequests.get(messageID);
	}

	//-------------------------------------------------------------------
	public InitiativeTracker<V> getIniTracker() {
		return iniTracker;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the checkSession
	 */
	public CheckSession getCheckSession() {
		return checkSession;
	}

	//-------------------------------------------------------------------
	/**
	 * @param checkSession the checkSession to set
	 */
	public void setCheckSession(CheckSession checkSession) {
		this.checkSession = checkSession;
	}
	
}
