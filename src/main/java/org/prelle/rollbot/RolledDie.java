package org.prelle.rollbot;

import java.util.ArrayList;
import java.util.List;

public class RolledDie  {
	
	private int diceType;
	private int face;
	private List<DiceEvaluationFlag> flags;
	
	//-------------------------------------------------------------------
	public RolledDie(int type, int face) {
		this.face  = face;
		this.diceType = type;
		this.flags = new ArrayList<DiceEvaluationFlag>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.valueOf(face);
	}

	//-------------------------------------------------------------------
	public boolean hasFlag(DiceEvaluationFlag flag) {
		return flags.contains(flag);
	}

	//-------------------------------------------------------------------
	/**
	 * @param exploded the exploded to set
	 */
	public void addFlag(DiceEvaluationFlag flag) {
		if (!flags.contains(flag))
			flags.add(flag);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public int getDiceType() {
		return diceType;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the face
	 */
	public int getFace() {
		return face;
	}

}
