package org.prelle.rollbot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MoveMessageToResultStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");

	//-------------------------------------------------------------------
	public MoveMessageToResultStep() {
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		for (int i=value.getRawElements().size()-1; i>=0; i--) {
			ParsedElement elem = value.getRawElements().get(i);
			if (elem.type==ParsedElementType.MESSAGE) {
				value.setMessage(String.valueOf(elem.rawValue));
				value.getRawElements().remove(elem);
				logger.debug("Move message to result");
			}
		}
	}

}
