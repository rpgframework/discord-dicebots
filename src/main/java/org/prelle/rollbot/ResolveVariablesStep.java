package org.prelle.rollbot;

import java.util.function.BiFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class ResolveVariablesStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");
	
	private BiFunction<ParsedElement, String, Integer> resolver;

	//-------------------------------------------------------------------
	public ResolveVariablesStep(BiFunction<ParsedElement, String, Integer> varResolver) {
		this.resolver = varResolver;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.type==ParsedElementType.VARIABLE) {
				// Check if already resolved
				if (elem.getValue()!=null)
					continue;
				
				Integer resolved = resolver.apply(elem,elem.getName());
				if (resolved==null)
					throw new ResolverException(elem.getName());
				logger.info("Variable "+elem.getName()+" resolves to "+resolved);
				elem.setValue(resolved);
				elem.setName(elem.getName()+"("+resolved+")");
			}
		}

	}

}
