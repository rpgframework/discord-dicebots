package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public enum CommonExecutionFlag implements ExecutionFlags {

	/** If the highest result is rolled, add another die and roll again */
	EXPLODE,
	/** Mark as pool roll */
	POOL,
	/** Mark as roll with dice */
	NO_POOL

}
