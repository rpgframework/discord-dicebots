package org.prelle.rollbot;

import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MoveFlagsToResultStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");
	
	private Function<String, ExecutionFlags> resolver;

	//-------------------------------------------------------------------
	public MoveFlagsToResultStep(Function<String, ExecutionFlags> flagResolver) {
		this.resolver = flagResolver;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		for (int i=value.getRawElements().size()-1; i>=0; i--) {
			ParsedElement elem = value.getRawElements().get(i);
			if (elem.type==ParsedElementType.FLAG) {
				ExecutionFlags flag = null;
				if (resolver!=null) {
					flag = resolver.apply((String)elem.rawValue);
				}
				if (flag==null) {
					try {
						flag = CommonExecutionFlag.valueOf((String)elem.rawValue);
					} catch (IllegalArgumentException e) {
						throw new ResolverException((String)elem.rawValue, true);
					}
				}
				value.addFlag(flag);
				value.getRawElements().remove(elem);
				logger.debug("Move flag "+flag+" to result");
			}
		}
	}

}
