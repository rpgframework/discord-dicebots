package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public enum RollPhase {

	PLAUSIBILITY_CHECK,
	PLAUSIBILITY_LATE_CHECK,
	/**
	 * Executed immediately after all dice have been rolled.
	 * E.g. to mark wild die for Shadowrun
	 */
	AFTER_ROLL,
	/**
	 * Evaluate the rolled dice according to the rules of your game.
	 * If no Hook for this phase is present, the default is to sum up
	 */
	DICE_TO_VALUE,
	/**
	 * After all is done.
	 * Used to take a numerical result and interprete it as a dice pool
	 * to roll again
	 */
	FINISHING,
	
}
