package org.prelle.rollbot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Sum all rolled dice to a single value.
 * @author prelle
 *
 */
public class SumUpDiceResultsStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");

	//-------------------------------------------------------------------
	public SumUpDiceResultsStep() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.DICE_TO_VALUE;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.type==ParsedElementType.OPERATOR && ((Operator)elem.rawValue)==Operator.DICE) {
				int sum = 0;
				for (RolledDie die : elem.getDice()) {
					sum += die.getFace();
				}
				logger.info(elem+" sums up to "+sum);
				elem.setValue(sum);
			}
		}

	}

}
