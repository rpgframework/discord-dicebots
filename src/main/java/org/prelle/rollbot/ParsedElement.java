package org.prelle.rollbot;

import java.util.List;

/**
 * @author prelle
 *
 */
public class ParsedElement {
	
	protected ParsedElementType type;
	protected Object rawValue;
	protected String name;
	protected Integer  value;
	protected List<RolledDie> dice;

	//-------------------------------------------------------------------
	public ParsedElement(ParsedElementType type, String value) {
		this.type = type;
		this.rawValue = value;
		this.name = value;
	}

	//-------------------------------------------------------------------
	public ParsedElement(ParsedElementType type, Integer value) {
		this.type = type;
		this.rawValue = value;
		this.value    = value;
	}

	//-------------------------------------------------------------------
	public ParsedElement(ParsedElementType type, Operator value) {
		this.type = type;
		this.rawValue = value;
	}

	//-------------------------------------------------------------------
	public ParsedElement(ParsedElementType type, List<ParsedElement> value) {
		this.type = type;
		this.rawValue = value;
	}

	//-------------------------------------------------------------------
	public String toString() {
//		if (type==ParsedElementType.DICE)
//			return type+":"+dieCount+"x"+rawValue;
		if (type==ParsedElementType.FLAG || type==ParsedElementType.MESSAGE)
			return type+":"+rawValue;
		if (name!=null)
			return name+"="+value;
//		if (value!=null)
			return type+":"+rawValue+"="+value;
//		return type+":"+rawValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dice
	 */
	public List<RolledDie> getDice() {
		return dice;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dice the dice to set
	 */
	public void setDice(List<RolledDie> dice) {
		this.dice = dice;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ParsedElementType getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the rawValue
	 */
	public Object getRawValue() {
		return rawValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rawValue the rawValue to set
	 */
	public void setRawValue(Object rawValue) {
		this.rawValue = rawValue;
	}

}
