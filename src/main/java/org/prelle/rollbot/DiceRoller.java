package org.prelle.rollbot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DiceRoller {
	
	protected static Logger logger = LogManager.getLogger("discord.dice");

	private enum Mode {
		IDLE,
		IN_HYPHEN,
		DIGIT,
		DIE,
		FLAG,
		VARIABLE,
	}
	
	private static Random random = new Random();

	public static void main(String[] args) {
//		String val = "25  +13";
//		System.out.println(DiceRoller.parse(val));
//		val = "2 * -13";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "2w6";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "2w6+4";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "2w6-4";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "10 !wild";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "Firearms";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "INT + LOG + EDGE";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "Perception/Town \"Im Labor\"";
//		System.out.println(val+" = "+DiceRoller.parse(val));
//		val = "2*(3+INT)";
//		System.out.println(val+" = "+DiceRoller.parse(val));
		String val = "2d10 +-3";
		System.out.println(val+" = "+DiceRoller.parse(val));
	}

	//-------------------------------------------------------------------
	static List<ParsedElement> parse(String command) {
		List<ParsedElement> parsed = new ArrayList<>();
		parse(command.trim(), 0, parsed);
		
		return parsed;
	}

	//-------------------------------------------------------------------
	private static int parse(String command, int from, List<ParsedElement> parsed) {
		Mode mode = Mode.IDLE;
		StringBuffer collect = new StringBuffer();
		for (int i=from; i<command.length(); i++) {
			char c = command.charAt(i);
//			logger.debug("Read "+c+" in "+mode);
			switch (mode) {
			case IDLE:
				if (Character.isWhitespace(c))
					continue;
				
				if (c=='"') {
					collect = new StringBuffer();
					mode = Mode.IN_HYPHEN;
//					logger.debug("  mode now "+mode);
				} else if (Character.isDigit(c)) {
					collect = new StringBuffer();
					collect.append(c);
					mode = Mode.DIGIT;
//					logger.debug("  mode now "+mode);
//				} else if ('w'==c || 'd'==c) {
//					collect = new StringBuffer();
//					mode = Mode.DIE;
				} else if (Character.isLetter(c)) {
					collect = new StringBuffer();
					collect.append(c);
					mode = Mode.VARIABLE;
//					logger.debug("  mode now "+mode);
				} else if ("+-*/".indexOf(c)>-1) {
					if ((i+1)<command.length() && Character.isDigit(command.charAt(i+1))) {
						// Minus isn't an operator, but belongs to number
						collect = new StringBuffer();
						collect.append(c);
						mode = Mode.DIGIT;
//						logger.debug("  mode now "+mode);
						continue;
					}
					
					collect = new StringBuffer();
					addOperator(parsed, Operator.getByCharacter(c));
					if (c!='-') {
						mode = Mode.IDLE;
//						logger.debug("  mode now "+mode);
					}
				} else if (c=='!') {
					mode = Mode.FLAG;
//					logger.debug("  mode now "+mode);
				} else if (c=='(') {
//					logger.debug("  START GROUP at "+i);
					List<ParsedElement> groupParsed = new ArrayList<ParsedElement>();
					i = parse(command, i+1, groupParsed);
//					logger.debug("  and end at "+i+" with "+groupParsed);
					ParsedElement group = new ParsedElement(ParsedElementType.GROUP, groupParsed);
					parsed.add(group);
					collect = new StringBuffer();
					mode = Mode.IDLE;
//					logger.debug("  mode now "+mode);
				} else {
					logger.warn("Don't know how to deal with '"+c+"' in "+mode);
				}
				break;
			case DIGIT:
				if (Character.isDigit(c)) {
					collect.append(c);					
				} else {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					mode = Mode.IDLE;
//					logger.debug("  mode now "+mode);
					if (Character.isWhitespace(c))
						continue;
					else if ('w'==c || 'd'==c) {
						mode = Mode.DIE;
						collect = new StringBuffer();
					} else if ("+-*/".indexOf(c)>-1) {
						collect = new StringBuffer();
						addOperator(parsed, Operator.getByCharacter(c));
						if (c!='-') {
							mode = Mode.IDLE;
//							logger.debug("  mode now "+mode);
						}
					} else {
						logger.warn("Don't know how to deal with '"+c+"' in "+mode+" after DIGIT");
					}
				}
				break;
			case IN_HYPHEN:
				if (c=='"') {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					mode = Mode.IDLE;
				} else {
					collect.append(c);
				}
				break;
			case DIE:
				if (Character.isDigit(c)) {
					collect.append(c);					
				} else {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					mode = Mode.IDLE;
//					logger.debug("  mode now "+mode);
					if (Character.isWhitespace(c)) {
						continue;
					} else if ("+-*/".indexOf(c)>-1) {
						collect = new StringBuffer();
						addOperator(parsed, Operator.getByCharacter(c));
						if (c!='-') {
							mode = Mode.IDLE;
							logger.debug("  mode now "+mode);
						}
					} else {
						logger.warn("Don't know how to deal with '"+c+"' in "+mode+" after DIE");
					}
				}
				break;
			case FLAG:
				if (Character.isWhitespace(c)) {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					mode = Mode.IDLE;
//					logger.debug("  mode now "+mode);
				} else {
					collect.append(c);
				}
				break;
			case VARIABLE:
				if (Character.isWhitespace(c)) {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					mode = Mode.IDLE;
//					logger.debug("  mode now "+mode);
				} else if (Character.isDigit(c)) {
					if (collect.length()==1) {
						collect = new StringBuffer();
						collect.append(c);						
						close(collect, Mode.DIE, parsed);
						collect = new StringBuffer();
						mode = Mode.IDLE;						
					} else {
						collect.append(c);
					}
				} else if ("+-".indexOf(c)>-1) {
					close(collect, mode, parsed);
					collect = new StringBuffer();
					addOperator(parsed, Operator.getByCharacter(c));
					if (c!='-') {
						mode = Mode.IDLE;
//						logger.debug("  mode now "+mode);
					}
				} else if (c==')') {
					close(collect, mode, parsed);
					collect = new StringBuffer();
//					logger.debug("  STOP GROUP");
					return i;
				} else {
					collect.append(c);
				}
				break;
			default:
				logger.warn("Don't know how to deal with '"+c+"' in "+mode);
			}
		}
		
		// Finalize
		close(collect, mode, parsed);
		
//		/*
//		 * Merge a number followed by a die without quantifier
//		 */
//		boolean changed = false;
//		do {
//			changed = mergeNumberWithDie(parsed);
//		} while (changed);
		
		
//		// Fix: Two numbers following each other get a PLUS added
//		ParsedElement last = null;
//		for (int i=parsed.size()-1; i>=0; i--) {
//			ParsedElement cur =parsed.get(i);
//			if (last!=null && last.type==ParsedElementType.NUMBER && cur.type==ParsedElementType.NUMBER) {
//				ParsedElement plus = new ParsedElement(ParsedElementType.OPERATOR, Operator.PLUS);
//				parsed.add(i+1, plus);
//			} else if (last!=null && last.type==ParsedElementType.DIE && cur.type==ParsedElementType.NUMBER) {
//				ParsedElement plus = new ParsedElement(ParsedElementType.OPERATOR, Operator.PLUS);
//				parsed.add(i+2, plus);
//			}
//			last = parsed.get(i);
//		}
		
		return command.length();
	}
	
//	//-------------------------------------------------------------------
//	private static boolean mergeNumberWithDie(List<ParsedElement> parsed) {
//		for (int i=0; i<parsed.size(); i++) {
//			ParsedElement elem = parsed.get(i);
//			if (elem.type==ParsedElementType.DIE && elem.dieCount==0) {
//				ParsedElement before = (i>0)?parsed.get(i-1):null;
//				if (before!=null && before.type==ParsedElementType.NUMBER) {
////					logger.warn("Merge "+before+" with "+elem);
//					elem.dieCount = (Integer)before.rawValue;
//					elem.type = ParsedElementType.DICE;
//					parsed.remove(before);
//					return true;
//				} else {
////					logger.warn("Assume count as 1 for "+elem);
//					elem.dieCount = 1;
//					elem.type = ParsedElementType.DICE;
//				}
//			}
//		}
//		return false;
//	}
	
	//-------------------------------------------------------------------
	private static void close(StringBuffer collect, Mode mode, List<ParsedElement> parsed) {
		switch(mode)  {
		case DIGIT:
			parsed.add(new ParsedElement(ParsedElementType.NUMBER, Integer.parseInt(collect.toString())));
			break;
//		case DIE:
//			parsed.add(new ParsedElement(ParsedElementType.DIE, Integer.parseInt(collect.toString())));
//			break;
		case FLAG:
			parsed.add(new ParsedElement(ParsedElementType.FLAG, collect.toString().toUpperCase()));
			break;
		case VARIABLE:
			parsed.add(new ParsedElement(ParsedElementType.VARIABLE, collect.toString().toUpperCase()));
			break;
		case IN_HYPHEN:
			parsed.add(new ParsedElement(ParsedElementType.MESSAGE, collect.toString()));
			break;
		case IDLE:
			break;
		default:
			logger.warn("Don't know how to close "+mode);
		}
		
	}
	
	//-------------------------------------------------------------------
	private static void addOperator(List<ParsedElement> parsed, Operator op) {
		if (parsed.isEmpty())
			return;
		
		ParsedElement last = parsed.get(parsed.size()-1);
		if (last.type==ParsedElementType.OPERATOR) {
			// Two  operators following each other
			Operator lastOP = (Operator)last.rawValue;
			switch (lastOP) {
			case PLUS:
				if (op==Operator.MINUS) {
					// Change into MINUS
					last.rawValue = op;	
					return;
				}
				break;
			}
			logger.warn("ToDo: Deal with lastOP="+lastOP+" followed by "+op);
		} else {
			parsed.add(new ParsedElement(ParsedElementType.OPERATOR, op));
		}
	}
	
	//-------------------------------------------------------------------
	private static List<PhaseHook> getHooks(RollPhase phase, PhaseHook...hooks) {
		List<PhaseHook> ret = new ArrayList<PhaseHook>();
		for (PhaseHook tmp : hooks) {
			if (tmp.getPhase()==phase)
				ret.add(tmp);
		}
		return ret;
	}
	
	//-------------------------------------------------------------------
	public static RollResult executeGroup(RollResult parentResult, List<ParsedElement> toExecute) {
		RollResult result = execute(toExecute, parentResult.getVarResolver(), parentResult.getFlagResolver(), parentResult.getHooks());
		return result;
	}
	
	//-------------------------------------------------------------------
	public static RollResult execute(List<ParsedElement> toExecute, BiFunction<ParsedElement, String, Integer> varResolver, Function<String, ExecutionFlags> flagResolver, PhaseHook...hooks) {
		RollResult result = new RollResult(null, toExecute, varResolver, flagResolver, hooks);
		logger.info("Raw = "+toExecute);
		
		List<PhaseHook> steps = new ArrayList<PhaseHook>();
		steps.add(new MoveFlagsToResultStep(flagResolver));
		steps.add(new MoveMessageToResultStep());
		steps.add(new ResolveVariablesStep(varResolver));
		steps.add(new AddPlusOpBetweenValuesStep());
		
		/*
		 * PLAUSIBILITY CHECK
		 */
		steps.addAll(getHooks(RollPhase.PLAUSIBILITY_CHECK, hooks));
		
		steps.add(new ResolveVariablesLateStep(varResolver));
		steps.addAll(getHooks(RollPhase.PLAUSIBILITY_LATE_CHECK, hooks));
		steps.add(new PerformOperatorStep(1)); // Roll
		
		/*
		 * AFTER ROLL
		 */
		steps.add(new DetectPoolRollStep());
		steps.addAll(getHooks(RollPhase.AFTER_ROLL, hooks));
		
		/*
		 * DICE TO VALUE
		 */
		if (getHooks(RollPhase.DICE_TO_VALUE, hooks).isEmpty()) {
			steps.add(new SumUpDiceResultsStep());
		} else {
			steps.addAll(getHooks(RollPhase.DICE_TO_VALUE, hooks));
		}
		
		steps.add(new PerformOperatorStep(2)); // Multiply and device
		steps.add(new PerformOperatorStep(3)); // Plus and minus
		/* Finishing */
		steps.addAll(getHooks(RollPhase.FINISHING, hooks));
		
		for (PhaseHook step : steps) {
			logger.info("Execute "+step.getClass());
			step.execute(result);
		}
		logger.info("Result1 = "+result);
		logger.info("Result2 = "+result.getValue());
		if (result.getValue()==null && result.getRawElements().size()==1 && result.getRawElements().get(0).getRawValue()==Operator.PLUS) {
			logger.info("Result3 = "+result.getRawElements().get(0).getValue());
			result.setValue(result.getRawElements().get(0).getValue());
		}
		return result;
	}

	//-------------------------------------------------------------------
	public static RollResult execute(String command, BiFunction<ParsedElement, String, Integer> varResolver, Function<String, ExecutionFlags> flagResolver, PhaseHook...hooks) {
		logger.info("Roll "+command);
		List<ParsedElement> elements = Parser.parse(command);
		
		return execute(elements, varResolver, flagResolver, hooks);
	}
	
}
