package org.prelle.rollbot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AddPlusOpBetweenValuesStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.PLAUSIBILITY_CHECK;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		if (value.getRawElements().size()<2)
			return;
		
		for (int i=1; i<value.getRawElements().size(); i++) {
			ParsedElement before = value.getRawElements().get(i-1);
			ParsedElement elem = value.getRawElements().get(i);
			logger.debug("Check "+before+" and "+elem);
			boolean beforeIsNonOP = before.getType()!=ParsedElementType.OPERATOR;
			boolean thisIsNonOP = elem.getType()!=ParsedElementType.OPERATOR;
			
			if (thisIsNonOP && beforeIsNonOP) {
				ParsedElement plusOP = new ParsedElement(ParsedElementType.OPERATOR, Operator.PLUS);
				value.getRawElements().add(i, plusOP);
				logger.debug("Add PLUS between "+before+" and "+elem);
				i++;
			}
		}
		logger.debug("Afterwards: "+value.getRawElements());
	}

}
