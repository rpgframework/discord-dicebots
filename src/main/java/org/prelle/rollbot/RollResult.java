package org.prelle.rollbot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author prelle
 *
 */
public class RollResult {
	
	private String originalCommand;
	private BiFunction<ParsedElement, String, Integer> varResolver;
	private Function<String, ExecutionFlags> flagResolver;
	private PhaseHook[] hooks;
	private String message;
	private List<ExecutionFlags> flags;
	private List<RollEvaluationFlag> evalFlags;
	
	private List<ParsedElement> raw;
	private Map<Integer, List<RolledDie>> rolled;
	private int diceGroup;
	private Boolean poolRoll;
	private Integer value;
	private Map<String, Object> userData;
 
	//-------------------------------------------------------------------
	public RollResult(String command, List<ParsedElement> unresolved, 
			BiFunction<ParsedElement, String, Integer> varResolver, 
			Function<String, ExecutionFlags> flagResolver, 
			PhaseHook...hooks) {
		originalCommand = command;
		raw      = unresolved;
		userData = new HashMap<String, Object>();
		this.flagResolver = flagResolver;
		this.varResolver  = varResolver;
		this.hooks        = hooks;
		flags    = new ArrayList<ExecutionFlags>();
		evalFlags    = new ArrayList<RollEvaluationFlag>();
		
		rolled   = new HashMap<Integer, List<RolledDie>>();
	}

	//-------------------------------------------------------------------
	public List<ExecutionFlags> getFlags() {
		return flags;
	}

	//-------------------------------------------------------------------
	public boolean hasFlag(ExecutionFlags flag) {
		return flags.contains(flag);
	}

	//-------------------------------------------------------------------
	public void addFlag(ExecutionFlags flag) {
		if (!flags.contains(flag))
			flags.add(flag);
		if (flag==CommonExecutionFlag.NO_POOL)
			poolRoll = Boolean.FALSE;
		else if (flag==CommonExecutionFlag.POOL)
			poolRoll = Boolean.TRUE;
	}

	//-------------------------------------------------------------------
	public List<RollEvaluationFlag> getEvaluationFlags() {
		return evalFlags;
	}

	//-------------------------------------------------------------------
	public boolean hasFlag(RollEvaluationFlag flag) {
		return evalFlags.contains(flag);
	}

	//-------------------------------------------------------------------
	public void addFlag(RollEvaluationFlag flag) {
		if (!evalFlags.contains(flag))
			evalFlags.add(flag);
	}

	//-------------------------------------------------------------------
	public String toString() {
		List<String> tmp = raw.stream().map(die -> String.valueOf(die)).collect(Collectors.toList());
		String ret = "["+String.join(",", tmp)+"]";
		
		if (message!=null)
			ret+="  -  "+message;
		ret+="  \t"+getDiceWithoutGroups();
		return ret;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @return the diceResult
//	 */
//	public List<Result> getDiceResult() {
//		return diceResult;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param diceResult the diceResult to set
//	 */
//	public void setDiceResult(List<Result> diceResult) {
//		this.diceResult = diceResult;
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	//-------------------------------------------------------------------
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		if (this.message==null)
			this.message = message;
		else
			this.message+="\n"+message;
	}

	//-------------------------------------------------------------------
	public List<ParsedElement> getRawElements() {
		return raw;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @param unresolved the unresolved to set
//	 */
//	public void setUnresolved(List<ParsedElement> unresolved) {
//		this.unresolved = unresolved;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @return the resolved
//	 */
//	public List<ParsedElement> getResolved() {
//		return resolved;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @param resolved the resolved to set
//	 */
//	public void setResolved(List<ParsedElement> resolved) {
//		this.resolved = resolved;
//	}

	//-------------------------------------------------------------------
	public int createNewDiceGroup() {
		return ++diceGroup;
	}

	//-------------------------------------------------------------------
	public void addRolledDie(int group, RolledDie die) {
		List<RolledDie> list = rolled.get(group);
		if (list==null) {
			list = new ArrayList<RolledDie>();
			rolled.put(group, list);
		}
		list.add(die);
	}

	//-------------------------------------------------------------------
	private List<Integer> getGroups() {
		List<Integer> ret = new ArrayList<Integer>();
		ret.addAll(rolled.keySet());
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public List<RolledDie> getDiceWithoutGroups() {
		List<RolledDie> ret = new ArrayList<RolledDie>();
		for (Integer key : getGroups()) {
			ret.addAll(rolled.get(key));
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public Map<Integer, List<RolledDie>> getRolled() {
		return rolled;
	}

	//-------------------------------------------------------------------
	public void setRolled(Map<Integer, List<RolledDie>> rolled) {
		this.rolled = rolled;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @return the varResolver
	 */
	public BiFunction<ParsedElement, String, Integer> getVarResolver() {
		return varResolver;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the flagResolver
	 */
	public Function<String, ExecutionFlags> getFlagResolver() {
		return flagResolver;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the hooks
	 */
	public PhaseHook[] getHooks() {
		return hooks;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the poolRoll
	 */
	public Boolean isPoolRoll() {
		return poolRoll;
	}

	//-------------------------------------------------------------------
	/**
	 * @param poolRoll the poolRoll to set
	 */
	public void setPoolRoll(boolean poolRoll) {
		this.poolRoll = poolRoll;
	}

	//-------------------------------------------------------------------
	public void setUserData(String key, Object value) {
		userData.put(key, value);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	public <T> T getUserData(Class<T> clazz, String key) {
		return (T)userData.get(key);
	}
}
