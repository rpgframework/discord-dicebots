package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public enum ParsedElementType {

	NUMBER,
	/* Type of die */
//	DIE,
//	/* Amount of die of a specific type */
//	DICE,
	VARIABLE,
	OPERATOR,
	GROUP,
	FLAG,
	MESSAGE,
}
