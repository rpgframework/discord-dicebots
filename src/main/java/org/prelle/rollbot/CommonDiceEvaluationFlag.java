package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public enum CommonDiceEvaluationFlag implements DiceEvaluationFlag {

	/** his dice has been added, because the previous die exploded */
	EXPLODED,
	
}
