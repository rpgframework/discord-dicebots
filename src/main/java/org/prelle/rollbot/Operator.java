package org.prelle.rollbot;

public enum Operator {
	PLUS(3),
	MINUS(3),
	MULTIPLY(2),
	DIVIDE(2),
	DICE(1)
	;
	int priority;
	Operator(int prio) {
		this.priority = prio;
	}
	public static Operator getByCharacter(char c) {
		switch (c) {
		case '+': return PLUS;
		case '-': return MINUS;
		case '*': return MULTIPLY;
		case '/': return DIVIDE;
		case 'd': return DICE;
		case 'w': return DICE;
		}
		throw new IllegalArgumentException("Unknown operator");
	}
	//-------------------------------------------------------------------
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}
}