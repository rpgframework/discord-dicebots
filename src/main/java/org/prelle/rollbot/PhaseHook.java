package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public interface PhaseHook {

	public RollPhase getPhase();
	
	public void execute(RollResult value);
	
}
