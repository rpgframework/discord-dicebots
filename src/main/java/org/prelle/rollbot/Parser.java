package org.prelle.rollbot;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class Parser {
	
	protected static Logger logger = LogManager.getLogger("discord.dice");

	private enum Mode {
		IDLE,
		IN_HYPHEN,
		DIGIT,
		FLAG,
		VARIABLE,
	}
	
	private static class ParseState {
		StringBuffer collect  = new StringBuffer();
		Mode mode = Mode.IDLE;
		List<ParsedElement> parsed = new ArrayList<ParsedElement>();
		public ParseState(List<ParsedElement> parsed) {
			this.parsed = parsed;
		}
	}

	//-------------------------------------------------------------------
	private static int processIDLE(String command, int i, ParseState state, char c) {
		if (Character.isWhitespace(c))
			return i;
		
		state.collect = new StringBuffer();
		
		if (c=='"') {
			state.mode = Mode.IN_HYPHEN;
		} else if (Character.isDigit(c)) {
			state.collect.append(c);
			state.mode = Mode.DIGIT;
//			logger.debug("  mode now "+mode);
//		} else if ('w'==c || 'd'==c) {
//			collect = new StringBuffer();
//			mode = Mode.DIE;
		} else if (Character.isLetter(c) || c=='_') {
			state.collect.append(c);
			state.mode = Mode.VARIABLE;
//			logger.debug("  mode now "+mode);
		} else if ("+-*/".indexOf(c)>-1) {
			state.parsed.add(new ParsedElement(ParsedElementType.OPERATOR, Operator.getByCharacter(c)));
			state.mode = Mode.IDLE;
		} else if (c=='!') {
			state.mode = Mode.FLAG;
//			logger.debug("  mode now "+mode);
		} else if (c=='(') {
			logger.debug("  START GROUP at "+i);
			List<ParsedElement> groupParsed = new ArrayList<ParsedElement>();
			i = parse(command, i+1, groupParsed);
			logger.debug("  and end at "+i+" with "+groupParsed);
			ParsedElement group = new ParsedElement(ParsedElementType.GROUP, groupParsed);
			state.parsed.add(group);
			state.collect = new StringBuffer();
			state.mode = Mode.IDLE;
			return i;
//			logger.debug("  mode now "+mode);
		} else {
			logger.warn("Don't know how to deal with '"+c+"' in "+state.mode);
		}
		
		return i;
	}

	//-------------------------------------------------------------------
	private static int processDIGIT(String command, int i, ParseState state, char c) {
		if (Character.isDigit(c)) {
			state.collect.append(c);					
		} else {
			close(state);
			state.collect = new StringBuffer();
			state.mode = Mode.IDLE;
			processIDLE(command, i, state, c);
		}
		return i;
	}	

	//-------------------------------------------------------------------
	public static List<ParsedElement> parseStep1(String command) {
		List<ParsedElement> parsed = new ArrayList<>();
		parse(command.trim(), 0, parsed);
		
		return parsed;
	}

	//-------------------------------------------------------------------
	private static int parse(String command, int from, List<ParsedElement> parsed) {
		ParseState state = new ParseState(parsed);
		for (int i=from; i<command.length(); i++) {
			char c = command.charAt(i);
			logger.debug("Read "+c+" in "+state.mode);
			switch (state.mode) {
			case IDLE:
				i = processIDLE(command, i, state, c);
				break;
			case DIGIT:
				if (c==')') {
					close(state);
					logger.debug("  STOP GROUP");
					return i;
				}
				i = processDIGIT(command, i, state, c);
				break;
			case IN_HYPHEN:
				if (c=='"') {
					close(state);
				} else {
					state.collect.append(c);
				}
				break;
			case FLAG:
				if (Character.isWhitespace(c)) {
					close(state);
				} else {
					state.collect.append(c);
				}
				break;
			case VARIABLE:
				if (Character.isWhitespace(c)) {
					close(state);
				} else if (c==')') {
					close(state);
					logger.debug("  STOP GROUP");
					return i;
				} else if (Character.isLetter(c) ||  c=='/' || c=='_') {
					state.collect.append(c);
				} else {
					close(state);
					processIDLE(command, i, state, c);
				}
				break;
			default:
				logger.warn("Don't know how to deal with '"+c+"' in "+state.mode);
			}
		}
		
		// Finalize
		close(state);
		
		
		return command.length();
	}
	
	//-------------------------------------------------------------------
	private static void close(ParseState state) {
		switch(state.mode)  {
		case DIGIT:
			state.parsed.add(new ParsedElement(ParsedElementType.NUMBER, Integer.parseInt(state.collect.toString())));
			break;
		case FLAG:
			state.parsed.add(new ParsedElement(ParsedElementType.FLAG, state.collect.toString().toUpperCase()));
			break;
		case VARIABLE:
			state.parsed.add(new ParsedElement(ParsedElementType.VARIABLE, state.collect.toString().toUpperCase()));
			break;
		case IN_HYPHEN:
			state.parsed.add(new ParsedElement(ParsedElementType.MESSAGE, state.collect.toString()));
			break;
		case IDLE:
			break;
		default:
			logger.warn("Don't know how to close "+state.mode);
		}
		
		state.mode = Mode.IDLE;
		state.collect = new StringBuffer();
	}

	//-------------------------------------------------------------------
	/**
	 * Detect dice operators
	 */
	public static void parseStep2(List<ParsedElement> parsed) {
		for (ParsedElement tmp : parsed) {
			if (tmp.type==ParsedElementType.VARIABLE) {
				String name = (String)tmp.rawValue;
				if (name.equalsIgnoreCase("d") || name.equalsIgnoreCase("w")) {
					logger.debug(" convert "+tmp+" to dice operator");
					tmp.type = ParsedElementType.OPERATOR;
					tmp.rawValue= Operator.DICE;
				}
			}
		}
	}
	
	//-------------------------------------------------------------------
	public static List<ParsedElement> parse(String command) {
		List<ParsedElement> ret = parseStep1(command);
		parseStep2(ret);
		return ret;
	}

}
