package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public class ResolverException extends RuntimeException {
	
	private static final long serialVersionUID = -7113879051951887108L;

	private boolean flag;
	private String variable;

	//-------------------------------------------------------------------
	public ResolverException(String variable) {
		super("Failed resolving '"+variable+"'");
		this.variable  = variable;
	}

	//-------------------------------------------------------------------
	public ResolverException(String name, boolean isFlag) {
		super("Failed resolving "+(isFlag?"flag":"variable")+" '"+name+"'");
		this.variable  = name;
	}

	//-------------------------------------------------------------------
	public String getVariable() {
		return variable;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the flag
	 */
	public boolean isFlag() {
		return flag;
	}

}
