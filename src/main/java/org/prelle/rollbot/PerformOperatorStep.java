package org.prelle.rollbot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author prelle
 *
 */
public class PerformOperatorStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");
	
	private static Random random = new Random();
	
	private int priority;

	//-------------------------------------------------------------------
	public PerformOperatorStep(int priority) {
		this.priority = priority;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//-------------------------------------------------------------------
	private ParsedElement getNextOperator(RollResult value, List<ParsedElement> alreadyExecuted) {
		for (ParsedElement tmp : value.getRawElements()) {
			if (tmp.type==ParsedElementType.OPERATOR) {
				Operator op = (Operator)tmp.rawValue;
				// Must match priority and may not be already executed
				if (op.getPriority()==priority && !alreadyExecuted.contains(tmp)) {
					alreadyExecuted.add(tmp);
					return tmp;
				}
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		boolean changed = true;
		List<ParsedElement> alreadyExecuted = new ArrayList<ParsedElement>();
		while (changed) {
			changed = false;
			ParsedElement opElem = getNextOperator(value, alreadyExecuted);
			if (opElem==null)
				return;
			Operator op = (Operator) opElem.rawValue;
			int index = value.getRawElements().indexOf(opElem);
			ParsedElement operand1 = null;
			ParsedElement operand2 = null;
			// There should be an operand before 
			if (index==0) {
				// No operand before
				// For DICE operator, assume a number value of 1
				if (op==Operator.DICE) {
					operand1 = new ParsedElement(ParsedElementType.NUMBER, 1);
				} else {
					throw new IllegalArgumentException("Operand "+op+" must be preceeded by an element with a value");
				}
			} else {
				operand1 = value.getRawElements().get(index-1);
				if (operand1.getValue()==null) {
					throw new IllegalArgumentException("Operand "+op+" has operand1 "+operand1+" which has no numerical value");
				}
			}
			// There should be an element afterwards
			if ( (index+1)>=value.getRawElements().size()) {
				// No operand after
				throw new IllegalArgumentException("Operand "+op+" must be followed by an element with a value");
			} else {
				operand2 = value.getRawElements().get(index+1);
				if (operand2.getValue()==null) {
					throw new IllegalArgumentException("Operand "+op+" has operand2 "+operand1+" which has no numerical value");
				}
			}
			
			changed = execute(value, op, opElem, operand1, operand2);
			logger.debug("After operator: "+value.getRawElements());
		}
	}

	//-------------------------------------------------------------------
	private boolean execute(RollResult value, Operator op, ParsedElement opElem, ParsedElement operand1, ParsedElement operand2) {
		switch (op) {
		case DICE:
			logger.debug("Roll "+operand1.getValue()+"d"+operand2.getValue());
			value.getRawElements().remove(operand1);
			value.getRawElements().remove(operand2);
			opElem.setName(operand1.getValue()+"d"+operand2.getValue());
			if (operand1.getValue()>40)
				throw new TooManyDiceException(operand1.getValue());
			roll(value, opElem, operand1.getValue(), operand2.getValue());
			int sumD = opElem.getDice().stream().collect(Collectors.summingInt(new ToIntFunction<RolledDie>() {
				public int applyAsInt(RolledDie die) {
					return die.getFace();
				}
			}));
			opElem.setValue(sumD);
			logger.debug("Rolled "+opElem.getDice());
			return true;
		case PLUS:
			int sum = operand1.getValue() + operand2.getValue();
			logger.warn(operand1.getValue()+" + "+operand2.getValue()+" = "+sum);			
			value.getRawElements().remove(operand1);
			value.getRawElements().remove(operand2);
			opElem.setName(operand1.getName()+"+"+operand2.getValue());
			opElem.setValue(sum);
			return true;
		case MINUS:
			sum = operand1.getValue() - operand2.getValue();
			logger.warn(operand1.getValue()+" - "+operand2.getValue()+" = "+sum);			
			value.getRawElements().remove(operand1);
			value.getRawElements().remove(operand2);
			opElem.setValue(sum);
			opElem.setName(operand1.getName()+"-"+operand2.getName());
			return true;
		case MULTIPLY:
			int prod = operand1.getValue() * operand2.getValue();
			logger.warn(operand1.getValue()+" * "+operand2.getValue()+" = "+prod);			
			value.getRawElements().remove(operand1);
			value.getRawElements().remove(operand2);
			opElem.setName(operand1.getName()+"*"+operand2.getName());
			opElem.setValue(prod);
			return true;
		default:
			logger.warn("Not implemented yet: "+op);
		}
		return false;
	}

	//-------------------------------------------------------------------
	private void roll(RollResult value, ParsedElement opElem, int amount, int type) {
		boolean shouldExplode = value.hasFlag(CommonExecutionFlag.EXPLODE);
		List<RolledDie> dice = new ArrayList<RolledDie>();
		
		if (shouldExplode) {
			for (int i=0; i<amount; i++) {
				int group = value.createNewDiceGroup();
				List<RolledDie> dicePart = rollSingleDie(value, group, type);
				dice.addAll(dicePart);
			}
		} else {
			int group = value.createNewDiceGroup();
			for (int i=0; i<amount; i++) {
				List<RolledDie> dicePart = rollSingleDie(value, group, type);
				dice.addAll(dicePart);
			}
		}
		opElem.setDice(dice);
	}

	//-------------------------------------------------------------------
	private List<RolledDie> rollSingleDie(RollResult value, int group, int type) {
		List<RolledDie> ret = new ArrayList<RolledDie>();
		boolean shouldExplode = value.hasFlag(CommonExecutionFlag.EXPLODE);
		int lastRoll = 0;
		int pass=0;
		do {
			lastRoll = random.nextInt(type)+1;
			RolledDie result = new RolledDie(type, lastRoll);
			if (pass>0)
				result.addFlag(CommonDiceEvaluationFlag.EXPLODED);
			ret.add(result);
			value.addRolledDie(group, result);
			pass++;
		} while (shouldExplode && lastRoll==type);
		
		return ret;
	}

}
