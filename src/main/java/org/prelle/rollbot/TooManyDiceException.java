package org.prelle.rollbot;

/**
 * @author prelle
 *
 */
public class TooManyDiceException extends RuntimeException {

	private int value;
	
	public TooManyDiceException(int value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	
	
}
