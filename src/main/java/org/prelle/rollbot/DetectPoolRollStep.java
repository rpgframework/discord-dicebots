package org.prelle.rollbot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * When the elements do not contain a dice operator, treat the whole thing as a 
 * pool of D6
 * @author prelle
 *
 */
public class DetectPoolRollStep implements PhaseHook {
	
	protected static Logger logger = LogManager.getLogger("discord");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rollbot.PhaseHook#getPhase()
	 */
	@Override
	public RollPhase getPhase() {
		return RollPhase.AFTER_ROLL;
	}

	//-------------------------------------------------------------------
	/**
	 * If any resolved variable is the attribute EDGE, add the WILD flag
	 * @see org.prelle.rollbot.PhaseHook#execute(org.prelle.rollbot.RollResult)
	 */
	@Override
	public void execute(RollResult value) {
		// Don't do this. if already done
		if (value.isPoolRoll()!=null) {
			return;
		}
		
		boolean diceFound = false;
		for (ParsedElement elem : value.getRawElements()) {
			if (elem.getType()==ParsedElementType.OPERATOR && elem.getRawValue()==Operator.DICE) {
				diceFound = true;
				break;
			}
		}
		
		value.setPoolRoll(!diceFound);
		if (diceFound) {
			logger.debug("Dice found");
		} else {
			logger.debug("No Dice found");
		}
		
	}

}
